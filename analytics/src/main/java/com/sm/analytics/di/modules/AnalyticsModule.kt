package com.sm.analytics.di.modules

import android.content.Context
import com.sm.analytics.tracker.AnalyticsTracker
import com.sm.analytics.tracker.HuaweiAnalyticsTrackerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AnalyticsModule {

    @Provides
    @Singleton
    fun provideAnalyticsTracker(context: Context): AnalyticsTracker =
        HuaweiAnalyticsTrackerImpl(context)
}
