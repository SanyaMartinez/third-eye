package com.sm.analytics.utils

object AnalyticsConstants {
    internal object UserPropertyKeys {
        const val PHONE_CODE = "phoneCode"
        const val PHONE_NUMBER = "phoneNumber"
        const val DEVICE_NAME = "deviceName"
        const val APP_VERSION_CODE = "appVersionCode"
        const val APP_VERSION_NUMBER = "appVersionNumber"
    }

    object Events {
        const val TARGET_REQUEST_SEND_SUCCESS = "targetRequestSendSuccess"
        const val TARGET_REQUEST_SEND_FAILURE = "targetRequestSendFailure"

        const val USER_DETAIL_EDIT_PHOTO = "userDetailEditPhoto"
        const val USER_DETAIL_EDIT_COLOR = "userDetailEditColor"
        const val USER_DETAIL_TOGGLE_NOTIFICATIONS = "userDetailToggleNotifications"
        const val USER_DETAIL_SEND_MESSAGE = "userDetailSendMessage"
        const val USER_DETAIL_SOUND_ALERT = "userDetailSoundAlert"
        const val USER_DETAIL_LOCATION_HISTORY = "userDetailLocationHistory"
        const val USER_DETAIL_DELETE_USER = "userDetailDeleteUser"

        const val GEOFENCES_WATCH = "geofencesWatch"
        const val GEOFENCES_EDIT = "geofencesEdit"
        const val GEOFENCES_DELETE = "geofencesDelete"

        const val PROFILE_CHANGE_NAME = "profileChangeName"
        const val PROFILE_LOGOUT = "profileLogout"
    }

    object Params {
        const val USER_ID = "userId"
    }
}
