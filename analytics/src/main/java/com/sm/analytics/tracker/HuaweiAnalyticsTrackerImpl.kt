package com.sm.analytics.tracker

import android.content.Context
import android.os.Bundle
import com.huawei.hms.analytics.HiAnalytics
import com.sm.analytics.utils.AnalyticsConstants
import javax.inject.Inject

internal class HuaweiAnalyticsTrackerImpl @Inject constructor(
    context: Context
): AnalyticsTracker {

    private val analyticsInstance = HiAnalytics.getInstance(context)

    override fun setUserData(
        userId: String,
        phoneCode: String,
        phoneNumber: String,
        deviceName: String,
        appVersionCode: String,
        appVersionNumber: String,
    ) {
        analyticsInstance.run {
            setUserId(userId)
            setUserProfile(AnalyticsConstants.UserPropertyKeys.PHONE_CODE, phoneCode)
            setUserProfile(AnalyticsConstants.UserPropertyKeys.PHONE_NUMBER, phoneNumber)
            setUserProfile(AnalyticsConstants.UserPropertyKeys.DEVICE_NAME, deviceName)
            setUserProfile(AnalyticsConstants.UserPropertyKeys.APP_VERSION_CODE, appVersionCode)
            setUserProfile(AnalyticsConstants.UserPropertyKeys.APP_VERSION_NUMBER, appVersionNumber)
        }
    }

    override fun trackEvent(eventName: String, params: Bundle) {
        analyticsInstance.onEvent(eventName, params)
    }
}
