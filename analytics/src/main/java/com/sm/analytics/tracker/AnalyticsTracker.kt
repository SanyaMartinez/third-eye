package com.sm.analytics.tracker

import android.os.Bundle

interface AnalyticsTracker {

    fun setUserData(
        userId: String,
        phoneCode: String,
        phoneNumber: String,
        deviceName: String,
        appVersionCode: String,
        appVersionNumber: String,
    )

    fun trackEvent(eventName: String, params: Bundle = Bundle())
}
