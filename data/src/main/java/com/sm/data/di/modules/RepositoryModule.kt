package com.sm.data.di.modules

import com.sm.data.localdb.UserInfoStorage
import com.sm.data.localdb.dao.*
import com.sm.data.networkdb.geofence.GeofenceNetworkDatabase
import com.sm.data.networkdb.location.LocationNetworkDatabase
import com.sm.data.networkdb.note.NoteNetworkDatabase
import com.sm.data.networkdb.notification.NotificationNetworkDatabase
import com.sm.data.networkdb.request.RequestNetworkDatabase
import com.sm.data.networkdb.userInfo.UserInfoNetworkDatabase
import com.sm.data.repository.geofence.GeofenceRepository
import com.sm.data.repository.geofence.GeofenceRepositoryImpl
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.location.LocationRepositoryImpl
import com.sm.data.repository.note.NoteRepository
import com.sm.data.repository.note.NoteRepositoryImpl
import com.sm.data.repository.notification.NotificationRepositoryImpl
import com.sm.data.repository.user.UserRepositoryImpl
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.data.repository.relations.RelationsRepositoryImpl
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.data.repository.userinfo.UserInfoRepositoryImpl
import com.sm.pushapi.managers.PushSender
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideLocationRepository(
        locationDao: LocationDao,
        locationNetworkDatabase: LocationNetworkDatabase
    ): LocationRepository = LocationRepositoryImpl(locationDao, locationNetworkDatabase)

    @Provides
    @Singleton
    fun provideNotificationRepository(
        notificationDao: NotificationDao,
        notificationNetworkDatabase: NotificationNetworkDatabase,
        pushSender: PushSender,
    ): NotificationRepository = NotificationRepositoryImpl(
        notificationDao,
        notificationNetworkDatabase,
        pushSender
    )

    @Provides
    @Singleton
    fun provideUserRepository(userDao: UserDao): UserRepository = UserRepositoryImpl(userDao)

    @Provides
    @Singleton
    fun provideGeofenceRepository(
        geofenceDao: GeofenceDao,
        geofenceNetworkDatabase: GeofenceNetworkDatabase,
        userInfoRepository: UserInfoRepository
    ): GeofenceRepository = GeofenceRepositoryImpl(geofenceDao, geofenceNetworkDatabase, userInfoRepository)

    @Provides
    @Singleton
    fun provideUserInfoRepository(
        userInfoStorage: UserInfoStorage,
        userInfoNetworkDatabase: UserInfoNetworkDatabase
    ): UserInfoRepository = UserInfoRepositoryImpl(userInfoStorage, userInfoNetworkDatabase)

    @Provides
    @Singleton
    fun provideRelationsRepository(
        requestNetworkDatabase: RequestNetworkDatabase,
        userRepository: UserRepository,
        userInfoRepository: UserInfoRepository
    ): RelationsRepository = RelationsRepositoryImpl(requestNetworkDatabase, userRepository, userInfoRepository)

    @Provides
    @Singleton
    fun provideNoteRepository(
        noteDao: NoteDao,
        noteNetworkDatabase: NoteNetworkDatabase,
    ): NoteRepository = NoteRepositoryImpl(noteDao, noteNetworkDatabase)
}
