package com.sm.data.di.modules

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.sm.data.localdb.AppDatabase
import com.sm.data.localdb.UserInfoStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDbModule {
    @Provides
    @Singleton
    fun provideAppDatabase(application: Application) = Room
        .databaseBuilder(application, AppDatabase::class.java, AppDatabase.DB_NAME)
        .addMigrations(
            AppDatabase.MIGRATION_1_2,
            AppDatabase.MIGRATION_2_3,
            AppDatabase.MIGRATION_3_4,
            AppDatabase.MIGRATION_4_5,
            AppDatabase.MIGRATION_5_6,
        )
        .build()

    @Provides
    @Singleton
    fun provideUserDao(appDatabase: AppDatabase) = appDatabase.userDao()

    @Provides
    @Singleton
    fun provideNotificationDao(appDatabase: AppDatabase) = appDatabase.notificationDao()

    @Provides
    @Singleton
    fun provideLocationDao(appDatabase: AppDatabase) = appDatabase.locationDao()

    @Provides
    @Singleton
    fun provideGeofenceDao(appDatabase: AppDatabase) = appDatabase.geofenceDao()

    @Provides
    @Singleton
    fun provideNoteDao(appDatabase: AppDatabase) = appDatabase.noteDao()

    @Provides
    @Singleton
    fun provideUserInfoStorage(context: Context) = UserInfoStorage(context)
}
