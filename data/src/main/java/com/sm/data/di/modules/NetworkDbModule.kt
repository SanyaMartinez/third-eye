package com.sm.data.di.modules

import com.sm.data.networkdb.geofence.GeofenceNetworkDatabase
import com.sm.data.networkdb.geofence.GeofenceNetworkDatabaseImpl
import com.sm.data.networkdb.location.LocationNetworkDatabase
import com.sm.data.networkdb.location.LocationNetworkDatabaseImpl
import com.sm.data.networkdb.note.NoteNetworkDatabase
import com.sm.data.networkdb.note.NoteNetworkDatabaseImpl
import com.sm.data.networkdb.notification.NotificationNetworkDatabase
import com.sm.data.networkdb.notification.NotificationNetworkDatabaseImpl
import com.sm.data.networkdb.request.RequestNetworkDatabase
import com.sm.data.networkdb.request.RequestNetworkDatabaseImpl
import com.sm.data.networkdb.userInfo.UserInfoNetworkDatabase
import com.sm.data.networkdb.userInfo.UserInfoNetworkDatabaseImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkDbModule {

    @Provides
    @Singleton
    fun provideUserInfoNetworkDatabase(): UserInfoNetworkDatabase = UserInfoNetworkDatabaseImpl()

    @Provides
    @Singleton
    fun provideGeofenceNetworkDatabase(): GeofenceNetworkDatabase = GeofenceNetworkDatabaseImpl()

    @Provides
    @Singleton
    fun provideLocationNetworkDatabase(): LocationNetworkDatabase = LocationNetworkDatabaseImpl()

    @Provides
    @Singleton
    fun provideRequestNetworkDatabase(): RequestNetworkDatabase = RequestNetworkDatabaseImpl()

    @Provides
    @Singleton
    fun provideNotificationNetworkDatabase(): NotificationNetworkDatabase = NotificationNetworkDatabaseImpl()

    @Provides
    @Singleton
    fun provideNoteNetworkDatabase(): NoteNetworkDatabase = NoteNetworkDatabaseImpl()
}
