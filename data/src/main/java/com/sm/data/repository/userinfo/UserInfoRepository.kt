package com.sm.data.repository.userinfo

import com.sm.data.models.UserInfoModel

interface UserInfoRepository {
    var userId: String
    var name: String
    var phoneCode: String
    var phoneNumber: String
    var pushToken: String
    var watchersIds: List<String>
    var targetsIds: List<String>

    fun getPhone(): String

    suspend fun sendUserInfo()

    suspend fun updatePushToken(pushToken: String)

    suspend fun updateName(name: String)

    suspend fun isExistByPhone(phoneCode: String, phoneNumber: String): Boolean

    suspend fun changeTargetWatcherRelation(targetId: String, watcherId: String, isAdding: Boolean)

    suspend fun getUserInfoByPhone(phoneCode: String, phoneNumber: String): UserInfoModel?

    suspend fun getUserInfoById(id: String): UserInfoModel?

    suspend fun loadAndUpdateUserInfo()

    fun clearData()
}
