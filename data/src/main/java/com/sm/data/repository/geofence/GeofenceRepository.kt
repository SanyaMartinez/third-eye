package com.sm.data.repository.geofence

import androidx.lifecycle.LiveData
import com.sm.data.models.GeofenceEntity

interface GeofenceRepository {
    fun getById(id: Long): LiveData<GeofenceEntity?>

    fun getAllToDisplay(): LiveData<List<GeofenceEntity>>

    suspend fun getAllToTrackAsync(): List<GeofenceEntity>

    suspend fun insert(item: GeofenceEntity)

    suspend fun delete(item: GeofenceEntity)

    suspend fun loadAndUpdateGeofences(watcherId: String)
}
