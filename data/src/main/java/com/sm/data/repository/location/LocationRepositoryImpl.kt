package com.sm.data.repository.location

import com.sm.data.localdb.dao.LocationDao
import com.sm.data.models.LocationEntity
import com.sm.data.networkdb.location.LocationNetworkDatabase
import javax.inject.Inject

internal class LocationRepositoryImpl @Inject constructor(
    private val locationDao: LocationDao,
    private val locationNetworkDatabase: LocationNetworkDatabase
): LocationRepository {
    override fun getAllBetweenTimeInterval(
        startTime: Long,
        endTime: Long,
        id: String
    ) = locationDao.getAllBetweenTimeInterval(startTime, endTime, id)

    override val updatedLocation = locationNetworkDatabase.updatedLocation

    override fun listenToLocationUpdates(targetsIds: List<String>) =
        locationNetworkDatabase.listenToLocationUpdates(targetsIds)

    override fun getTargetsLastLocations()=
        locationDao.getTargetsLastLocations()

    override suspend fun insert(item: LocationEntity) {
        locationNetworkDatabase.insert(item)
        locationDao.insert(item)
    }

    override suspend fun insertLocal(item: LocationEntity) = locationDao.insert(item)

    override suspend fun loadLocations(targetId: String) {
        val locations = locationNetworkDatabase.loadLocations(targetId)
        locationDao.insertAll(locations)
    }
}
