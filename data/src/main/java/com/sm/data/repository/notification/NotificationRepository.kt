package com.sm.data.repository.notification

import androidx.lifecycle.LiveData
import com.sm.data.models.NotificationEntity

interface NotificationRepository {
    fun getAll(): LiveData<List<NotificationEntity>>

    suspend fun sendNotification(userId: String, userPushToken: String, item: NotificationEntity): Boolean

    suspend fun loadAll(userId: String)

    suspend fun add(item: NotificationEntity)

    suspend fun hide(userId: String, item: NotificationEntity)
}
