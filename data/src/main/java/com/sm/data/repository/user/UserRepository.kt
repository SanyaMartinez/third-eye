package com.sm.data.repository.user

import androidx.lifecycle.LiveData
import com.sm.data.models.UserEntity

interface UserRepository {
    fun getById(id: String): LiveData<UserEntity?>

    suspend fun getByIdAsync(id: String): UserEntity?

    fun getAll(): LiveData<List<UserEntity>>

    fun getAllAccepted(): LiveData<List<UserEntity>>

    fun getAcceptedTargets(): LiveData<List<UserEntity>>

    suspend fun getAcceptedTargetsAsync(): List<UserEntity>

    suspend fun getAcceptedWatchersAsync(): List<UserEntity>

    suspend fun insert(item: UserEntity)

    suspend fun update(item: UserEntity)

    suspend fun updateTargetStatusById(id: String, isTarget: Boolean, isAcceptedTarget: Boolean)

    suspend fun updateWatcherStatusById(id: String, isWatcher: Boolean, isAcceptedWatcher: Boolean)
}
