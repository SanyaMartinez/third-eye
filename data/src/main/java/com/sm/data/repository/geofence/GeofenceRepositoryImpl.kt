package com.sm.data.repository.geofence

import com.sm.data.localdb.dao.GeofenceDao
import com.sm.data.models.GeofenceEntity
import com.sm.data.networkdb.geofence.GeofenceNetworkDatabase
import com.sm.data.repository.userinfo.UserInfoRepository
import javax.inject.Inject

internal class GeofenceRepositoryImpl @Inject constructor(
    private val geofenceDao: GeofenceDao,
    private val geofenceNetworkDatabase: GeofenceNetworkDatabase,
    private val userInfoRepository: UserInfoRepository
): GeofenceRepository {
    override fun getById(id: Long) = geofenceDao.getById(id)

    override fun getAllToDisplay() = geofenceDao.getAllToDisplay(userInfoRepository.userId)

    override suspend fun getAllToTrackAsync() = geofenceDao.getAllToTrackAsync(userInfoRepository.userId)

    override suspend fun insert(item: GeofenceEntity) {
        geofenceNetworkDatabase.insert(item)
        geofenceDao.insert(item)
    }

    override suspend fun delete(item: GeofenceEntity) {
        geofenceNetworkDatabase.delete(item)
        geofenceDao.delete(item)
    }

    override suspend fun loadAndUpdateGeofences(watcherId: String) {
        val geofences = geofenceNetworkDatabase.loadGeofences(watcherId)
        geofenceDao.updateAll(watcherId, geofences)
    }
}
