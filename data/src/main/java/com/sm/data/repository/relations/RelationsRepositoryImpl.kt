package com.sm.data.repository.relations

import com.sm.data.models.RequestModel
import com.sm.data.models.UserEntity
import com.sm.data.models.UserInfoModel
import com.sm.data.networkdb.request.RequestNetworkDatabase
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import javax.inject.Inject

/**
 * Отвечает за создание/получение/обработку/отмену запросов на отслеживание между
 * пользователями, связь между ними, удаление пользователей и обновление UserRepository
 */
internal class RelationsRepositoryImpl @Inject constructor(
    private val requestNetworkDatabase: RequestNetworkDatabase,
    private val userRepository: UserRepository,
    private val userInfoRepository: UserInfoRepository
): RelationsRepository {
    override suspend fun sendRequest(targetUserInfo: UserInfoModel) {
        val request = RequestModel(
            targetId = targetUserInfo.id,
            watcherId = userInfoRepository.userId
        )
        requestNetworkDatabase.insert(request)

        targetUserInfo.run {
            val savedTarget = userRepository.getByIdAsync(id)
            val updatedTarget = savedTarget?.copy(isTarget = true)
                ?: UserEntity(id, name, phoneCode, phoneNumber, pushToken, isTarget = true)

            userRepository.insert(updatedTarget)
        }
    }

    override suspend fun cancelRequest(targetId: String) {
        val watcherId = userInfoRepository.userId
        deleteRequestFromNetworkDB(targetId, watcherId)

        userRepository.updateTargetStatusById(targetId, isTarget = false, isAcceptedTarget = false)
    }

    override suspend fun acceptRequest(watcherId: String) {
        val targetId = userInfoRepository.userId
        deleteRequestFromNetworkDB(targetId, watcherId)
        changeTargetWatcherRelation(targetId, watcherId, isAdding = true)

        userRepository.updateWatcherStatusById(watcherId, isWatcher = true, isAcceptedWatcher = true)
    }

    override suspend fun denyRequest(watcherId: String) {
        val targetId = userInfoRepository.userId
        deleteRequestFromNetworkDB(targetId, watcherId)

        userRepository.updateWatcherStatusById(watcherId, isWatcher = false, isAcceptedWatcher = false)
    }

    override suspend fun handleCanceling(watcherId: String) {
        val targetId = userInfoRepository.userId
        deleteRequestFromNetworkDB(targetId, watcherId)

        userRepository.updateWatcherStatusById(watcherId, isWatcher = false, isAcceptedWatcher = false)
    }

    override suspend fun handleAccepting(targetId: String) {
        val watcherId = userInfoRepository.userId
        deleteRequestFromNetworkDB(targetId, watcherId)
        changeTargetWatcherRelation(targetId, watcherId, isAdding = true)

        userRepository.updateTargetStatusById(targetId, isTarget = true, isAcceptedTarget = true)
    }

    override suspend fun handleDenying(targetId: String) {
        val watcherId = userInfoRepository.userId
        deleteRequestFromNetworkDB(targetId, watcherId)

        userRepository.updateTargetStatusById(targetId, isTarget = false, isAcceptedTarget = false)
    }

    override suspend fun deleteTarget(targetId: String) {
        val watcherId = userInfoRepository.userId
        changeTargetWatcherRelation(targetId, watcherId, isAdding = false)

        userRepository.updateTargetStatusById(targetId, isTarget = false, isAcceptedTarget = false)
    }

    override suspend fun deleteWatcher(watcherId: String) {
        val targetId = userInfoRepository.userId
        changeTargetWatcherRelation(targetId, watcherId, isAdding = false)

        userRepository.updateWatcherStatusById(watcherId, isWatcher = false, isAcceptedWatcher = false)
    }

    override suspend fun handleDeletingTarget(watcherId: String) {
        val targetId = userInfoRepository.userId
        changeTargetWatcherRelation(targetId, watcherId, isAdding = false)

        userRepository.updateWatcherStatusById(watcherId, isWatcher = false, isAcceptedWatcher = false)
    }

    override suspend fun handleDeletingWatcher(targetId: String) {
        val watcherId = userInfoRepository.userId
        changeTargetWatcherRelation(targetId, watcherId, isAdding = false)

        userRepository.updateTargetStatusById(targetId, isTarget = false, isAcceptedTarget = false)
    }

    private suspend fun deleteRequestFromNetworkDB(targetId: String, watcherId: String) {
        requestNetworkDatabase.getId(targetId, watcherId)?.let { requestId ->
            requestNetworkDatabase.delete(requestId)
        }
    }

    private suspend fun changeTargetWatcherRelation(targetId: String, watcherId: String, isAdding: Boolean) {
        userInfoRepository.changeTargetWatcherRelation(targetId, watcherId, isAdding)
        userInfoRepository.loadAndUpdateUserInfo()
    }

    override suspend fun syncRequests() {
        syncIncomingRequests()
        syncOutgoingRequests()
    }

    /**
     * Проверка входящих запросов и обновление репозитория пользователей (текущий юзер - target)
     */
    private suspend fun syncIncomingRequests() {
        val targetId = userInfoRepository.userId
        val watchersIds = requestNetworkDatabase.loadRequestsWatchersIds(targetId)

        watchersIds.forEach { userId ->
            userInfoRepository.getUserInfoById(userId)?.run {
                val savedWatcher = userRepository.getByIdAsync(userId)
                val updatedWatcher = savedWatcher?.copy(isWatcher = true)
                    ?: UserEntity(id, name, phoneCode, phoneNumber, pushToken, isWatcher = true)

                userRepository.insert(updatedWatcher)
            }
        }
    }

    /**
     * Проверка исходящих запросов и обновление репозитория пользователей (текущий юзер - watcher)
     */
    private suspend fun syncOutgoingRequests() {
        val watcherId = userInfoRepository.userId
        val targetsIds = requestNetworkDatabase.loadRequestsTargetsIds(watcherId)

        targetsIds.forEach { userId ->
            userInfoRepository.getUserInfoById(userId)?.run {
                val savedTarget = userRepository.getByIdAsync(userId)
                val updatedTarget = savedTarget?.copy(isTarget = true)
                    ?: UserEntity(id, name, phoneCode, phoneNumber, pushToken, isTarget = true)

                userRepository.insert(updatedTarget)
            }
        }
    }

    /**
     * Синхронизуем локальную БД всех пользователей со списками targetsIds и watchersIds из UserInfo
     */
    override suspend fun syncUsers() {
        userInfoRepository.loadAndUpdateUserInfo()

        syncAcceptedTargets()
        syncAcceptedWatchers()

        syncSelfAccount()
    }

    /**
     * Синхронизуем локальную БД отслеживаемых пользователей со списком targetsIds.
     * Добавляем отсутствующих(новых) пользователей и скрываем лишних(удаленных) пользователей.
     */
    private suspend fun syncAcceptedTargets() {
        userInfoRepository.targetsIds.forEach {
            userInfoRepository.getUserInfoById(it)?.run {
                val user = userRepository.getByIdAsync(it)
                if (user == null) {
                    val updatedUser = UserEntity(
                        id = id,
                        name = name,
                        phoneCode = phoneCode,
                        phoneNumber = phoneNumber,
                        pushToken = pushToken,
                        isTarget = true,
                        isAcceptedTarget = true
                    )
                    userRepository.insert(updatedUser)
                } else {
                    val updatedUser = user.copy(
                        name = name,
                        phoneCode = phoneCode,
                        phoneNumber = phoneNumber,
                        pushToken = pushToken,
                        isTarget = true,
                        isAcceptedTarget = true
                    )
                    userRepository.update(updatedUser)
                }
            }
        }

        userRepository.getAcceptedTargetsAsync().forEach {
            if (!userInfoRepository.targetsIds.contains(it.id)) {
                val updatedUser = it.copy(isTarget = false, isAcceptedTarget = false)
                userRepository.update(updatedUser)
            }
        }
    }

    /**
     * Синхронизуем локальную БД наблюдающих пользователей со списком watchersIds.
     * Добавляем отсутствующих(новых) пользователей и скрываем лишних(удаленных) пользователей.
     */
    private suspend fun syncAcceptedWatchers() {
        userInfoRepository.watchersIds.forEach {
            userInfoRepository.getUserInfoById(it)?.run {
                val user = userRepository.getByIdAsync(it)
                if (user == null) {
                    val updatedUser = UserEntity(
                        id = id,
                        name = name,
                        phoneCode = phoneCode,
                        phoneNumber = phoneNumber,
                        pushToken = pushToken,
                        isWatcher = true,
                        isAcceptedWatcher = true
                    )
                    userRepository.insert(updatedUser)
                } else {
                    val updatedUser = user.copy(
                        name = name,
                        phoneCode = phoneCode,
                        phoneNumber = phoneNumber,
                        pushToken = pushToken,
                        isWatcher = true,
                        isAcceptedWatcher = true
                    )
                    userRepository.update(updatedUser)
                }
            }
        }

        userRepository.getAcceptedWatchersAsync().forEach {
            if (!userInfoRepository.watchersIds.contains(it.id)) {
                val updatedUser = it.copy(isWatcher = false, isAcceptedWatcher = false)
                userRepository.update(updatedUser)
            }
        }
    }

    /**
     * Храним сущность своего юзера локально для корректной работы SELECT запросов
     * по своему userId при JOIN'е таблиц (UserEntity + NoteEntity)
     */
    private suspend fun syncSelfAccount() {
        userInfoRepository.run {
            val selfUserEntity = UserEntity(userId, name, phoneCode, phoneNumber, pushToken)
            userRepository.insert(selfUserEntity)
        }
    }
}
