package com.sm.data.repository.userinfo

import com.sm.data.localdb.UserInfoStorage
import com.sm.data.models.UserInfoModel
import com.sm.data.networkdb.userInfo.UserInfoNetworkDatabase
import javax.inject.Inject

internal class UserInfoRepositoryImpl @Inject constructor(
    private val userInfoStorage: UserInfoStorage,
    private val userInfoNetworkDatabase: UserInfoNetworkDatabase
): UserInfoRepository {

    override var userId: String
        get() = userInfoStorage.userId
        set(value) {
            userInfoStorage.userId = value
        }

    override var name: String
        get() = userInfoStorage.name
        set(value) {
            userInfoStorage.name = value
        }

    override var phoneCode: String
        get() = userInfoStorage.phoneCode
        set(value) {
            userInfoStorage.phoneCode = value
        }

    override var phoneNumber: String
        get() = userInfoStorage.phoneNumber
        set(value) {
            userInfoStorage.phoneNumber = value
        }

    override var pushToken: String
        get() = userInfoStorage.pushToken
        set(value) {
            userInfoStorage.pushToken = value
        }

    override var watchersIds: List<String>
        get() = userInfoStorage.watchersIds
        set(value) {
            userInfoStorage.watchersIds = value
        }

    override var targetsIds: List<String>
        get() = userInfoStorage.targetsIds
        set(value) {
            userInfoStorage.targetsIds = value
        }

    override fun getPhone() = phoneCode + phoneNumber

    override suspend fun sendUserInfo() {
        val userInfo = UserInfoModel(
            id = userId,
            name = name,
            phoneCode = phoneCode,
            phoneNumber = phoneNumber,
            pushToken = pushToken,
            watchers = watchersIds,
            targets = targetsIds
        )
        userInfoNetworkDatabase.setData(userInfo)
    }

    override suspend fun updatePushToken(pushToken: String) {
        if (userId.isEmpty()) return

        userInfoNetworkDatabase.updatePushToken(userId, pushToken)
        this.pushToken = pushToken
    }

    override suspend fun updateName(name: String) {
        userInfoNetworkDatabase.updateName(userId, name)
        this.name = name
    }

    override suspend fun isExistByPhone(phoneCode: String, phoneNumber: String) =
        getUserInfoByPhone(phoneCode, phoneNumber) != null

    override suspend fun changeTargetWatcherRelation(targetId: String, watcherId: String, isAdding: Boolean) {
        val targetsIds = userInfoNetworkDatabase.getTargetsIds(watcherId)
        val watchersIds = userInfoNetworkDatabase.getWatchersIds(targetId)

        if (isAdding) {
            userInfoNetworkDatabase.addTarget(targetId, watcherId, targetsIds)
            userInfoNetworkDatabase.addWatcher(targetId, watcherId, watchersIds)
        } else {
            userInfoNetworkDatabase.deleteTarget(targetId, watcherId, targetsIds)
            userInfoNetworkDatabase.deleteWatcher(targetId, watcherId, watchersIds)
        }
    }

    override suspend fun getUserInfoByPhone(phoneCode: String, phoneNumber: String) =
        userInfoNetworkDatabase.getUserInfoByPhone(phoneCode, phoneNumber)

    override suspend fun getUserInfoById(id: String) =
        userInfoNetworkDatabase.getUserInfoById(id)

    override suspend fun loadAndUpdateUserInfo() {
        userInfoNetworkDatabase.getUserInfoById(userId)?.let {
            name = it.name
            phoneCode = it.phoneCode
            phoneNumber = it.phoneNumber
            pushToken = it.pushToken
            watchersIds = it.watchers
            targetsIds = it.targets
        }
    }

    override fun clearData() {
        userId = ""
        name = ""
        phoneCode = ""
        phoneNumber = ""
        pushToken = ""
        watchersIds = listOf()
        targetsIds = listOf()
    }
}
