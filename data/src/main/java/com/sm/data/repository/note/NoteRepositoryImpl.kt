package com.sm.data.repository.note

import android.net.Uri
import com.sm.data.localdb.dao.NoteDao
import com.sm.data.models.NoteEntity
import com.sm.data.networkdb.note.NoteNetworkDatabase
import javax.inject.Inject

internal class NoteRepositoryImpl @Inject constructor(
    private val noteDao: NoteDao,
    private val noteNetworkDatabase: NoteNetworkDatabase,
): NoteRepository {
    override fun getAllUsersNotes() = noteDao.getAllUsersNotes()

    override suspend fun insert(item: NoteEntity, imageUri: Uri?) {
        noteNetworkDatabase.insert(item, imageUri)
        noteDao.insert(item)
    }

    override suspend fun deleteOwn(item: NoteEntity) {
        noteNetworkDatabase.deleteOwn(item)
        noteDao.delete(item)
    }

    override suspend fun deleteNotOwn(item: NoteEntity, userId: String) {
        noteNetworkDatabase.deleteNotOwn(item, userId)
        noteDao.delete(item)
    }

    override suspend fun loadAndUpdateNotes(creatorId: String, userId: String) {
        val notes = noteNetworkDatabase.loadNotes(creatorId, userId)
        noteDao.updateAll(creatorId, notes)
    }
}
