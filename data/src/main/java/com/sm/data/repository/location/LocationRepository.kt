package com.sm.data.repository.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sm.data.models.LocationEntity
import com.sm.data.models.UserLocation

interface LocationRepository {
    fun getAllBetweenTimeInterval(startTime: Long, endTime: Long, id: String): LiveData<List<LocationEntity>>

    val updatedLocation: MutableLiveData<LocationEntity>

    fun listenToLocationUpdates(targetsIds: List<String>)

    fun getTargetsLastLocations(): LiveData<List<UserLocation>>

    suspend fun insert(item: LocationEntity)

    suspend fun insertLocal(item: LocationEntity)

    suspend fun loadLocations(targetId: String)
}
