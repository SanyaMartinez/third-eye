package com.sm.data.repository.user

import com.sm.data.models.UserEntity
import com.sm.data.localdb.dao.UserDao
import javax.inject.Inject

internal class UserRepositoryImpl @Inject constructor(
    private val userDao: UserDao
): UserRepository {
    override fun getById(id: String) = userDao.getById(id)

    override suspend fun getByIdAsync(id: String) = userDao.getByIdAsync(id)

    override fun getAll() = userDao.getAll()

    override fun getAllAccepted() = userDao.getAllAccepted()

    override fun getAcceptedTargets() = userDao.getAcceptedTargets()

    override suspend fun getAcceptedTargetsAsync() = userDao.getAcceptedTargetsAsync()

    override suspend fun getAcceptedWatchersAsync() = userDao.getAcceptedWatchersAsync()

    override suspend fun insert(item: UserEntity) = userDao.insert(item)

    override suspend fun update(item: UserEntity) = userDao.update(item)

    override suspend fun updateTargetStatusById(id: String, isTarget: Boolean, isAcceptedTarget: Boolean) =
        userDao.updateTargetStatusById(id, isTarget, isAcceptedTarget)

    override suspend fun updateWatcherStatusById(id: String, isWatcher: Boolean, isAcceptedWatcher: Boolean) =
        userDao.updateWatcherStatusById(id, isWatcher, isAcceptedWatcher)
}
