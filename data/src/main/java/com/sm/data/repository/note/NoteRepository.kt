package com.sm.data.repository.note

import android.net.Uri
import androidx.lifecycle.LiveData
import com.sm.data.models.NoteEntity
import com.sm.data.models.UserNote

interface NoteRepository {
    fun getAllUsersNotes(): LiveData<List<UserNote>>

    suspend fun insert(item: NoteEntity, imageUri: Uri? = null)

    suspend fun deleteOwn(item: NoteEntity)

    suspend fun deleteNotOwn(item: NoteEntity, userId: String)

    suspend fun loadAndUpdateNotes(creatorId: String, userId: String)
}
