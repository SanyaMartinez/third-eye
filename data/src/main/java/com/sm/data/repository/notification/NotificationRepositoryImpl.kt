package com.sm.data.repository.notification

import com.sm.data.models.NotificationEntity
import com.sm.data.localdb.dao.NotificationDao
import com.sm.data.networkdb.notification.NotificationNetworkDatabase
import com.sm.pushapi.managers.PushSender
import javax.inject.Inject

internal class NotificationRepositoryImpl @Inject constructor(
    private val notificationDao: NotificationDao,
    private val notificationNetworkDatabase: NotificationNetworkDatabase,
    private val pushSender: PushSender,
): NotificationRepository {

    override fun getAll() = notificationDao.getAll()

    override suspend fun sendNotification(userId: String, userPushToken: String, item: NotificationEntity): Boolean {
        val notifications = notificationNetworkDatabase.loadAll(userId)
        val newNotificationId = notifications?.size ?: 0

        item.id = newNotificationId
        notificationNetworkDatabase.insert(userId, item)

        val pushBody = item.toPushBody(pushToken = userPushToken)
        return pushSender.sendPush(pushBody)
    }

    override suspend fun loadAll(userId: String) {
        notificationNetworkDatabase.loadAll(userId)?.let {
            notificationDao.insertAll(it.toList())
        }
    }

    override suspend fun add(item: NotificationEntity) = notificationDao.insert(item)

    override suspend fun hide(userId: String, item: NotificationEntity) {
        notificationNetworkDatabase.hide(userId, item.id)
        notificationDao.hide(item.id)
    }
}
