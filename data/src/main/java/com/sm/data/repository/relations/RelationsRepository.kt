package com.sm.data.repository.relations

import com.sm.data.models.UserInfoModel

/**
 * Отвечает за создание/получение/обработку/отмену запросов на отслеживание между
 * пользователями, связь между ними, удаление пользователей и обновление UserRepository
 */
interface RelationsRepository {
    suspend fun sendRequest(targetUserInfo: UserInfoModel)

    suspend fun cancelRequest(targetId: String)

    suspend fun acceptRequest(watcherId: String)

    suspend fun denyRequest(watcherId: String)

    suspend fun handleCanceling(watcherId: String)

    suspend fun handleAccepting(targetId: String)

    suspend fun handleDenying(targetId: String)

    suspend fun deleteTarget(targetId: String)

    suspend fun deleteWatcher(watcherId: String)

    suspend fun handleDeletingTarget(watcherId: String)

    suspend fun handleDeletingWatcher(targetId: String)

    suspend fun syncRequests()

    suspend fun syncUsers()
}
