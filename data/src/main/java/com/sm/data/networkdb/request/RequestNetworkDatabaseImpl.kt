package com.sm.data.networkdb.request

import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.sm.data.models.RequestModel

/**
 * Firebase Realtime Database
 */
internal class RequestNetworkDatabaseImpl: RequestNetworkDatabase {

    private val requestDatabase = Firebase.database.reference.child(PATH_REQUEST)

    override suspend fun insert(request: RequestModel) {
        requestDatabase.push().key?.let { key ->
            val task = requestDatabase.child(key).setValue(request)
            Tasks.await(task)
        }
    }

    override suspend fun getId(targetId: String, watcherId: String): String? {
        val source: TaskCompletionSource<String> = TaskCompletionSource()

        val query = requestDatabase.orderByChild(PATH_WATCHER_ID).equalTo(watcherId)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val requestsHashMap = dataSnapshot.getValue<HashMap<String, RequestModel>>()

                    requestsHashMap?.filter { it.value.targetId == targetId }?.keys?.firstOrNull()?.let { requestId ->
                        source.setResult(requestId)
                        return
                    }
                }
                source.setResult(null)
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setException(databaseError.toException())
        })

        return Tasks.await(source.task)
    }

    override suspend fun delete(id: String) {
        val task = requestDatabase.child(id).removeValue()
        Tasks.await(task)
    }

    override suspend fun loadRequestsWatchersIds(targetId: String): List<String> {
        val source: TaskCompletionSource<List<String>> = TaskCompletionSource()

        val query = requestDatabase.orderByChild(PATH_TARGET_ID).equalTo(targetId)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val requestsHashMap = dataSnapshot.getValue<HashMap<String, RequestModel>>()
                    val requests = requestsHashMap?.values?.toList() ?: emptyList()
                    val watchersIds = requests.map { it.watcherId }

                    source.setResult(watchersIds)
                } else
                    source.setResult(emptyList())
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setException(databaseError.toException())
        })

        return Tasks.await(source.task)
    }

    override suspend fun loadRequestsTargetsIds(watcherId: String): List<String> {
        val source: TaskCompletionSource<List<String>> = TaskCompletionSource()

        val query = requestDatabase.orderByChild(PATH_WATCHER_ID).equalTo(watcherId)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val requestsHashMap = dataSnapshot.getValue<HashMap<String, RequestModel>>()
                    val requests = requestsHashMap?.values?.toList() ?: emptyList()
                    val targetsIds = requests.map { it.targetId }

                    source.setResult(targetsIds)
                } else
                    source.setResult(emptyList())
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setException(databaseError.toException())
        })

        return Tasks.await(source.task)
    }

    companion object {
        private const val PATH_REQUEST = "request"
        private const val PATH_TARGET_ID = "targetId"
        private const val PATH_WATCHER_ID = "watcherId"
    }
}
