package com.sm.data.networkdb.request

import com.sm.data.models.RequestModel

interface RequestNetworkDatabase {
    suspend fun insert(request: RequestModel)

    suspend fun getId(targetId: String, watcherId: String): String?

    suspend fun delete(id: String)

    suspend fun loadRequestsWatchersIds(targetId: String): List<String>

    suspend fun loadRequestsTargetsIds(watcherId: String): List<String>
}
