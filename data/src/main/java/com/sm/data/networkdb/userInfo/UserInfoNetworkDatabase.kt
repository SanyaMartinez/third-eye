package com.sm.data.networkdb.userInfo

import com.sm.data.models.UserInfoModel

interface UserInfoNetworkDatabase {
    suspend fun setData(userInfo: UserInfoModel)

    suspend fun updatePushToken(id: String, pushToken: String)

    suspend fun updateName(id: String, name: String)

    suspend fun getTargetsIds(watcherId: String): ArrayList<String>?

    suspend fun getWatchersIds(targetId: String): ArrayList<String>?

    suspend fun addTarget(targetId: String, watcherId: String, targetsIds: ArrayList<String>?)

    suspend fun addWatcher(targetId: String, watcherId: String, watchersIds: ArrayList<String>?)

    suspend fun deleteTarget(targetId: String, watcherId: String, targetsIds: ArrayList<String>?)

    suspend fun deleteWatcher(targetId: String, watcherId: String, watchersIds: ArrayList<String>?)

    suspend fun getUserInfoByPhone(phoneCode: String, phoneNumber: String): UserInfoModel?

    suspend fun getUserInfoById(id: String): UserInfoModel?
}
