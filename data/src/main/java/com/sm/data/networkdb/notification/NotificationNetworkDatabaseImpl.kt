package com.sm.data.networkdb.notification

import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.sm.data.models.NotificationEntity

/**
 * Firebase Realtime Database
 */
internal class NotificationNetworkDatabaseImpl: NotificationNetworkDatabase {

    private val notificationDatabase = Firebase.database.reference.child(PATH_NOTIFICATION)

    override suspend fun loadAll(userId: String): ArrayList<NotificationEntity>? {
        val source: TaskCompletionSource<ArrayList<NotificationEntity>?> = TaskCompletionSource()

        val notificationsQuery = notificationDatabase.child(userId)
        notificationsQuery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val notifications = dataSnapshot.getValue<ArrayList<NotificationEntity>>()
                    source.setResult(notifications)
                } else {
                    source.setResult(null)
                }
            }
            override fun onCancelled(databaseError: DatabaseError) = source.setResult(null)
        })

        return Tasks.await(source.task)
    }

    override suspend fun insert(userId: String, notification: NotificationEntity) {
        val task = notificationDatabase.child(userId).child(notification.id.toString()).setValue(notification)
        Tasks.await(task)
    }

    override suspend fun hide(userId: String, notificationId: Int) {
        val task = notificationDatabase.child(userId).child(notificationId.toString()).child(PATH_IS_HIDDEN).setValue(true)
        Tasks.await(task)
    }

    companion object {
        private const val PATH_NOTIFICATION = "notification"
        private const val PATH_IS_HIDDEN = "isHidden"
    }
}
