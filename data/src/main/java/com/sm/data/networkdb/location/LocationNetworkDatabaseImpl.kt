package com.sm.data.networkdb.location

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.sm.data.models.LocationEntity

/**
 * Firebase Realtime Database
 */
internal class LocationNetworkDatabaseImpl: LocationNetworkDatabase {

    private val locationDatabase = Firebase.database.reference.child(PATH_LOCATION)

    override suspend fun insert(location: LocationEntity) {
        val task = locationDatabase.child(location.userId).child(location.timestamp.toString()).setValue(location)
        Tasks.await(task)
    }

    override suspend fun loadLocations(targetId: String): List<LocationEntity> {
        val source: TaskCompletionSource<List<LocationEntity>> = TaskCompletionSource()

        val query = locationDatabase.child(targetId)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val locationsHashMap = dataSnapshot.getValue<HashMap<String, LocationEntity>>()
                    val locations = locationsHashMap?.values?.toList() ?: emptyList()
                    source.setResult(locations)
                } else
                    source.setResult(emptyList())
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setException(databaseError.toException())
        })

        return Tasks.await(source.task)
    }


    override val updatedLocation: MutableLiveData<LocationEntity> = MutableLiveData()

    private val locationsListener = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val locationsHashMap = dataSnapshot.getValue<HashMap<String, LocationEntity>>()
            val locations = locationsHashMap?.values?.maxByOrNull { it.timestamp }
            updatedLocation.postValue(locations)
        }

        override fun onCancelled(databaseError: DatabaseError) { }
    }

    override fun listenToLocationUpdates(targetsIds: List<String>) {
        targetsIds.forEach { targetId ->
            locationDatabase.child(targetId).removeEventListener(locationsListener)
            locationDatabase.child(targetId).addValueEventListener(locationsListener)
        }
    }

    companion object {
        private const val PATH_LOCATION = "location"
    }
}
