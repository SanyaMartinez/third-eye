package com.sm.data.networkdb.userInfo

import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.sm.data.models.UserInfoModel

/**
 * Firebase Realtime Database
 */
internal class UserInfoNetworkDatabaseImpl: UserInfoNetworkDatabase {

    private val userInfoDatabase = Firebase.database.reference.child(PATH_USER_INFO)

    override suspend fun setData(userInfo: UserInfoModel) {
        if (userInfo.id.isEmpty()) return
        val task = userInfoDatabase.child(userInfo.id).setValue(userInfo)
        Tasks.await(task)
    }

    override suspend fun updatePushToken(id: String, pushToken: String) {
        val task = userInfoDatabase.child(id).child(PATH_PUSH_TOKEN).setValue(pushToken)
        Tasks.await(task)
    }

    override suspend fun updateName(id: String, name: String) {
        val task = userInfoDatabase.child(id).child(PATH_NAME).setValue(name)
        Tasks.await(task)
    }

    override suspend fun getTargetsIds(watcherId: String): ArrayList<String>? {
        val source: TaskCompletionSource<ArrayList<String>?> = TaskCompletionSource()

        val targetsQuery = userInfoDatabase.child(watcherId).child(PATH_TARGETS)
        targetsQuery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val targetsIdsArrayList = dataSnapshot.getValue<ArrayList<String>>()
                    source.setResult(targetsIdsArrayList)
                } else {
                    source.setResult(null)
                }
            }
            override fun onCancelled(databaseError: DatabaseError) = source.setResult(null)
        })

        return Tasks.await(source.task)
    }

    override suspend fun getWatchersIds(targetId: String): ArrayList<String>? {
        val source: TaskCompletionSource<ArrayList<String>?> = TaskCompletionSource()

        val watchersQuery = userInfoDatabase.child(targetId).child(PATH_WATCHERS)
        watchersQuery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val watchersIdsArrayList = dataSnapshot.getValue<ArrayList<String>>()
                    source.setResult(watchersIdsArrayList)
                } else {
                    source.setResult(null)
                }
            }
            override fun onCancelled(databaseError: DatabaseError) = source.setResult(null)
        })

        return Tasks.await(source.task)
    }

    override suspend fun addTarget(targetId: String, watcherId: String, targetsIds: ArrayList<String>?) {
        val targetIndex = when {
            targetsIds == null -> "0"
            !targetsIds.contains(targetId) -> targetsIds.size.toString()
            else -> return
        }

        val task = userInfoDatabase.child(watcherId).child(PATH_TARGETS).updateChildren(mapOf(targetIndex to targetId))
        Tasks.await(task)
    }

    override suspend fun addWatcher(targetId: String, watcherId: String, watchersIds: ArrayList<String>?) {
        val watcherIndex = when {
            watchersIds == null -> "0"
            !watchersIds.contains(watcherId) -> watchersIds.size.toString()
            else -> return
        }

        val task = userInfoDatabase.child(targetId).child(PATH_WATCHERS).updateChildren(mapOf(watcherIndex to watcherId))
        Tasks.await(task)
    }

    override suspend fun deleteTarget(targetId: String, watcherId: String, targetsIds: ArrayList<String>?) {
        targetsIds?.run {
            if (!contains(targetId))
                return

            remove(targetId)
            val task = userInfoDatabase.child(watcherId).child(PATH_TARGETS).setValue(this)
            Tasks.await(task)
        }
    }

    override suspend fun deleteWatcher(targetId: String, watcherId: String, watchersIds: ArrayList<String>?) {
        watchersIds?.run {
            if (!contains(watcherId))
                return

            remove(watcherId)
            val task = userInfoDatabase.child(targetId).child(PATH_WATCHERS).setValue(this)
            Tasks.await(task)
        }
    }

    override suspend fun getUserInfoByPhone(phoneCode: String, phoneNumber: String): UserInfoModel? {
        val source: TaskCompletionSource<UserInfoModel?> = TaskCompletionSource()

        // Сначала проверяем по номеру
        val query = userInfoDatabase.orderByChild(PATH_PHONE_NUMBER).equalTo(phoneNumber)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val userInfoHashMap = dataSnapshot.getValue<HashMap<String, UserInfoModel>>()

                    // Далее проверяем по коду страны
                    userInfoHashMap?.values?.forEach { userInfo ->
                        if (userInfo.phoneCode == phoneCode) {
                            source.setResult(userInfo)
                            return
                        }
                    }

                    // Если нет совпадений по номеру / коду, то результат пуст
                    source.setResult(null)
                } else
                    source.setResult(null)
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setResult(null)
        })

        return Tasks.await(source.task)
    }

    override suspend fun getUserInfoById(id: String): UserInfoModel? {
        val source: TaskCompletionSource<UserInfoModel?> = TaskCompletionSource()

        val query = userInfoDatabase.child(id)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val userInfo = dataSnapshot.getValue<UserInfoModel>()

                    source.setResult(userInfo)
                    return
                }

                // Если не нашли пользователя - возвращаем null
                source.setResult(null)
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setResult(null)
        })

        return Tasks.await(source.task)
    }

    companion object {
        private const val PATH_USER_INFO = "userInfo"

        private const val PATH_NAME = "name"
        private const val PATH_PUSH_TOKEN = "pushToken"
        private const val PATH_PHONE_NUMBER = "phoneNumber"
        private const val PATH_WATCHERS = "watchers"
        private const val PATH_TARGETS = "targets"
    }
}
