package com.sm.data.networkdb.note

import android.net.Uri
import com.sm.data.models.NoteEntity

interface NoteNetworkDatabase {
    suspend fun insert(noteEntity: NoteEntity, imageUri: Uri?)

    suspend fun deleteOwn(noteEntity: NoteEntity)

    suspend fun deleteNotOwn(noteEntity: NoteEntity, userId: String)

    suspend fun loadNotes(creatorId: String, userId: String): List<NoteEntity>
}
