package com.sm.data.networkdb.geofence

import com.sm.data.models.GeofenceEntity

interface GeofenceNetworkDatabase {
    suspend fun insert(geofence: GeofenceEntity)

    suspend fun delete(geofence: GeofenceEntity)

    suspend fun loadGeofences(watcherId: String): List<GeofenceEntity>
}
