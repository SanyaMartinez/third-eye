package com.sm.data.networkdb.geofence

import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.sm.data.models.GeofenceEntity

/**
 * Firebase Realtime Database
 */
internal class GeofenceNetworkDatabaseImpl: GeofenceNetworkDatabase {

    private val geofenceDatabase = Firebase.database.reference.child(PATH_GEOFENCE)

    override suspend fun insert(geofence: GeofenceEntity) {
        val task = geofenceDatabase.child(geofence.watcherId).child(geofence.id.toString()).setValue(geofence)
        Tasks.await(task)
    }

    override suspend fun delete(geofence: GeofenceEntity) {
        val task = geofenceDatabase.child(geofence.watcherId).child(geofence.id.toString()).removeValue()
        Tasks.await(task)
    }

    override suspend fun loadGeofences(watcherId: String): List<GeofenceEntity> {
        val source: TaskCompletionSource<List<GeofenceEntity>> = TaskCompletionSource()

        val query = geofenceDatabase.child(watcherId)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val geofencesHashMap = dataSnapshot.getValue<HashMap<String, GeofenceEntity>>()
                    val geofences = geofencesHashMap?.values?.toList() ?: emptyList()
                    source.setResult(geofences)
                } else
                    source.setResult(emptyList())
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setException(databaseError.toException())
        })

        return Tasks.await(source.task)
    }

    companion object {
        private const val PATH_GEOFENCE = "geofence"
    }
}
