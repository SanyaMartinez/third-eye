package com.sm.data.networkdb.location

import androidx.lifecycle.MutableLiveData
import com.sm.data.models.LocationEntity

interface LocationNetworkDatabase {
    suspend fun insert(location: LocationEntity)

    suspend fun loadLocations(targetId: String): List<LocationEntity>

    val updatedLocation: MutableLiveData<LocationEntity>

    fun listenToLocationUpdates(targetsIds: List<String>)
}
