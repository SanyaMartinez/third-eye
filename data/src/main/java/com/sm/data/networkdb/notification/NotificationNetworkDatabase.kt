package com.sm.data.networkdb.notification

import com.sm.data.models.NotificationEntity

interface NotificationNetworkDatabase {

    suspend fun loadAll(userId: String): ArrayList<NotificationEntity>?

    suspend fun insert(userId: String, notification: NotificationEntity)

    suspend fun hide(userId: String, notificationId: Int)
}
