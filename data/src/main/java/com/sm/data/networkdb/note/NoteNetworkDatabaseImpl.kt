package com.sm.data.networkdb.note

import android.net.Uri
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.sm.data.models.NoteEntity

/**
 * Firebase Realtime Database & Firebase Storage
 */
internal class NoteNetworkDatabaseImpl: NoteNetworkDatabase {

    private val noteDatabase = Firebase.database.reference.child(PATH_NOTE)
    private val noteStorage = Firebase.storage.reference.child(PATH_NOTE)

    override suspend fun insert(noteEntity: NoteEntity, imageUri: Uri?) {
        imageUri?.let {
            val uploadedUri = insertImage(noteEntity, it)
            noteEntity.noteImageUri = uploadedUri.toString()
            noteEntity.noteImagePath = imageUri.lastPathSegment
        }
        insertData(noteEntity)
    }

    private suspend fun insertImage(noteEntity: NoteEntity, imageUri: Uri): Uri? {
        val source: TaskCompletionSource<Uri?> = TaskCompletionSource()

        val userIdPath = noteEntity.creatorId
        val imagePath = imageUri.lastPathSegment ?: return null
        val noteImageRef = noteStorage.child(userIdPath).child(imagePath)

        noteImageRef.putFile(imageUri).continueWithTask { noteImageRef.downloadUrl }
            .addOnSuccessListener { source.setResult(it) }
            .addOnFailureListener { source.setException(it) }

        return Tasks.await(source.task)
    }

    private suspend fun insertData(noteEntity: NoteEntity) {
        val task = noteDatabase.child(noteEntity.creatorId).child(noteEntity.noteId.toString()).setValue(noteEntity)
        Tasks.await(task)
    }

    override suspend fun deleteOwn(noteEntity: NoteEntity) {
        noteEntity.noteImagePath?.let {
            deleteImage(noteEntity.creatorId, it)
        }
        deleteData(noteEntity)
    }

    private suspend fun deleteImage(userIdPath: String, imagePath: String) {
        val noteImageRef = noteStorage.child(userIdPath).child(imagePath)

        val task = noteImageRef.delete()
            .addOnSuccessListener { }
            .addOnFailureListener { }

        Tasks.await(task)
    }

    private suspend fun deleteData(noteEntity: NoteEntity) {
        val task = noteDatabase.child(noteEntity.creatorId).child(noteEntity.noteId.toString()).removeValue()
        Tasks.await(task)
    }

    override suspend fun deleteNotOwn(noteEntity: NoteEntity, userId: String) {
        noteEntity.run {
            val updatedUsersIds = (usersIds as MutableList).apply {
                remove(userId)
            }

            val task = noteDatabase.child(creatorId).child(noteId.toString()).child(PATH_USERS_IDS).setValue(updatedUsersIds)
            Tasks.await(task)
        }
    }

    override suspend fun loadNotes(creatorId: String, userId: String): List<NoteEntity> {
        val source: TaskCompletionSource<List<NoteEntity>> = TaskCompletionSource()

        val query = noteDatabase.child(creatorId)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val notesHashMap = dataSnapshot.getValue<HashMap<String, NoteEntity>>()
                    val notes = notesHashMap?.values?.toList() ?: emptyList()
                    val result = notes.filter { it.creatorId == userId || it.usersIds.contains(userId) }
                    source.setResult(result)
                } else
                    source.setResult(emptyList())
            }

            override fun onCancelled(databaseError: DatabaseError) = source.setException(databaseError.toException())
        })

        return Tasks.await(source.task)
    }

    companion object {
        private const val PATH_NOTE = "note"
        private const val PATH_USERS_IDS = "usersIds"
    }
}
