package com.sm.data.models

data class UserInfoModel(
    val id: String = "",
    val name: String = "",
    val phoneCode: String = "",
    val phoneNumber: String = "",
    val pushToken: String = "",
    val watchers: List<String> = emptyList(),
    val targets: List<String> = emptyList(),
)
