package com.sm.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class UserEntity(
    @PrimaryKey
    val id: String,
    val name: String,
    val phoneCode: String,
    val phoneNumber: String,
    val pushToken: String,
    val isTarget: Boolean = false,
    val isAcceptedTarget: Boolean = false,
    val isWatcher: Boolean = false,
    val isAcceptedWatcher: Boolean = false,
    val imageUri: String? = null,
    val color: Int? = null,
    val nickname: String? = null,
    val notificationsEnabled: Boolean = true
): Serializable {
    fun nameToShow() = nickname ?: name
    fun getPhone() = phoneCode + phoneNumber
}
