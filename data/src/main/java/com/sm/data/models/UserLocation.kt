package com.sm.data.models

import androidx.room.Embedded

data class UserLocation (
    @Embedded
    val user: UserEntity,

    @Embedded
    val location: LocationEntity
)
