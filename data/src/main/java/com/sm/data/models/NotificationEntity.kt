package com.sm.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sm.pushapi.managers.PushBodyBuilder
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class NotificationEntity(
    @PrimaryKey
    var id: Int = 0,
    val timestamp: Long = 0L,
    val topic: String = "",
    val title: String = "",
    val body: String = "",
    val senderUserId: String = "",
    val data: String = "",
    @field:JvmField // firebase: use this annotation if your Boolean field is prefixed with 'is'
    val isHidden: Boolean = false,
): Serializable {
    fun getDateTimeString(): String = SimpleDateFormat("dd.MM.yy\nHH:mm:ss", Locale("ru")).format(Date(timestamp))

    fun toPushBody(pushToken: String) = PushBodyBuilder()
        .setNotificationTitle(title)
        .setNotificationBody(body)
        .setNotificationTopic(topic)
        .setNotificationId(id)
        .setTimestamp(timestamp)
        .setSenderUserId(senderUserId)
        .setPushData(data)
        .setPushTokens(listOf(pushToken))
        .build()
}
