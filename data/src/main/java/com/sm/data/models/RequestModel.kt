package com.sm.data.models

data class RequestModel(
    val targetId: String = "",
    val watcherId: String = ""
)
