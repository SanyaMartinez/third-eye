package com.sm.data.models

import androidx.room.Entity
import com.huawei.hms.maps.model.LatLng

@Entity(primaryKeys = ["noteId", "creatorId"])
data class NoteEntity(
    val noteId: Long = System.currentTimeMillis(),
    val title: String = "",
    val description: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val creatorId: String = "",
    val usersIds: List<String> = emptyList(),
    var noteImageUri: String? = null,
    var noteImagePath: String? = null,
) {
    fun getPosition() = LatLng(latitude, longitude)
}
