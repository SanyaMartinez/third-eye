package com.sm.data.models

import androidx.room.Embedded

data class UserNote (
    @Embedded
    val user: UserEntity,

    @Embedded
    val note: NoteEntity
)
