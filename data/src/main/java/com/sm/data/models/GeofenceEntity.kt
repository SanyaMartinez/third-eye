package com.sm.data.models

import androidx.room.Entity
import com.huawei.hms.maps.model.LatLng

@Entity(primaryKeys = ["id", "watcherId"])
data class GeofenceEntity(
    var id: Long = System.currentTimeMillis(),
    val title: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val radius: Double = 0.0,
    val watcherId: String = "",
    val targetsIds: List<String> = emptyList(),
    val color: Int = 0
) {
    fun getCenter() = LatLng(latitude, longitude)
}
