package com.sm.data.models

import androidx.room.Entity
import com.huawei.hms.maps.model.LatLng

@Entity(primaryKeys = ["userId", "timestamp"])
data class LocationEntity(
    val userId: String = "",
    val timestamp: Long = 0L,
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
) {
    fun toLatLng() = LatLng(latitude, longitude)
}
