package com.sm.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sm.data.models.LocationEntity
import com.sm.data.models.UserLocation

@Dao
interface LocationDao {
    @Query("SELECT l.*, u.* FROM LocationEntity l JOIN UserEntity u ON l.userId = u.id WHERE u.isTarget = 1 AND u.isAcceptedTarget = 1 GROUP BY l.userId")
    fun getTargetsLastLocations(): LiveData<List<UserLocation>>

    @Query("SELECT * FROM LocationEntity WHERE timestamp BETWEEN :start AND :end AND userId = :id")
    fun getAllBetweenTimeInterval(start: Long, end: Long, id: String): LiveData<List<LocationEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: LocationEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<LocationEntity>)

    @Delete
    suspend fun delete(item: LocationEntity)
}
