package com.sm.data.localdb

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.sm.core.utils.Constants
import java.lang.Exception
import javax.inject.Inject

class UserInfoStorage @Inject constructor(context: Context) {

    private val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)

    var userId: String
        get() = sharedPrefs.getString(PREFS_KEY_USER_ID, "") ?: ""
        set(value) {
            sharedPrefs.edit {
                putString(PREFS_KEY_USER_ID, value)
            }
        }

    var name: String
        get() = sharedPrefs.getString(PREFS_KEY_NAME, "") ?: ""
        set(value) {
            sharedPrefs.edit {
                putString(PREFS_KEY_NAME, value)
            }
        }

    var phoneCode: String
        get() = sharedPrefs.getString(PREFS_KEY_PHONE_CODE, "") ?: ""
        set(value) {
            sharedPrefs.edit {
                putString(PREFS_KEY_PHONE_CODE, value)
            }
        }

    var phoneNumber: String
        get() = sharedPrefs.getString(PREFS_KEY_PHONE_NUMBER, "") ?: ""
        set(value) {
            sharedPrefs.edit {
                putString(PREFS_KEY_PHONE_NUMBER, value)
            }
        }

    var pushToken: String
        get() = sharedPrefs.getString(PREFS_KEY_PUSH_TOKEN, "") ?: ""
        set(value) {
            sharedPrefs.edit {
                putString(PREFS_KEY_PUSH_TOKEN, value)
            }
        }

    var watchersIds: List<String>
        get() {
            val json = sharedPrefs.getString(PREFS_KEY_WATCHERS_IDS, "") ?: ""
            return try {
                Gson().fromJson(json, Constants.GsonType.LIST)
            } catch (e: Exception) {
                emptyList()
            }
        }
        set(value) {
            val json = Gson().toJson(value)
            sharedPrefs.edit {
                putString(PREFS_KEY_WATCHERS_IDS, json)
            }
        }

    var targetsIds: List<String>
        get() {
            val json = sharedPrefs.getString(PREFS_KEY_TARGETS_IDS, "") ?: ""
            return try {
                Gson().fromJson(json, Constants.GsonType.LIST)
            } catch (e: Exception) {
                emptyList()
            }
        }
        set(value) {
            val json = Gson().toJson(value)
            sharedPrefs.edit {
                putString(PREFS_KEY_TARGETS_IDS, json)
            }
        }

    companion object {
        const val PREFS_KEY_USER_ID = "userId"
        const val PREFS_KEY_NAME = "name"
        const val PREFS_KEY_PHONE_CODE = "phoneCode"
        const val PREFS_KEY_PHONE_NUMBER = "phoneNumber"
        const val PREFS_KEY_PUSH_TOKEN = "pushToken"
        const val PREFS_KEY_WATCHERS_IDS = "watchersIds"
        const val PREFS_KEY_TARGETS_IDS = "targetsIds"
    }
}
