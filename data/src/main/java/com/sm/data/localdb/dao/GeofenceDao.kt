package com.sm.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sm.data.models.GeofenceEntity

@Dao
interface GeofenceDao {
    @Query("SELECT * FROM GeofenceEntity WHERE id=:id")
    fun getById(id: Long): LiveData<GeofenceEntity?>

    @Query("SELECT * FROM GeofenceEntity WHERE watcherId = :userId")
    fun getAllToDisplay(userId: String): LiveData<List<GeofenceEntity>>

    // List хранить нельзя, поэтому проверяем по строке, оборачивая userId в %..%
    suspend fun getAllToTrackAsync(userId: String) = getAllToTrackByPatternAsync("%$userId%")

    @Query("SELECT * FROM GeofenceEntity WHERE targetsIds LIKE :userIdPattern")
    suspend fun getAllToTrackByPatternAsync(userIdPattern: String): List<GeofenceEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(geofence: GeofenceEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(geofences: List<GeofenceEntity>)

    @Delete
    suspend fun delete(geofence: GeofenceEntity)

    @Query("DELETE FROM GeofenceEntity WHERE watcherId = :userId")
    suspend fun deleteAll(userId: String)

    @Transaction
    suspend fun updateAll(userId: String, geofences: List<GeofenceEntity>) {
        deleteAll(userId)
        insertAll(geofences)
    }
}
