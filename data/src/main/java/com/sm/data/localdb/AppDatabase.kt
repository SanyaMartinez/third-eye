package com.sm.data.localdb

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.sm.data.localdb.AppDatabase.Companion.CURRENT_DB_VERSION
import com.sm.data.localdb.dao.*
import com.sm.data.localdb.typeconverters.ListConverter
import com.sm.data.models.*

@Database(
    entities = [
        LocationEntity::class,
        NotificationEntity::class,
        UserEntity::class,
        GeofenceEntity::class,
        NoteEntity::class,
    ],
    version = CURRENT_DB_VERSION
)
@TypeConverters(
    ListConverter::class
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun notificationDao(): NotificationDao

    abstract fun locationDao(): LocationDao

    abstract fun geofenceDao(): GeofenceDao

    abstract fun noteDao(): NoteDao

    companion object {
        private object DatabaseVersion {
            const val V1 = 1
            const val V2 = 2
            const val V3 = 3
            const val V4 = 4
            const val V5 = 5
            const val V6 = 6
        }

        const val DB_NAME = "DB_NAME"
        const val CURRENT_DB_VERSION = DatabaseVersion.V6

        val MIGRATION_1_2 = object : Migration(DatabaseVersion.V1, DatabaseVersion.V2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE UserEntity")
                database.execSQL("CREATE TABLE IF NOT EXISTS `UserEntity` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `phoneCode` TEXT NOT NULL, `phoneNumber` TEXT NOT NULL, `pushToken` TEXT NOT NULL, `isTarget` INTEGER NOT NULL, `isAcceptedTarget` INTEGER NOT NULL, `isWatcher` INTEGER NOT NULL, `isAcceptedWatcher` INTEGER NOT NULL, `imageUri` TEXT, `color` INTEGER, `nickname` TEXT, `notificationsEnabled` INTEGER NOT NULL, PRIMARY KEY(`id`))")
            }
        }

        val MIGRATION_2_3 = object : Migration(DatabaseVersion.V2, DatabaseVersion.V3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE NotificationEntity")
                database.execSQL("CREATE TABLE IF NOT EXISTS `NotificationEntity` (`id` INTEGER NOT NULL, `timestamp` INTEGER NOT NULL, `topic` TEXT NOT NULL, `title` TEXT NOT NULL, `body` TEXT NOT NULL, `data` TEXT NOT NULL, PRIMARY KEY(`id`))")
            }
        }

        val MIGRATION_3_4 = object : Migration(DatabaseVersion.V3, DatabaseVersion.V4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE IF EXISTS `NotificationEntity`")
                database.execSQL("CREATE TABLE IF NOT EXISTS `NotificationEntity` (`id` INTEGER NOT NULL, `timestamp` INTEGER NOT NULL, `topic` TEXT NOT NULL, `title` TEXT NOT NULL, `body` TEXT NOT NULL, `senderUserId` TEXT NOT NULL, `data` TEXT NOT NULL, PRIMARY KEY(`id`))")
            }
        }

        val MIGRATION_4_5 = object : Migration(DatabaseVersion.V4, DatabaseVersion.V5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `NotificationEntity` ADD `isHidden` INTEGER DEFAULT 0 NOT NULL")
            }
        }

        val MIGRATION_5_6 = object : Migration(DatabaseVersion.V5, DatabaseVersion.V6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS `NoteEntity` (`noteId` INTEGER NOT NULL, `title` TEXT NOT NULL, `description` TEXT NOT NULL, `latitude` REAL NOT NULL, `longitude` REAL NOT NULL, `creatorId` TEXT NOT NULL, `usersIds` TEXT NOT NULL, `noteImageUri` TEXT, `noteImagePath` TEXT, PRIMARY KEY(`noteId`, `creatorId`))")
            }
        }
    }
}
