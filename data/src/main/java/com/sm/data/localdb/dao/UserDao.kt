package com.sm.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sm.data.models.UserEntity

@Dao
interface UserDao {
    @Query("SELECT * FROM UserEntity where id=:id")
    fun getById(id: String): LiveData<UserEntity?>

    @Query("SELECT * FROM UserEntity where id=:id")
    suspend fun getByIdAsync(id: String): UserEntity?

    @Query("SELECT * FROM UserEntity")
    fun getAll(): LiveData<List<UserEntity>>

    @Query("SELECT * FROM UserEntity WHERE isAcceptedTarget = 1 OR isAcceptedWatcher = 1")
    fun getAllAccepted(): LiveData<List<UserEntity>>

    @Query("SELECT * FROM UserEntity WHERE isTarget = 1 AND isAcceptedTarget = 1")
    fun getAcceptedTargets(): LiveData<List<UserEntity>>

    @Query("SELECT * FROM UserEntity WHERE isTarget = 1 AND isAcceptedTarget = 1")
    suspend fun getAcceptedTargetsAsync(): List<UserEntity>

    @Query("SELECT * FROM UserEntity WHERE isWatcher = 1 AND isAcceptedWatcher = 1")
    suspend fun getAcceptedWatchersAsync(): List<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: UserEntity)

    @Update
    suspend fun update(user: UserEntity)

    @Query("UPDATE UserEntity SET isTarget=:isTarget, isAcceptedTarget=:isAcceptedTarget WHERE id=:id")
    suspend fun updateTargetStatusById(id: String, isTarget: Boolean, isAcceptedTarget: Boolean)

    @Query("UPDATE UserEntity SET isWatcher=:isWatcher, isAcceptedWatcher=:isAcceptedWatcher WHERE id=:id")
    suspend fun updateWatcherStatusById(id: String, isWatcher: Boolean, isAcceptedWatcher: Boolean)
}
