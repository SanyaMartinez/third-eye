package com.sm.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sm.data.models.NotificationEntity

@Dao
interface NotificationDao {
    @Query("SELECT * FROM NotificationEntity WHERE id=:id")
    fun getById(id: Int): LiveData<NotificationEntity?>

    @Query("SELECT * FROM NotificationEntity WHERE isHidden == 0 ORDER BY timestamp DESC")
    fun getAll(): LiveData<List<NotificationEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(notification: NotificationEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(notifications: List<NotificationEntity>)

    @Query("UPDATE NotificationEntity SET isHidden = 1 WHERE id=:notificationId")
    suspend fun hide(notificationId: Int)
}
