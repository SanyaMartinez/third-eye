package com.sm.data.localdb.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.sm.core.utils.Constants

class ListConverter {
    @TypeConverter
    fun fromListToString(list: List<String>): String = Gson().toJson(list, Constants.GsonType.LIST)

    @TypeConverter
    fun fromStringToList(json: String): List<String> = Gson().fromJson(json, Constants.GsonType.LIST)
}
