package com.sm.data.localdb.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sm.data.models.NoteEntity
import com.sm.data.models.UserNote

@Dao
interface NoteDao {
    @Query("SELECT n.*, u.* FROM NoteEntity n JOIN UserEntity u ON n.creatorId = u.id GROUP BY n.creatorId")
    fun getAllUsersNotes(): LiveData<List<UserNote>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(note: NoteEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(notes: List<NoteEntity>)

    @Delete
    suspend fun delete(note: NoteEntity)

    @Query("DELETE FROM NoteEntity WHERE creatorId = :userId")
    suspend fun deleteAll(userId: String)

    @Transaction
    suspend fun updateAll(userId: String, notes: List<NoteEntity>) {
        deleteAll(userId)
        insertAll(notes)
    }
}
