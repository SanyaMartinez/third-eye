package com.sm.data

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.sm.data.localdb.AppDatabase
import com.sm.data.localdb.dao.LocationDao
import com.sm.data.models.LocationEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LocationDaoTest {
    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var locationDao: LocationDao

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room
            .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()

        locationDao = appDatabase.locationDao()
    }

    @Test
    fun insertAndGetAllBetweenTimeInterval() {
        val userId = "1"
        val time1 = System.currentTimeMillis()
        val time2 = time1 + 1000L
        val time3 = time1 + 1000000L

        val location1 = LocationEntity(userId, time1, latitude = 100.0, longitude = 100.0)
        val location2 = LocationEntity(userId, time2, latitude = 100.0, longitude = 100.0)
        val location3 = LocationEntity(userId, time3, latitude = 100.0, longitude = 100.0)

        val allLocations = listOf(location1, location2, location3)

        runBlocking {
            locationDao.insertAll(allLocations)
        }

        val startTime = time1
        val endTime = time2
        val locationsInInterval = listOf(location1, location2)

        val liveData = locationDao.getAllBetweenTimeInterval(startTime, endTime, userId)
        val dbLocations = getValue(liveData)

        assertTrue(dbLocations?.size == 2)
        assertEquals(locationsInInterval, dbLocations)
    }

    @After
    fun closeDb() = appDatabase.close()
}
