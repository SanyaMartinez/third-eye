package com.sm.data

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.sm.data.localdb.AppDatabase
import com.sm.data.localdb.dao.UserDao
import com.sm.data.models.UserEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserDaoTest {
    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var userDao: UserDao

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room
            .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()

        userDao = appDatabase.userDao()
    }

    @Test
    fun insertAndGetAll() {
        val user1 = UserEntity(
            id = "1",
            name = "user1",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "1",
            isTarget = true,
            isAcceptedTarget = true,
            isWatcher = false,
            isAcceptedWatcher = false
        )
        val user2 = UserEntity(
            id = "2",
            name = "user2",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "2",
            isTarget = false,
            isAcceptedTarget = false,
            isWatcher = true,
            isAcceptedWatcher = true
        )

        runBlocking {
            userDao.insert(user1)
            userDao.insert(user2)
        }

        val users = listOf(user1, user2)

        val liveData = userDao.getAll()
        val dbUsers = getValue(liveData)

        assertEquals(users, dbUsers)
    }

    @Test
    fun insertAndGetAcceptedTargetsAndWatchers() {
        val requestedTarget = UserEntity(
            id = "1",
            name = "requestedTarget",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "1",
            isTarget = true,
            isAcceptedTarget = false,
            isWatcher = false,
            isAcceptedWatcher = false
        )
        val acceptedTarget = UserEntity(
            id = "2",
            name = "acceptedTarget",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "2",
            isTarget = true,
            isAcceptedTarget = true,
            isWatcher = false,
            isAcceptedWatcher = false
        )
        val requestedWatcher = UserEntity(
            id = "3",
            name = "requestedWatcher",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "3",
            isTarget = false,
            isAcceptedTarget = false,
            isWatcher = true,
            isAcceptedWatcher = false
        )
        val acceptedWatcher = UserEntity(
            id = "4",
            name = "acceptedWatcher",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "4",
            isTarget = false,
            isAcceptedTarget = false,
            isWatcher = true,
            isAcceptedWatcher = true
        )

        runBlocking {
            userDao.insert(requestedTarget)
            userDao.insert(acceptedTarget)
            userDao.insert(requestedWatcher)
            userDao.insert(acceptedWatcher)

            val acceptedTargets = listOf(acceptedTarget)
            val dbAcceptedTargets = userDao.getAcceptedTargetsAsync()

            assertEquals(acceptedTargets, dbAcceptedTargets)

            val acceptedWatchers = listOf(acceptedWatcher)
            val dbAcceptedWatchers = userDao.getAcceptedWatchersAsync()

            assertEquals(acceptedWatchers, dbAcceptedWatchers)
        }
    }

    @Test
    fun updateAndGetById() {
        val userId = "1"
        val user = UserEntity(
            id = userId,
            name = "user",
            phoneCode = "+7",
            phoneNumber = "1",
            pushToken = "1",
            isTarget = false,
            isAcceptedTarget = false,
            isWatcher = false,
            isAcceptedWatcher = false
        )

        runBlocking {
            userDao.insert(user)

            val updatedUser = user.copy(
                name = "new name",
                phoneCode = "+7",
                phoneNumber = "8005553535",
                isWatcher = true
            )

            userDao.update(updatedUser)

            val dbUser = userDao.getByIdAsync(userId)

            assertEquals(updatedUser, dbUser)
        }
    }

    @After
    fun closeDb() = appDatabase.close()
}
