package com.sm.data

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.sm.data.localdb.AppDatabase
import com.sm.data.localdb.dao.NotificationDao
import com.sm.data.models.NotificationEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NotificationDaoTest {
    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var notificationDao: NotificationDao

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room
            .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()

        notificationDao = appDatabase.notificationDao()
    }

    @Test
    fun insertAndGetById() {
        val id = 1
        val notification = NotificationEntity(
            id = id,
            timestamp = System.currentTimeMillis(),
            topic = "Test",
            title = "Unreal title",
            body = "Unreal body",
            senderUserId = "zxc",
            data = "",
        )

        runBlocking {
            notificationDao.insert(notification)
        }

        val liveData = notificationDao.getById(id)
        val dbNotification = getValue(liveData)

        assertEquals(notification, dbNotification)
    }

    @After
    fun closeDb() = appDatabase.close()
}
