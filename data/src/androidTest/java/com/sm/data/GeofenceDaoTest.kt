package com.sm.data

import android.content.Context
import androidx.room.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.sm.data.localdb.AppDatabase
import com.sm.data.localdb.dao.GeofenceDao
import com.sm.data.models.GeofenceEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GeofenceDaoTest {
    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var geofenceDao: GeofenceDao

    @Before
    fun createDb() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room
            .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()

        geofenceDao = appDatabase.geofenceDao()
    }

    @Test
    fun insertGetTrackedAndDelete() {
        val userTargetId = "1"
        val otherTargetId = "2"
        val watcherId = "3"

        val geofence1 = GeofenceEntity(
            id = 1,
            title = "geofence1",
            latitude = 55.0,
            longitude = 55.0,
            radius = 100.0,
            watcherId = watcherId,
            targetsIds = listOf(userTargetId),
            color = 0
        )

        val geofence2 = GeofenceEntity(
            id = 2,
            title = "geofence2",
            latitude = 55.0,
            longitude = 55.0,
            radius = 100.0,
            watcherId = watcherId,
            targetsIds = listOf(userTargetId, otherTargetId),
            color = 0
        )

        val geofence3 = GeofenceEntity(
            id = 3,
            title = "geofence3",
            latitude = 55.0,
            longitude = 55.0,
            radius = 100.0,
            watcherId = watcherId,
            targetsIds = listOf(otherTargetId),
            color = 0
        )

        val allGeofences = listOf(geofence1, geofence2, geofence3)
        val trackedGeofences = listOf(geofence1, geofence2)

        runBlocking {
            geofenceDao.insertAll(allGeofences)
            val dbGeofencesAfterInsert = geofenceDao.getAllToTrackAsync(userTargetId)

            assertEquals(2, dbGeofencesAfterInsert.size)
            assertTrue(dbGeofencesAfterInsert.containsAll(trackedGeofences))

            geofenceDao.deleteAll(watcherId)
            val dbGeofencesAfterDelete = geofenceDao.getAllToTrackAsync(userTargetId)

            assertTrue(dbGeofencesAfterDelete.isEmpty())
        }
    }

    @After
    fun closeDb() = appDatabase.close()
}
