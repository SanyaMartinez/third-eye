package com.sm.core.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class ExtensionsTest {
    @Test
    fun tag_isCorrect() {
        assertEquals("String", String().TAG)
        assertEquals("Date", Date().TAG)
        assertEquals("AppCompatActivity", AppCompatActivity().TAG)
        assertEquals("Fragment", Fragment().TAG)
    }
}
