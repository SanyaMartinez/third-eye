package com.sm.core.utils

import android.graphics.Color
import org.junit.Test

import org.junit.Assert.*

class UtilsTest {

    @Test
    fun getHexColorFromInt_isCorrect() {
        assertEquals("#FFFFFF", getHexColorFromInt(Color.WHITE))
        assertEquals("#000000", getHexColorFromInt(Color.BLACK))
        assertEquals("#FF0000", getHexColorFromInt(Color.RED))
        assertEquals("#00FF00", getHexColorFromInt(Color.GREEN))
        assertEquals("#0000FF", getHexColorFromInt(Color.BLUE))
    }

    @Test
    fun getOpacityHexValue_isCorrect() {
        assertEquals("ff", getOpacityHexValue(0.0))
        assertEquals("cc", getOpacityHexValue(0.2))
        assertEquals("80", getOpacityHexValue(0.5))
        assertEquals("4d", getOpacityHexValue(0.7))
    }

    @Test
    fun toMillis_isCorrect() {
        assertEquals(0L, toMillis(0))
        assertEquals(1_000L, toMillis(1))
        assertEquals(999_000L, toMillis(999))
    }
}
