package com.sm.core.utils

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

object Constants {
    object FormatPatterns {
        const val DATE = "EEE, d MMMM, yyyy"
        const val TIME = "HH:mm"
    }

    object GsonType {
        val LIST: Type = object : TypeToken<List<String>>() {}.type
    }

    object HmsValues {
        const val API_KEY = "CgB6e3x9nJUNKjQQ8TXlxDedeMN2aS296Jr18gGxjSFDHz/HCzk9jvxNzNMN9i36qt0H9eGVmRQnLYUy7IdbjigT"
    }
}
