package com.sm.core.utils

import android.graphics.Color
import java.lang.Exception
import kotlin.math.roundToInt

fun getHexColorFromInt(color: Int) = String.format("#%06X", 0xFFFFFF and color)

fun getTransparentColor(color: Int, transparency: Double): Int {
    if (transparency !in 0.0..1.0) throw Exception("Transparency should be between 0.0 and 1.0")

    val opacityHexValue = getOpacityHexValue(transparency)
    val hexColor = getHexColorFromInt(color)

    val transparentHexColor = hexColor.first() + opacityHexValue + hexColor.substring(1)
    return Color.parseColor(transparentHexColor)
}

fun getOpacityHexValue(transparency: Double): String {
    val opaque = 1 - transparency
    val intValue = (255 * opaque).roundToInt()
    return intValue.toString(16)
}

private const val TO_MILLIS_MULTIPLIER = 1_000L

fun toMillis(seconds: Int) = seconds * TO_MILLIS_MULTIPLIER
