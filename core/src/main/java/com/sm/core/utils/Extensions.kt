package com.sm.core.utils

import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import java.text.SimpleDateFormat
import java.util.*

val Any.TAG: String
    get() = this::class.java.simpleName

fun Context.isPermissionGranted(permission: String) =
    ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun Fragment.showToast(
    @StringRes resId: Int,
    duration: Int = Toast.LENGTH_SHORT
) = showToast(getString(resId), duration)

fun Fragment.showToast(
    text: String,
    duration: Int = Toast.LENGTH_SHORT
) = Toast.makeText(requireContext(), text, duration).show()

fun Calendar.formatToString(pattern: String): String =
    SimpleDateFormat(pattern, Locale.getDefault()).format(this.time)
