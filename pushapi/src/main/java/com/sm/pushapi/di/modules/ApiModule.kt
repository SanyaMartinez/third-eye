package com.sm.pushapi.di.modules

import android.content.Context
import com.sm.pushapi.apiservices.AuthApiService
import com.sm.pushapi.apiservices.PushApiService
import com.sm.pushapi.managers.PushDataStorage
import com.sm.pushapi.utils.ApiConstants
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideClient(headersInterceptor: Interceptor): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(headersInterceptor)
        .build()

    @Provides
    @Singleton
    fun provideAuthApiService(client: OkHttpClient): AuthApiService = Retrofit.Builder()
        .baseUrl(ApiConstants.BaseUrls.AUTH)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AuthApiService::class.java)

    @Provides
    @Singleton
    fun providePushApiService(client: OkHttpClient): PushApiService = Retrofit.Builder()
        .baseUrl(ApiConstants.BaseUrls.PUSH)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(PushApiService::class.java)

    @Provides
    @Singleton
    fun provideHeadersInterceptor(context: Context) = Interceptor {
        val request = it.request()
        val requestBuilder = request.newBuilder()

        PushDataStorage.getToken(context)?.let { bearerToken ->
            requestBuilder.addHeader(ApiConstants.HeadersName.AUTHORIZATION, bearerToken)
        }

        it.proceed(requestBuilder.build())
    }
}
