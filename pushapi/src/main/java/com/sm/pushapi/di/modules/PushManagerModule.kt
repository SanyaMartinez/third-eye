package com.sm.pushapi.di.modules

import android.content.Context
import com.sm.pushapi.apiservices.AuthApiService
import com.sm.pushapi.apiservices.PushApiService
import com.sm.pushapi.managers.PushDataStorage
import com.sm.pushapi.managers.PushSender
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PushManagerModule {
    @Provides
    @Singleton
    fun providePushNotificationSender(
        pushDataStorage: PushDataStorage,
        authApiService: AuthApiService,
        pushApiService: PushApiService
    ) = PushSender(pushDataStorage, authApiService, pushApiService)

    @Provides
    @Singleton
    fun providePushDataStorage(context: Context) = PushDataStorage(context)
}
