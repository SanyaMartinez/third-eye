package com.sm.pushapi.models

data class PushBody(
    val message: Message
)

data class Message(
    val `data`: String,
    val token: List<String>
)
