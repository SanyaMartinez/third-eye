package com.sm.pushapi.models

data class SendPushResponse(
    val code: String,
    val msg: String
)
