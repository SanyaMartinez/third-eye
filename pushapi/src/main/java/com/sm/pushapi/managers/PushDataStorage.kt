package com.sm.pushapi.managers

import android.content.Context
import androidx.core.content.edit
import com.sm.core.utils.toMillis
import javax.inject.Inject

class PushDataStorage @Inject constructor(context: Context) {

    private val sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    private var tokenExpiringTime
        get() = sharedPreferences.getLong(PREFS_KEY_TIME, 0)
        set(value) = sharedPreferences.edit { putLong(PREFS_KEY_TIME, value) }

    fun needUpdateToken() = System.currentTimeMillis() > tokenExpiringTime

    fun updateToken(accessToken: String) = sharedPreferences.edit {
        val token = accessToken.replace("\\", "")
        putString(PREFS_KEY_TOKEN, "Bearer $token")
    }

    fun updateTime(ttlInSeconds: Int) {
        tokenExpiringTime = System.currentTimeMillis() + toMillis(ttlInSeconds)
    }

    companion object {
        private const val PREFS_NAME = "PushApi"
        private const val PREFS_KEY_TIME = "expiringTime"
        private const val PREFS_KEY_TOKEN = "bearerToken"

        fun getToken(context: Context) = context
            .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
            .getString(PREFS_KEY_TOKEN, null)
    }
}
