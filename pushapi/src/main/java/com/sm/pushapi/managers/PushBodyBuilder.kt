package com.sm.pushapi.managers

import com.sm.pushapi.models.Message
import com.sm.pushapi.models.PushBody
import com.sm.pushapi.utils.PushConstants

class PushBodyBuilder {
    private var notificationTitle: String = ""
    private var notificationBody: String = ""
    private var notificationTopic: String = ""
    private var notificationId: Int = 0
    private var timestamp: Long = System.currentTimeMillis()
    private var senderUserId: String = ""
    private var pushData: String = ""
    private var pushTokens: List<String> = emptyList()

    fun setNotificationTitle(title: String) = this.apply { notificationTitle = title }

    fun setNotificationBody(body: String) = this.apply { notificationBody = body }

    fun setNotificationTopic(topic: String) = this.apply { notificationTopic = topic }

    fun setNotificationId(id: Int) = this.apply { notificationId = id }

    fun setTimestamp(millis: Long) = this.apply { timestamp = millis }

    fun setSenderUserId(userId: String) = this.apply { senderUserId = userId }

    fun setPushData(data: String) = this.apply { pushData = data }

    fun setPushTokens(tokens: List<String>) = this.apply { pushTokens = tokens }

    fun build(): PushBody = PushBody(
        message = Message(
            data = "{" +
                "'${PushConstants.Keys.TOPIC}': '$notificationTopic', " +
                "'${PushConstants.Keys.ID}': '$notificationId', " +
                "'${PushConstants.Keys.TITLE}': '$notificationTitle', " +
                "'${PushConstants.Keys.BODY}': '$notificationBody', " +
                "'${PushConstants.Keys.TIMESTAMP}': '$timestamp', " +
                "'${PushConstants.Keys.SENDER_USER_ID}': '$senderUserId', " +
                "'${PushConstants.Keys.PUSH_DATA}': \"$pushData\"" +
                "}",
            token = pushTokens
        )
    )
}
