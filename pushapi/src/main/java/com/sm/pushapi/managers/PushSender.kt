package com.sm.pushapi.managers

import android.util.Log
import com.sm.core.utils.TAG
import com.sm.pushapi.apiservices.AuthApiService
import com.sm.pushapi.apiservices.PushApiService
import com.sm.pushapi.models.*
import java.lang.Exception
import javax.inject.Inject

class PushSender @Inject constructor(
    private val pushDataStorage: PushDataStorage,
    private val authApiService: AuthApiService,
    private val pushApiService: PushApiService
) {

    suspend fun sendPush(pushBody: PushBody): Boolean {
        try {
            Log.d(TAG, "Loading")
            if (pushDataStorage.needUpdateToken()) {
                authApiService.auth().let {
                    Log.d(TAG, "Success auth response: $it")
                    pushDataStorage.updateToken(it.accessToken)
                    pushDataStorage.updateTime(it.expiresIn)
                }
            }

            pushApiService.sendPush(pushBody).let {
                Log.d(TAG, "Success send push response: $it")
                return true
            }
        } catch (e: Exception) {
            Log.w(TAG, "Error:", e)
            return false
        }
    }
}
