package com.sm.pushapi.utils

internal object ApiConstants {
    object BaseUrls {
        const val AUTH = "https://oauth-login.cloud.huawei.com"
        const val PUSH = "https://push-api.cloud.huawei.com"
    }

    object Params {
        const val APP_ID = "103030401"

        const val GRANT_TYPE = "client_credentials"
        const val CLIENT_ID = APP_ID
        const val CLIENT_SECRET = "2fb216d03481df17c843a98c13962432ba588e2ebea2cbec4b0bb960aabbc609"
    }

    object HeadersName {
        const val AUTHORIZATION = "Authorization"
    }
}
