package com.sm.pushapi.utils

object PushConstants {
    object Topics {
        const val DEFAULT = "Default"
        const val GEOFENCE_UPDATE = "GeofenceUpdate"
        const val GEOFENCE_CONVERSION = "GeofenceConversion"
        const val NOTE_ADD = "NoteAdd"
        const val WATCHER_REQUEST = "WatcherRequest"
        const val WATCHER_DENY_REQUEST = "WatcherDenyRequest"
        const val WATCHER_ACCEPT_REQUEST = "WatcherAcceptRequest"
        const val WATCHER_CANCEL_REQUEST = "WatcherCancelRequest"
        const val SOUND_ALERT = "SoundAlert"
        const val MESSAGE = "Message"
        const val DELETE_TARGET = "DeleteTarget"
        const val DELETE_WATCHER = "DeleteWatcher"
        const val ACTIVITY_CONVERSION = "ActivityConversion"
    }

    object Keys {
        const val ID = "id"
        const val TITLE = "title"
        const val BODY = "body"
        const val TIMESTAMP = "timestamp"
        const val TOPIC = "topic"
        const val SENDER_USER_ID = "senderUserId"
        const val PUSH_DATA = "pushData"
    }

    object DataKeys {
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val ACTIVITY_TYPE = "activityType"
    }
}
