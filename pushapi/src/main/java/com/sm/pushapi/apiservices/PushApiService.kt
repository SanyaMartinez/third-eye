package com.sm.pushapi.apiservices

import com.sm.pushapi.models.PushBody
import com.sm.pushapi.models.SendPushResponse
import com.sm.pushapi.utils.ApiConstants
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface PushApiService {
    @POST("/v1/{appId}/messages:send")
    suspend fun sendPush(
        @Body body: PushBody,
        @Path("appId") appId: String = ApiConstants.Params.APP_ID
    ): SendPushResponse
}
