package com.sm.pushapi.apiservices

import com.sm.pushapi.models.AuthResponse
import com.sm.pushapi.utils.ApiConstants
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthApiService {
    @POST("/oauth2/v3/token")
    @FormUrlEncoded
    suspend fun auth(
        @Field("grant_type") grantType: String = ApiConstants.Params.GRANT_TYPE,
        @Field("client_id") clientId: String = ApiConstants.Params.CLIENT_ID,
        @Field("client_secret") clientSecret: String = ApiConstants.Params.CLIENT_SECRET
    ): AuthResponse
}
