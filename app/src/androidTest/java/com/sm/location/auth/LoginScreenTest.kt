package com.sm.location.auth

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sm.location.R
import com.sm.location.checkToastWithText
import com.sm.location.ui.auth.AuthActivity
import com.sm.location.waitForDelay
import org.hamcrest.core.StringContains.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginScreenTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(AuthActivity::class.java)

    @Test
    fun testLoginScreen() {
        testLoginScreenViewsVisibility()
        testLoginScreenCountryPicker()
        testLoginScreenEnterPhone()
    }

    private fun testLoginScreenViewsVisibility() {
        val viewIds = listOf(
            R.id.authLoginCountryCodePicker,
            R.id.authLoginPhoneNumberEditText,
            R.id.authLoginPhoneFromContactsText,
            R.id.authLoginButtonContinue,
            R.id.authLoginButtonRegistration,
        )
        viewIds.forEach {
            onView(withId(it)).check(matches(isDisplayed()))
        }
    }

    private fun testLoginScreenCountryPicker() {
        onView(withId(R.id.authLoginCountryCodePicker)).perform(click())
        waitForDelay()
        onView(withId(R.id.cardViewRoot)).check(matches(isDisplayed()))
        onView(withText("Belarus (BY)")).check(matches(isDisplayed())).perform(click())
        waitForDelay()
        onView(withId(R.id.textView_selectedCountry)).check(matches(withText(containsString("+375"))))
        onView(withId(R.id.authLoginCountryCodePicker)).perform(click())
        waitForDelay()
        onView(withId(R.id.cardViewRoot)).check(matches(isDisplayed()))
        onView(withText("Russian Federation (RU)")).check(matches(isDisplayed())).perform(click())
        waitForDelay()
        onView(withId(R.id.textView_selectedCountry)).check(matches(withText(containsString("+7"))))
    }

    private fun testLoginScreenEnterPhone() {
        onView(withId(R.id.authLoginPhoneNumberEditText))
            .perform(clearText(), typeText("1234"), pressImeActionButton())
        onView(withId(R.id.authLoginButtonContinue)).perform(click())
        checkToastWithText(R.string.auth_login_phone_error_text)
    }
}
