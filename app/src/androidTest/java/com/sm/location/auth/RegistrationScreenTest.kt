package com.sm.location.auth

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sm.location.R
import com.sm.location.checkToastWithText
import com.sm.location.ui.auth.AuthActivity
import com.sm.location.waitForDelay
import org.hamcrest.core.StringContains.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RegistrationScreenTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(AuthActivity::class.java)

    @Test
    fun testRegistrationScreen() {
        testRegistrationScreenViewsVisibility()
        testRegistrationScreenCountryPicker()
        testRegistrationScreenEnterName()
    }

    private fun testRegistrationScreenViewsVisibility() {
        onView(withId(R.id.authLoginButtonRegistration)).perform(click())
        waitForDelay()

        val viewIds = listOf(
            R.id.authRegistrationNameEditText,
            R.id.authRegistrationCountryCodePicker,
            R.id.authRegistrationPhoneNumberEditText,
            R.id.authRegistrationPhoneFromContactsText,
            R.id.authRegistrationButton,
        )
        viewIds.forEach {
            onView(withId(it)).check(matches(isDisplayed()))
        }
    }

    private fun testRegistrationScreenCountryPicker() {
        onView(withId(R.id.authRegistrationCountryCodePicker)).perform(click())
        waitForDelay()
        onView(withId(R.id.cardViewRoot)).check(matches(isDisplayed()))
        onView(withText("Belarus (BY)")).check(matches(isDisplayed())).perform(click())
        waitForDelay()
        onView(withId(R.id.textView_selectedCountry)).check(matches(withText(containsString("+375"))))
        onView(withId(R.id.authRegistrationCountryCodePicker)).perform(click())
        waitForDelay()
        onView(withId(R.id.cardViewRoot)).check(matches(isDisplayed()))
        onView(withText("Russian Federation (RU)")).check(matches(isDisplayed())).perform(click())
        waitForDelay()
        onView(withId(R.id.textView_selectedCountry)).check(matches(withText(containsString("+7"))))
    }

    private fun testRegistrationScreenEnterName() {
        onView(withId(R.id.authRegistrationNameEditText))
            .perform(clearText(), typeText("New Test User"), pressImeActionButton())
        waitForDelay()
        onView(withId(R.id.authRegistrationPhoneNumberEditText))
            .perform(clearText(), typeText("1234"), pressImeActionButton())
        waitForDelay()
        onView(withId(R.id.authRegistrationButton)).perform(click())
        checkToastWithText(R.string.auth_registration_phone_error_text)
    }
}
