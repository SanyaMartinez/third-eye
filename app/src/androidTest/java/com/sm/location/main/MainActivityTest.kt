package com.sm.location.main

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sm.location.R
import com.sm.location.ui.main.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testBottomNav() {
        onView(withId(R.id.bottomNavigationView)).check(matches(isDisplayed()))

        val bottomMenuViewsIds = listOf(
            R.id.navigationProfile,
            R.id.navigationHistory,
            R.id.navigationUsers,
            R.id.navigationMap
        )
        bottomMenuViewsIds.forEach {
            onView(withId(it)).check(matches(isDisplayed())).check(matches(isClickable()))
        }
    }
}