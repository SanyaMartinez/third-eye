package com.sm.location.main

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sm.location.R
import com.sm.location.checkToastWithText
import com.sm.location.ui.main.MainActivity
import com.sm.location.waitForDelay
import org.hamcrest.core.StringContains
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UsersScreenTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testUsersScreen() {
        onView(withId(R.id.navigationUsers)).perform(click())
        onView(withId(R.id.fabAddUser))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        testUsersAddingScreen()
    }

    private fun testUsersAddingScreen() {
        val viewIds = listOf(
            R.id.addingCountryCodePicker,
            R.id.addingPhoneNumberEditText,
            R.id.addingPhoneFromContactsText,
            R.id.buttonSendRequest,
        )
        viewIds.forEach {
            onView(withId(it)).check(matches(isDisplayed()))
        }

        testUsersAddingScreenCountryPicker()
        testUsersAddingScreenEnterPhone()
    }

    private fun testUsersAddingScreenCountryPicker() {
        onView(withId(R.id.addingCountryCodePicker)).perform(click())
        waitForDelay()
        onView(withId(R.id.cardViewRoot)).check(matches(isDisplayed()))
        onView(withText("Belarus (BY)")).check(matches(isDisplayed())).perform(click())
        waitForDelay()
        onView(withId(R.id.textView_selectedCountry)).check(matches(withText(StringContains.containsString("+375"))))
        onView(withId(R.id.addingCountryCodePicker)).perform(click())
        waitForDelay()
        onView(withId(R.id.cardViewRoot)).check(matches(isDisplayed()))
        onView(withText("Russian Federation (RU)")).check(matches(isDisplayed())).perform(click())
        waitForDelay()
        onView(withId(R.id.textView_selectedCountry)).check(matches(withText(StringContains.containsString("+7"))))
    }

    private fun testUsersAddingScreenEnterPhone() {
        onView(withId(R.id.addingPhoneNumberEditText))
            .perform(ViewActions.clearText(), ViewActions.typeText("1234"), ViewActions.pressImeActionButton())
        onView(withId(R.id.buttonSendRequest)).perform(click())
        checkToastWithText(R.string.users_adding_error_text)
    }
}