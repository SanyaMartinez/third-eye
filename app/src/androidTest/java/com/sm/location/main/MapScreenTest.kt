package com.sm.location.main

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sm.location.R
import com.sm.location.clickXY
import com.sm.location.ui.main.MainActivity
import com.sm.location.waitForDelay
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MapScreenTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testMapScreen() {
        onView(withId(R.id.navigationMap)).perform(click())
        onView(withId(R.id.fabEditGeofences)).check(matches(isDisplayed())).check(matches(isClickable()))
        onView(withId(R.id.targetsBottomSheet)).check(matches(isDisplayed()))

        onView(withId(R.id.fabEditGeofences)).perform(click())
        testAreasScreen()
    }

    private fun testAreasScreen() {
        onView(withId(R.id.geofencesBottomSheet)).check(matches(isDisplayed()))
        onView(withId(R.id.buttonAddGeofence)).check(matches(isDisplayed())).check(matches(isClickable()))

        onView(withId(R.id.buttonAddGeofence)).perform(click())
        testEditingArea()
    }

    private fun testEditingArea() {
        onView(withId(R.id.geofenceEditingMap)).perform(clickXY(200, 200))
        waitForDelay()

        onView(withId(R.id.geofenceEditingBottomSheet)).check(matches(isDisplayed()))

        onView(withId(R.id.buttonSaveGeofence)).check(matches(isDisplayed())).check(matches(not(isEnabled())))

        onView(withId(R.id.radiusSeekBar)).check(matches(isDisplayed()))
        onView(withId(R.id.geofenceEditText))
            .perform(ViewActions.clearText(), ViewActions.typeText("New Area"), ViewActions.pressImeActionButton())
        waitForDelay()

        onView(withId(R.id.rowTargetsSelect)).check(matches(isDisplayed())).perform(click())
        onView(withText(R.string.geofence_editing_targets_alert_title)).check(matches(isDisplayed()))
        onView(withText(android.R.string.cancel)).perform(click())
        waitForDelay()

        onView(withId(R.id.rowColorPicker)).check(matches(isDisplayed())).perform(click())
        onView(withId(R.id.colorPicker)).check(matches(isDisplayed()))
        onView(withText(android.R.string.cancel)).perform(click())
        waitForDelay()

        onView(withId(R.id.buttonSaveGeofence)).check(matches(isDisplayed())).check(matches(isEnabled()))

        pressBack()
        pressBack()
    }
}