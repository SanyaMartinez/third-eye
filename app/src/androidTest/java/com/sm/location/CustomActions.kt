package com.sm.location

import android.view.View
import android.view.WindowManager
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Root
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.GeneralClickAction
import androidx.test.espresso.action.Press
import androidx.test.espresso.action.Tap
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher


private const val DEFAULT_DELAY = 300L

fun waitForDelay(timeInMillis: Long = DEFAULT_DELAY): ViewInteraction =
    onView(ViewMatchers.isRoot()).perform(delay(timeInMillis))

private fun delay(timeInMillis: Long) = object : ViewAction {
    override fun perform(uiController: UiController?, view: View?) { uiController?.loopMainThreadForAtLeast(timeInMillis) }
    override fun getConstraints() = ViewMatchers.isRoot()
    override fun getDescription() = "wait for $timeInMillis milliseconds"
}


fun checkToastWithText(strResId: Int): ViewInteraction = onView(withText(strResId)).inRoot(ToastMatcher()).check(matches(isDisplayed()))

class ToastMatcher : TypeSafeMatcher<Root?>() {
    override fun describeTo(description: Description) { description.appendText("is toast") }
    override fun matchesSafely(root: Root?): Boolean {
        val type = root?.windowLayoutParams?.get()?.type
        if (type == WindowManager.LayoutParams.TYPE_TOAST) {
            val windowToken = root.decorView.windowToken
            val appToken = root.decorView.applicationWindowToken
            if (windowToken === appToken)
                return true
        }
        return false
    }
}

fun clickXY(x: Int, y: Int): ViewAction {
    return GeneralClickAction(
        Tap.SINGLE,
        { view ->
            val screenPos = IntArray(2)
            view.getLocationOnScreen(screenPos)
            val screenX = (screenPos[0] + x).toFloat()
            val screenY = (screenPos[1] + y).toFloat()
            floatArrayOf(screenX, screenY)
        },
        Press.FINGER)
}
