package com.sm.location.services

import android.util.Log
import com.huawei.hms.push.HmsMessageService
import com.huawei.hms.push.RemoteMessage
import com.sm.core.utils.TAG
import com.sm.location.notifications.MessagingHandler
import dagger.android.AndroidInjection
import java.lang.Exception
import javax.inject.Inject

class MyHmsMessageService: HmsMessageService() {

    @Inject
    lateinit var messagingHandler: MessagingHandler

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        messagingHandler.handleNewToken(token)
    }

    override fun onTokenError(e: Exception) {
        super.onTokenError(e)
        Log.e(TAG, "Error with getting new push token", e)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        messagingHandler.handleNewMessage(remoteMessage)
    }
}
