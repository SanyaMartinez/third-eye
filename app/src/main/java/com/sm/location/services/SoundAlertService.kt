package com.sm.location.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.*
import androidx.core.app.NotificationCompat
import com.sm.location.R
import com.sm.location.notifications.NotificationsCreator
import com.sm.pushapi.utils.PushConstants

class SoundAlertService : Service() {

    private val am by lazy { getSystemService(Context.AUDIO_SERVICE) as AudioManager }
    private val vibrator by lazy { getSystemService(VIBRATOR_SERVICE) as Vibrator }

    private var mp: MediaPlayer? = null

    private var currentVolume = 0

    override fun onBind(intent: Intent?) = Binder()

    override fun onCreate() {
        mp = MediaPlayer().apply {
            setDataSource(this@SoundAlertService, DEFAULT_RINGTONE)
            prepare()
        }

        currentVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC)
        val maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        setVolume(maxVolume)
    }

    override fun onStart(intent: Intent?, startid: Int) {
        mp?.start()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_START -> createNotification(intent.extras)
            ACTION_CLOSE -> close()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun setVolume(volume: Int) = am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0)

    private fun createNotification(bundle: Bundle?) {
        val channelId = NotificationsCreator.Companion.ChannelId.SOUND_ALERT
        val channelName = getString(R.string.notification_channel_name_sound_alert)
        val channelDescription = getString(R.string.notification_channel_description_sound_alert)
        val notificationIconId = R.drawable.ic_notifications

        createNotificationChannel(channelId, channelName, channelDescription)

        val intent = Intent(this, SoundAlertService::class.java).apply {
            action = ACTION_CLOSE
        }
        val pendingIntent = PendingIntent.getService(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val closeAction = NotificationCompat.Action(R.drawable.ic_close, getString(R.string.sound_alert_button), pendingIntent)

        val title = bundle?.getString(PushConstants.Keys.TITLE) ?: getString(R.string.sound_alert_default_title)
        val body = bundle?.getString(PushConstants.Keys.BODY) ?: getString(R.string.sound_alert_default_body)

        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(notificationIconId)
            .setContentTitle(title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .addAction(closeAction)

        startForeground(defaultNotificationId, builder.build())
    }

    private fun createNotificationChannel(
        channelId: String,
        channelName: String,
        channelDescription: String
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(vibration, 0))

            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelId, channelName, importance).apply {
                description = channelDescription
            }

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun close() {
        setVolume(currentVolume)
        vibrator.cancel()
        stopForeground(true)
        stopSelf()
    }

    override fun onDestroy() {
        mp?.stop()
        mp?.release()
    }

    companion object {
        private val DEFAULT_RINGTONE = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)

        private const val defaultNotificationId = 100001

        private val vibration = longArrayOf(200, 200, 300, 400, 500, 400, 300, 200, 200)

        const val ACTION_START = "ACTION_START"
        const val ACTION_CLOSE = "ACTION_CLOSE"
    }
}
