package com.sm.location.managers

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.util.Log
import com.huawei.hms.location.*
import com.sm.core.utils.TAG
import com.sm.core.utils.toMillis
import com.sm.location.receivers.GeofenceBroadcastReceiver
import com.sm.data.repository.geofence.GeofenceRepository

class GeofenceManager(
    private val activity: Activity,
    private val geofenceRepository: GeofenceRepository
) {
    private var geofenceService: GeofenceService? = null
    private var geofencePendingIntent: PendingIntent? = null

    private val idList = ArrayList<String>()
    private val geofenceList = ArrayList<Geofence>()

    init {
        geofenceService = LocationServices.getGeofenceService(activity)
        geofencePendingIntent = getGeofencePendingIntent()
    }

    private fun getGeofencePendingIntent(): PendingIntent? {
        val intent = Intent(activity, GeofenceBroadcastReceiver::class.java)
        intent.action = GeofenceBroadcastReceiver.ACTION_PROCESS_GEOFENCE
        return PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    suspend fun requestGeofence() {
        createGeofences()
        if (geofenceList.isEmpty()) return

        val request = GeofenceRequest.Builder()
            .createGeofenceList(geofenceList)
            .build()

        geofenceService?.run {
            createGeofenceList(request, geofencePendingIntent)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful)
                        Log.i(TAG, "add geofence success!")
                    else
                        Log.e(TAG, "add geofence failed : ", task.exception)
                }
        }
    }

    private suspend fun createGeofences() {
        geofenceList.clear()
        idList.clear()

        geofenceRepository.getAllToTrackAsync().forEach {
            val geofenceId = "${it.id}$GEOFENCE_ID_DELIMITER${it.title}$GEOFENCE_ID_DELIMITER${it.watcherId}"
            val latitude = it.latitude
            val longitude = it.longitude
            val radius = it.radius.toFloat()

            geofenceList.add(
                Geofence.Builder()
                    .setUniqueId(geofenceId)
                    .setValidContinueTime(TIME_INTERVAL_GEOFENCE_UPDATE)
                    .setRoundArea(latitude, longitude, radius)
                    .setConversions(Geofence.ENTER_GEOFENCE_CONVERSION or Geofence.EXIT_GEOFENCE_CONVERSION)
                    .build()
            )
            idList.add(geofenceId)
        }
    }

    fun removeGeofences() {
        geofenceService?.run {
            deleteGeofenceList(idList)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful)
                        Log.i(TAG, "delete geofence with ID success!")
                    else
                        Log.e(TAG, "delete geofence with ID failed ")
                }
                .addOnFailureListener { e ->
                    Log.e(TAG, "delete geofence with ID failed", e)
                }
        }
    }

    companion object {
        private val TIME_INTERVAL_GEOFENCE_UPDATE = toMillis(60) // 1 min
        const val GEOFENCE_ID_DELIMITER = "|"
    }
}
