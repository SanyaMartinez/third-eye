package com.sm.location.managers

import android.Manifest
import android.app.Activity
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.sm.core.utils.isPermissionGranted

class PermissionsManager {

    fun isLocationPermissionsGranted(activity: Activity) = activity.run {
        isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) &&
            isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    fun requestLocationPermissions(activity: Activity) {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        val requestCode: Int = REQUEST_CODE_LOCATION

        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    fun requestLocationPermissions(fragment: Fragment) {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        val requestCode: Int = REQUEST_CODE_LOCATION

        fragment.requestPermissions(permissions, requestCode)
    }

    fun isBackgroundLocationPermissionsGranted(activity: Activity)  = if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) true
    else activity.isPermissionGranted(PERMISSION_ACCESS_BACKGROUND_LOCATION)

    fun requestBackgroundLocationPermissions(activity: Activity) {
        val permissions = arrayOf(PERMISSION_ACCESS_BACKGROUND_LOCATION)
        val requestCode = REQUEST_CODE_BACKGROUND_LOCATION

        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    fun requestBackgroundLocationPermissions(fragment: Fragment) {
        val permissions = arrayOf(PERMISSION_ACCESS_BACKGROUND_LOCATION)
        val requestCode = REQUEST_CODE_BACKGROUND_LOCATION

        fragment.requestPermissions(permissions, requestCode)
    }

    fun isActivityRecognitionPermissionsGranted(activity: Activity) = activity.run {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P)
            isPermissionGranted(PERMISSION_ACTIVITY_RECOGNITION_HMS)
        else
            isPermissionGranted(Manifest.permission.ACTIVITY_RECOGNITION)
    }

    fun requestActivityRecognitionPermissions(activity: Activity) {
        val permissions: Array<String>
        val requestCode: Int

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            permissions = arrayOf(PERMISSION_ACTIVITY_RECOGNITION_HMS)
            requestCode = REQUEST_CODE_ACTIVITY_RECOGNITION
        } else {
            permissions = arrayOf(Manifest.permission.ACTIVITY_RECOGNITION)
            requestCode = REQUEST_CODE_ACTIVITY_RECOGNITION_SDK_29
        }

        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    fun requestActivityRecognitionPermissions(fragment: Fragment) {
        val permissions: Array<String>
        val requestCode: Int

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            permissions = arrayOf(PERMISSION_ACTIVITY_RECOGNITION_HMS)
            requestCode = REQUEST_CODE_ACTIVITY_RECOGNITION
        } else {
            permissions = arrayOf(Manifest.permission.ACTIVITY_RECOGNITION)
            requestCode = REQUEST_CODE_ACTIVITY_RECOGNITION_SDK_29
        }

        fragment.requestPermissions(permissions, requestCode)
    }

    fun isStoragePermissionsGranted(activity: Activity) =
        activity.isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)

    fun requestStoragePermissions(fragment: Fragment) {
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        val requestCode = REQUEST_CODE_STORAGE

        fragment.requestPermissions(permissions, requestCode)
    }

    fun isReadContactsPermissionsGranted(activity: Activity) =
        activity.isPermissionGranted(Manifest.permission.READ_CONTACTS)

    fun requestReadContactsPermissions(fragment: Fragment) {
        val permissions = arrayOf(Manifest.permission.READ_CONTACTS)
        val requestCode = REQUEST_CODE_READ_CONTACTS

        fragment.requestPermissions(permissions, requestCode)
    }

    companion object {
        private const val PERMISSION_ACCESS_BACKGROUND_LOCATION = "android.permission.ACCESS_BACKGROUND_LOCATION"

        private const val PERMISSION_ACTIVITY_RECOGNITION_HMS = "com.huawei.hms.permission.ACTIVITY_RECOGNITION"

        const val REQUEST_CODE_LOCATION = 1000
        const val REQUEST_CODE_BACKGROUND_LOCATION = 1001
        const val REQUEST_CODE_ACTIVITY_RECOGNITION = 1002
        const val REQUEST_CODE_ACTIVITY_RECOGNITION_SDK_29 = 1003
        const val REQUEST_CODE_STORAGE = 1004
        const val REQUEST_CODE_READ_CONTACTS = 1005
    }
}
