package com.sm.location.managers

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentSender
import android.util.Log
import com.huawei.hms.common.ApiException
import com.huawei.hms.common.ResolvableApiException
import com.huawei.hms.location.*
import com.sm.core.utils.TAG
import com.sm.core.utils.toMillis
import com.sm.location.receivers.LocationBroadcastReceiver

class LocationManager(
    private val activity: Activity
) {
    private val fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
    private val settingsClient: SettingsClient = LocationServices.getSettingsClient(activity)

    private var locationPendingIntent: PendingIntent? = null

    init {
        locationPendingIntent = getLocationPendingIntent()
    }

    private fun getLocationPendingIntent(): PendingIntent {
        val intent = Intent(activity, LocationBroadcastReceiver::class.java)
        intent.action = LocationBroadcastReceiver.ACTION_PROCESS_LOCATION
        return PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun requestLocationUpdates() {
        val locationRequest = LocationRequest().apply {
            interval = TIME_INTERVAL_LOCATION_UPDATE
            needAddress = true
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val locationSettingsRequest = LocationSettingsRequest
            .Builder()
            .addLocationRequest(locationRequest)
            .build()

        settingsClient.checkLocationSettings(locationSettingsRequest)
            .addOnSuccessListener {
                Log.i(TAG, "check location settings success $it")
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationPendingIntent)
                    .addOnSuccessListener {
                        Log.i(TAG, "requestLocationUpdatesWithCallback onSuccess")
                    }
                    .addOnFailureListener { e ->
                        Log.e(TAG, "requestLocationUpdatesWithCallback onFailure", e)
                    }
            }
            .addOnFailureListener { e ->
                val statusCode = (e as ApiException).statusCode
                when (statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        val rae = e as ResolvableApiException
                        // Call startResolutionForResult to display a pop-up asking the user to enable related permission.
                        rae.startResolutionForResult(activity, 0)
                    } catch (sie: IntentSender.SendIntentException) {
                        Log.e(TAG, "PendingIntent unable to execute request.")
                    }
                }
            }
    }

    fun removeLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationPendingIntent)
            .addOnSuccessListener {
                Log.d(TAG, "removeLocationUpdatesWithCallback success")
            }
            .addOnFailureListener { e ->
                Log.e(TAG, "removeLocationUpdatesWithCallback error", e)
            }
    }

    /*
    fun requestLastKnownLocation() {
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener { location ->
                if (location == null) {
                    return@addOnSuccessListener
                }
                // Processing logic of the Location object.
            }
            .addOnFailureListener { e ->
                Log.e(TAG, "lastLocation error", e)
            }
    }
     */

    companion object {
        private val TIME_INTERVAL_LOCATION_UPDATE = toMillis(60) // 1 min
    }
}
