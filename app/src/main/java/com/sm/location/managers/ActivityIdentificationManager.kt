package com.sm.location.managers

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.util.Log
import com.huawei.hms.location.*
import com.sm.core.utils.TAG
import com.sm.core.utils.toMillis
import com.sm.location.receivers.ActivityBroadcastReceiver

class ActivityIdentificationManager(
    private val activity: Activity
) {
    private var activityPendingIntent: PendingIntent? = null
    private var activityIdentificationService: ActivityIdentificationService? = null

    init {
        activityIdentificationService = ActivityIdentification.getService(activity)
        activityPendingIntent = getActivityIdentificationPendingIntent()
    }

    private fun getActivityIdentificationPendingIntent(): PendingIntent {
        val intent = Intent(activity, ActivityBroadcastReceiver::class.java)
        intent.action = ActivityBroadcastReceiver.ACTION_PROCESS_ACTIVITY
        return PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    // Смена активности
    fun requestActivityConversion() {
        val activities = listOf(
            ActivityIdentificationData.VEHICLE,
            ActivityIdentificationData.BIKE,
            ActivityIdentificationData.FOOT,
            ActivityIdentificationData.WALKING,
            ActivityIdentificationData.RUNNING
        )
        val conversions = activities.map { ActivityConversionInfo(it, ActivityConversionInfo.ENTER_ACTIVITY_CONVERSION) }
        val request = ActivityConversionRequest(conversions)

        activityIdentificationService?.run {
            createActivityConversionUpdates(request, activityPendingIntent)
                .addOnSuccessListener {
                    Log.i(TAG, "createActivityConversionUpdates onSuccess")
                }
                .addOnFailureListener { e ->
                    Log.e(TAG, "createActivityConversionUpdates onFailure", e)
                }
        }
    }

    fun removeActivityConversion() {
        activityIdentificationService?.run {
            deleteActivityConversionUpdates(activityPendingIntent)
                .addOnSuccessListener {
                    Log.i(TAG, "deleteActivityConversionUpdates onSuccess")
                }
                .addOnFailureListener { e ->
                    Log.e(TAG, "deleteActivityConversionUpdates onFailure", e)
                }
        }
    }

    companion object {
        private val TIME_INTERVAL_ACTIVITY_UPDATE = toMillis(60) // 1 min
    }
}
