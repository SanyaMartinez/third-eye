package com.sm.location.di.modules

import com.sm.location.receivers.GeofenceBroadcastReceiver
import com.sm.location.receivers.ActivityBroadcastReceiver
import com.sm.location.receivers.LocationBroadcastReceiver
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BroadcastReceiverModule {

    @ContributesAndroidInjector
    abstract fun contributesActivityBroadcastReceiver(): ActivityBroadcastReceiver

    @ContributesAndroidInjector
    abstract fun contributesGeofenceBroadcastReceiver(): GeofenceBroadcastReceiver

    @ContributesAndroidInjector
    abstract fun contributesLocationBroadcastReceiver(): LocationBroadcastReceiver
}
