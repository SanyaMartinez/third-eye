package com.sm.location.di.modules

import com.sm.location.ui.main.MainActivity
import com.sm.location.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity
}
