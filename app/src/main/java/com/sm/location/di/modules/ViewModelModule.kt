package com.sm.location.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sm.location.di.ViewModelFactory
import com.sm.location.ui.auth.login.AuthLoginViewModel
import com.sm.location.ui.auth.registration.AuthRegistrationViewModel
import com.sm.location.ui.geofenceediting.GeofenceEditingViewModel
import com.sm.location.ui.geofences.GeofencesViewModel
import com.sm.location.ui.locationhistory.LocationHistoryViewModel
import com.sm.location.ui.history.HistoryListViewModel
import com.sm.location.ui.map.MapViewModel
import com.sm.location.ui.noteadding.NoteAddingViewModel
import com.sm.location.ui.profile.ProfileViewModel
import com.sm.location.ui.splash.SplashViewModel
import com.sm.location.ui.users.adding.UsersAddingViewModel
import com.sm.location.ui.users.list.UsersListViewModel
import com.sm.location.ui.users.detail.UserDetailViewModel
import com.sm.location.ui.watchrequest.WatchRequestViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule {
    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
    @Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthLoginViewModel::class)
    internal abstract fun bindAuthLoginViewModel(viewModel: AuthLoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthRegistrationViewModel::class)
    internal abstract fun bindAuthRegistrationViewModel(viewModel: AuthRegistrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoryListViewModel::class)
    internal abstract fun bindNotificationsListViewModel(viewModel: HistoryListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UsersListViewModel::class)
    internal abstract fun bindUsersListViewModel(viewModel: UsersListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UsersAddingViewModel::class)
    internal abstract fun bindUsersAddingViewModel(viewModel: UsersAddingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WatchRequestViewModel::class)
    internal abstract fun bindWatchRequestViewModel(viewModel: WatchRequestViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailViewModel::class)
    internal abstract fun bindUserDetailViewModel(viewModel: UserDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocationHistoryViewModel::class)
    internal abstract fun bindLocationHistoryViewModel(viewModel: LocationHistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    internal abstract fun bindMapViewModel(viewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GeofencesViewModel::class)
    internal abstract fun bindGeofencesViewModel(viewModel: GeofencesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GeofenceEditingViewModel::class)
    internal abstract fun bindGeofenceEditingViewModel(viewModel: GeofenceEditingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NoteAddingViewModel::class)
    internal abstract fun bindNoteAddingViewModel(viewModel: NoteAddingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel
}
