package com.sm.location.di.modules

import com.sm.location.services.MyHmsMessageService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    internal abstract fun contributeHmsMessaging(): MyHmsMessageService
}
