package com.sm.location.di

import android.app.Application
import com.sm.analytics.di.modules.AnalyticsModule
import com.sm.data.di.modules.LocalDbModule
import com.sm.data.di.modules.NetworkDbModule
import com.sm.data.di.modules.RepositoryModule
import com.sm.pushapi.di.modules.ApiModule
import com.sm.pushapi.di.modules.PushManagerModule
import com.sm.location.MainApplication
import com.sm.location.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        // app
        AndroidInjectionModule::class,
        ApplicationModule::class,
        ActivityModule::class,
        BroadcastReceiverModule::class,
        FragmentModule::class,
        NotificationsModule::class,
        ServiceModule::class,
        ViewModelModule::class,

        // data
        LocalDbModule::class,
        NetworkDbModule::class,
        RepositoryModule::class,

        // pushapi
        ApiModule::class,
        PushManagerModule::class,

        // analytics
        AnalyticsModule::class,
    ]
)
interface ApplicationComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): ApplicationComponent
    }

    fun inject(mainApplication: MainApplication)
}
