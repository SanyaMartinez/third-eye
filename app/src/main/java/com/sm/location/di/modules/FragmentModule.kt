package com.sm.location.di.modules

import com.sm.location.ui.auth.login.AuthLoginFragment
import com.sm.location.ui.auth.registration.AuthRegistrationFragment
import com.sm.location.ui.geofenceediting.GeofenceEditingFragment
import com.sm.location.ui.geofences.GeofencesFragment
import com.sm.location.ui.locationhistory.LocationHistoryFragment
import com.sm.location.ui.history.HistoryListFragment
import com.sm.location.ui.map.MapMainFragment
import com.sm.location.ui.noteadding.NoteAddingFragment
import com.sm.location.ui.profile.ProfileFragment
import com.sm.location.ui.users.adding.UsersAddingFragment
import com.sm.location.ui.users.list.UsersListFragment
import com.sm.location.ui.users.detail.UserDetailFragment
import com.sm.location.ui.watchrequest.WatchRequestFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun authLoginFragment(): AuthLoginFragment

    @ContributesAndroidInjector
    abstract fun authRegistrationFragment(): AuthRegistrationFragment

    @ContributesAndroidInjector
    abstract fun notificationsListFragment(): HistoryListFragment

    @ContributesAndroidInjector
    abstract fun usersListFragment(): UsersListFragment

    @ContributesAndroidInjector
    abstract fun usersAddingFragment(): UsersAddingFragment

    @ContributesAndroidInjector
    abstract fun watchRequestFragment(): WatchRequestFragment

    @ContributesAndroidInjector
    abstract fun userDetailFragment(): UserDetailFragment

    @ContributesAndroidInjector
    abstract fun locationHistoryFragment(): LocationHistoryFragment

    @ContributesAndroidInjector
    abstract fun mapFragment(): MapMainFragment

    @ContributesAndroidInjector
    abstract fun geofencesFragment(): GeofencesFragment

    @ContributesAndroidInjector
    abstract fun geofenceEditingFragment(): GeofenceEditingFragment

    @ContributesAndroidInjector
    abstract fun noteAddingFragment(): NoteAddingFragment

    @ContributesAndroidInjector
    abstract fun profileFragment(): ProfileFragment
}
