package com.sm.location.di.modules

import android.content.Context
import com.sm.location.notifications.MessagingHandler
import com.sm.location.notifications.NotificationsCreator
import com.sm.data.repository.geofence.GeofenceRepository
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.note.NoteRepository
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NotificationsModule {
    @Provides
    @Singleton
    fun provideMessagingHandler(
        notificationsCreator: NotificationsCreator,
        notificationRepository: NotificationRepository,
        locationRepository: LocationRepository,
        userRepository: UserRepository,
        userInfoRepository: UserInfoRepository,
        geofenceRepository: GeofenceRepository,
        relationsRepository: RelationsRepository,
        noteRepository: NoteRepository,
    ) = MessagingHandler(
        notificationsCreator,
        notificationRepository,
        locationRepository,
        userRepository,
        userInfoRepository,
        geofenceRepository,
        relationsRepository,
        noteRepository,
    )

    @Provides
    @Singleton
    fun provideNotificationsCreator(context: Context) = NotificationsCreator(context)
}
