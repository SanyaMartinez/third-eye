package com.sm.location.utils

import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import com.hbb20.CountryCodePicker

fun CountryCodePicker.setEditedDialogTextProvider(
    title: String? = null,
    searchHint: String? = null,
    noResultACK: String? = null,
) {
    val dialogTextProvider = object : CountryCodePicker.CustomDialogTextProvider {
        override fun getCCPDialogTitle(
            language: CountryCodePicker.Language,
            defaultTitle: String
        ) = title ?: defaultTitle

        override fun getCCPDialogSearchHintText(
            language: CountryCodePicker.Language,
            defaultSearchHintText: String
        ) = searchHint ?: defaultSearchHintText

        override fun getCCPDialogNoResultACK(
            language: CountryCodePicker.Language,
            defaultNoResultACK: String
        ) = noResultACK ?: defaultNoResultACK
    }

    this.setCustomDialogTextProvider(dialogTextProvider)
}

fun retrievePhoneNumberFromIntent(context: Context, intent: Intent?): String? {
    var result: String? = null

    intent?.data?.let { contactData ->
        val cur = context.contentResolver.query(contactData, null, null, null, null)
        cur?.let { cursor ->
            if (cursor.count > 0) {
                if (cursor.moveToNext()) {
                    val idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID)
                    val id = cursor.getString(idIndex)

                    val hasPhoneIndex = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)
                    val hasPhone = cursor.getString(hasPhoneIndex)

                    if (hasPhone.toInt() > 0) {
                        val phones = context.contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                            null,
                            null
                        )

                        phones?.let {
                            while (it.moveToNext()) {
                                val phoneIndex = it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
                                val phoneNumber = it.getString(phoneIndex)

                                result = phoneNumber
                            }
                        }
                        phones?.close()
                    }
                }
            }
        }
        cur?.close()
    }

    return result
}
