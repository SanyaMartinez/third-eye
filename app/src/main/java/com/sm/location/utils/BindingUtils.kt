package com.sm.location.utils

import android.content.res.ColorStateList
import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.imageview.ShapeableImageView
import com.sm.location.R
import com.sm.location.adapters.BindableAdapter

@BindingAdapter("android:visibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("adapterData")
fun <T> setAdapterData(recyclerView: RecyclerView, data: List<T>?) {
    data?.let {
        (recyclerView.adapter as? BindableAdapter<T>)?.data = it
    }
}

@BindingAdapter("android:src")
fun setImageUri(view: ImageView, imageUri: String?) {
    if (imageUri == null) {
        Glide.with(view.context)
            .load(R.drawable.default_user_image)
            .centerCrop()
            .into(view)
    } else {
        Glide.with(view.context)
            .load(Uri.parse(imageUri))
            .centerCrop()
            .into(view)
    }
}

@BindingAdapter("app:tint")
fun setImageTint(view: ImageView, colorRes: Int) {
    view.setColorFilter(colorRes)
}

@BindingAdapter(value = ["app:srcCompat", "app:color"], requireAll = true)
fun setCircleImageWithStroke(view: ShapeableImageView, imageUri: String?, color: Int) {
    if (imageUri == null)
        view.setImageDrawable(ContextCompat.getDrawable(view.context, R.drawable.default_user_image))
    else
        view.setImageURI(Uri.parse(imageUri))

    val colorStateList = ColorStateList.valueOf(color)
    view.strokeColor = colorStateList
}
