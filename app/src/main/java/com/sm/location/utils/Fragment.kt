package com.sm.location.utils

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun Fragment.showSettingsAlert(@StringRes strResId: Int) {
    MaterialAlertDialogBuilder(requireContext())
        .setMessage(strResId)
        .setNegativeButton(android.R.string.cancel) { _, _ -> }
        .setPositiveButton(android.R.string.ok) { _, _ -> toAppSettings() }
        .show()
}

fun Fragment.toAppSettings() {
    val intent = Intent(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        Uri.parse("package:" + requireActivity().packageName)
    )
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
}
