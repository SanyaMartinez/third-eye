package com.sm.location.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.sm.location.R
import com.sm.location.services.SoundAlertService
import com.sm.location.services.SoundAlertService.Companion.ACTION_START
import com.sm.location.ui.splash.SplashActivity
import com.sm.pushapi.utils.PushConstants
import javax.inject.Inject

class NotificationsCreator @Inject constructor(
    private val context: Context
) {
    fun createNotification(
        title: String,
        body: String,
        notificationId: Int,
        topic: String,
    ) {
        val channelId: String
        val channelName: String
        val channelDescription: String
        val notificationIconId: Int
        when (topic) {
            PushConstants.Topics.SOUND_ALERT -> {
                startSoundAlert(title, body)
                return
            }
            PushConstants.Topics.MESSAGE -> {
                channelId = ChannelId.MESSAGE
                channelName = context.getString(R.string.notification_channel_name_message)
                channelDescription = context.getString(R.string.notification_channel_description_message)
                notificationIconId = R.drawable.ic_message
            }
            PushConstants.Topics.GEOFENCE_UPDATE,
            PushConstants.Topics.GEOFENCE_CONVERSION -> {
                channelId = ChannelId.GEOFENCE_CONVERSION
                channelName = context.getString(R.string.notification_channel_name_geofence_conversion)
                channelDescription = context.getString(R.string.notification_channel_description_geofence_conversion)
                notificationIconId = R.drawable.ic_location_on
            }
            PushConstants.Topics.NOTE_ADD -> {
                channelId = ChannelId.NOTE_ADD
                channelName = context.getString(R.string.notification_channel_name_note)
                channelDescription = context.getString(R.string.notification_channel_description_note)
                notificationIconId = R.drawable.ic_rate_review
            }
            PushConstants.Topics.ACTIVITY_CONVERSION -> {
                channelId = ChannelId.ACTIVITY_CONVERSION
                channelName = context.getString(R.string.notification_channel_name_activity_conversion)
                channelDescription = context.getString(R.string.notification_channel_description_activity_conversion)
                notificationIconId = R.drawable.ic_run
            }
            PushConstants.Topics.WATCHER_REQUEST,
            PushConstants.Topics.WATCHER_DENY_REQUEST,
            PushConstants.Topics.WATCHER_ACCEPT_REQUEST,
            PushConstants.Topics.WATCHER_CANCEL_REQUEST,
            PushConstants.Topics.DELETE_TARGET,
            PushConstants.Topics.DELETE_WATCHER -> {
                channelId = ChannelId.WATCHER_REQUEST
                channelName = context.getString(R.string.notification_channel_name_watcher_request)
                channelDescription = context.getString(R.string.notification_channel_description_watcher_request)
                notificationIconId = R.drawable.ic_person
            }
            else -> {
                channelId = ChannelId.DEFAULT
                channelName = context.getString(R.string.notification_channel_name_default)
                channelDescription = context.getString(R.string.notification_channel_description_default)
                notificationIconId = R.drawable.ic_location_on
            }
        }

        createNotificationChannel(channelId, channelName, channelDescription)

        // TODO
        // Create an explicit intent for an Activity in your app
        val intent = Intent(context, SplashActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        val builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(notificationIconId)
            .setContentTitle(title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)

        NotificationManagerCompat.from(context).run {
            notify(notificationId, builder.build())
        }
    }

    private fun startSoundAlert(title: String, body: String) {
        val intent = Intent(context, SoundAlertService::class.java).apply {
            action = ACTION_START
            putExtra(PushConstants.Keys.TITLE, title)
            putExtra(PushConstants.Keys.BODY, body)
        }
        ContextCompat.startForegroundService(context, intent)
    }

    private fun createNotificationChannel(
        channelId: String,
        channelName: String,
        channelDescription: String
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, channelName, importance).apply {
                description = channelDescription
            }

            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        object ChannelId {
            const val DEFAULT = "1000"
            const val GEOFENCE_CONVERSION = "1001"
            const val ACTIVITY_CONVERSION = "1002"
            const val WATCHER_REQUEST = "1003"
            const val SOUND_ALERT = "1004"
            const val MESSAGE = "1005"
            const val NOTE_ADD = "1006"
        }
    }
}
