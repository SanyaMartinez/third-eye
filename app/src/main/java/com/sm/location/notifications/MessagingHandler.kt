package com.sm.location.notifications

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.huawei.hms.push.RemoteMessage
import com.sm.core.utils.TAG
import com.sm.data.models.NotificationEntity
import com.sm.pushapi.utils.PushConstants
import com.sm.data.repository.geofence.GeofenceRepository
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.note.NoteRepository
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MessagingHandler @Inject constructor(
    private val notificationsCreator: NotificationsCreator,
    private val notificationRepository: NotificationRepository,
    private val locationRepository: LocationRepository,
    private val userRepository: UserRepository,
    private val userInfoRepository: UserInfoRepository,
    private val geofenceRepository: GeofenceRepository,
    private val relationsRepository: RelationsRepository,
    private val noteRepository: NoteRepository,
) {

    fun handleNewToken(token: String) {
        Log.d(TAG, "New token: $token")
        CoroutineScope(Dispatchers.IO).launch {
            userInfoRepository.updatePushToken(token)
        }
    }

    fun handleNewMessage(remoteMessage: RemoteMessage?) {
        remoteMessage?.run {
            Log.d(TAG, "New push message")

            val data = dataOfMap ?: emptyMap()
            val title = data[PushConstants.Keys.TITLE] ?: return
            val body = data[PushConstants.Keys.BODY] ?: return

            val id = data[PushConstants.Keys.ID]?.toIntOrNull() ?: return
            val timestamp = data[PushConstants.Keys.TIMESTAMP]?.toLongOrNull() ?: return
            val topic = data[PushConstants.Keys.TOPIC] ?: return
            val senderUserId = data[PushConstants.Keys.SENDER_USER_ID] ?: return
            val pushData = data[PushConstants.Keys.PUSH_DATA] ?: return

            CoroutineScope(Dispatchers.IO).launch {
                handlePushData(topic, senderUserId, pushData)

                val notificationEntity = NotificationEntity(
                    id = id,
                    timestamp = timestamp,
                    topic = topic,
                    title = title,
                    body = body,
                    senderUserId = senderUserId,
                    data = pushData,
                )

                notificationRepository.add(notificationEntity)

                val needShowPush = userRepository.getByIdAsync(senderUserId)?.notificationsEnabled
                if (needShowPush == true)
                    notificationsCreator.createNotification(title, body, id, topic)
            }
        }
    }

    private suspend fun handlePushData(topic: String, senderUserId: String, data: String) {
        val dataMap = try {
            Gson().fromJson<Map<String, String>>(data, DATA_MAP_TYPE)
        } catch (e: Exception) {
            null
        }

        when (topic) {
            PushConstants.Topics.WATCHER_REQUEST -> handleWatcherRequest()
            PushConstants.Topics.WATCHER_DENY_REQUEST -> handleDenyingWatcherRequest(senderUserId)
            PushConstants.Topics.WATCHER_ACCEPT_REQUEST -> handleAcceptingWatcherRequest(senderUserId)
            PushConstants.Topics.WATCHER_CANCEL_REQUEST -> handleCancelingWatcherRequest(senderUserId)
            PushConstants.Topics.DELETE_TARGET -> handleDeletingTarget(senderUserId)
            PushConstants.Topics.DELETE_WATCHER -> handleDeletingWatcher(senderUserId)
            PushConstants.Topics.NOTE_ADD -> handleNoteAdding(senderUserId)
            PushConstants.Topics.GEOFENCE_UPDATE -> handleGeofenceUpdate(senderUserId)
            PushConstants.Topics.GEOFENCE_CONVERSION -> { }
            PushConstants.Topics.ACTIVITY_CONVERSION -> { }
            PushConstants.Topics.MESSAGE -> { }
            PushConstants.Topics.SOUND_ALERT -> { }
            PushConstants.Topics.DEFAULT -> { }
        }
    }

    private suspend fun handleWatcherRequest() {
        relationsRepository.syncRequests()
    }

    private suspend fun handleDenyingWatcherRequest(targetId: String) {
        relationsRepository.handleDenying(targetId)
    }

    private suspend fun handleAcceptingWatcherRequest(targetId: String) {
        relationsRepository.handleAccepting(targetId)
        locationRepository.listenToLocationUpdates(listOf(targetId))
    }

    private suspend fun handleCancelingWatcherRequest(watcherId: String) {
        relationsRepository.handleCanceling(watcherId)
    }

    private suspend fun handleDeletingTarget(watcherId: String) {
         relationsRepository.handleDeletingTarget(watcherId)
    }

    private suspend fun handleDeletingWatcher(targetId: String) {
        relationsRepository.handleDeletingWatcher(targetId)
    }

    private suspend fun handleNoteAdding(creatorId: String) {
        noteRepository.loadAndUpdateNotes(creatorId, userInfoRepository.userId)
    }

    private suspend fun handleGeofenceUpdate(watcherId: String) {
        geofenceRepository.loadAndUpdateGeofences(watcherId)
    }

    companion object {
        private val DATA_MAP_TYPE = object : TypeToken<Map<String, String>>() {}.type
    }
}
