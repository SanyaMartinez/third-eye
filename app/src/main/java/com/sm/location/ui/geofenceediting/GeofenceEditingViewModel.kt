package com.sm.location.ui.geofenceediting

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sm.location.models.State
import com.sm.pushapi.utils.PushConstants
import com.sm.location.R
import com.sm.data.models.GeofenceEntity
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.geofence.GeofenceRepository
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class GeofenceEditingViewModel @Inject constructor(
    private val geofencesRepository: GeofenceRepository,
    private val notificationRepository: NotificationRepository,
    private val userInfoRepository: UserInfoRepository,
    userRepository: UserRepository
) : ViewModel() {

    val acceptedTargets = userRepository.getAcceptedTargets()

    val geofenceId = MutableLiveData<Long>()

    val geofence = Transformations.switchMap(geofenceId) {
        geofencesRepository.getById(it)
    }

    val geofenceSaveState = MutableLiveData<State>()

    /**
     * Сохраняем Геозону и уведомляем Таргеты о добавлении, чтобы тот загрузил и обновил список
     */
    fun saveGeofence(context: Context, geofence: GeofenceEntity) = viewModelScope.launch(Dispatchers.IO) {
        geofenceSaveState.postValue(State.LOADING)

        try {
            geofencesRepository.insert(geofence)
        } catch (e: Exception) {
            geofenceSaveState.postValue(State.FAILURE)
            return@launch
        }

        val title = context.getString(R.string.geofence_editing_notification_title, userInfoRepository.name)
        val body = context.getString(R.string.geofence_editing_notification_body, geofence.title)
        val senderUserId = userInfoRepository.userId

        val notification = NotificationEntity(
            timestamp = System.currentTimeMillis(),
            topic = PushConstants.Topics.GEOFENCE_UPDATE,
            title = title,
            body = body,
            senderUserId = senderUserId,
        )

        acceptedTargets.value?.forEach { target ->
            if (geofence.targetsIds.contains(target.id)) {
                val result = notificationRepository.sendNotification(target.id, target.pushToken, notification)

                if (!result) {
                    geofenceSaveState.postValue(State.FAILURE)
                    return@launch
                }
            }
        }

        geofenceSaveState.postValue(State.SUCCESS)
    }
}
