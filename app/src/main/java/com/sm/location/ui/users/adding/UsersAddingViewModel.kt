package com.sm.location.ui.users.adding

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sm.core.utils.TAG
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.pushapi.utils.PushConstants
import com.sm.location.R
import com.sm.location.models.RequestState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class UsersAddingViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository,
    private val userInfoRepository: UserInfoRepository,
    private val relationsRepository: RelationsRepository
) : ViewModel() {

    val sendRequestState = MutableLiveData<RequestState>()

    fun sendRequest(context: Context, code: String, number: String) = viewModelScope.launch(Dispatchers.IO) {
        sendRequestState.postValue(RequestState.LOADING)
        val targetUserInfo = userInfoRepository.getUserInfoByPhone(code, number)

        when {
            targetUserInfo == null -> sendRequestState.postValue(RequestState.ERROR_USER_NOT_FOUND)
            targetUserInfo.id == userInfoRepository.userId -> sendRequestState.postValue(RequestState.ERROR_SELF_REQUEST)
            else -> {
                try {
                    relationsRepository.sendRequest(targetUserInfo)

                    sendRequestState.postValue(RequestState.SUCCESS)

                    val title = context.getString(R.string.users_adding_notification_title, userInfoRepository.name)
                    val body = context.getString(R.string.users_adding_notification_body, userInfoRepository.getPhone())
                    val senderUserId = userInfoRepository.userId

                    val notification = NotificationEntity(
                        timestamp = System.currentTimeMillis(),
                        topic = PushConstants.Topics.WATCHER_REQUEST,
                        title = title,
                        body = body,
                        senderUserId = senderUserId,
                    )
                    notificationRepository.sendNotification(targetUserInfo.id, targetUserInfo.pushToken, notification)
                } catch (e: Exception) {
                    Log.e(TAG, "Error with sending request", e)
                    sendRequestState.postValue(RequestState.FAILURE)
                }
            }
        }
    }
}
