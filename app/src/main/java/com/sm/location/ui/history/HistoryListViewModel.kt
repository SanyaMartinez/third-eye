package com.sm.location.ui.history

import androidx.lifecycle.*
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class HistoryListViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository,
    private val userInfoRepository: UserInfoRepository,
) : ViewModel() {

    val notifications = notificationRepository.getAll()

    init {
        viewModelScope.launch(Dispatchers.IO) {
            notificationRepository.loadAll(userInfoRepository.userId)
        }
    }

    fun deleteNotification(notificationEntity: NotificationEntity) = viewModelScope.launch(Dispatchers.IO) {
        notificationRepository.hide(userInfoRepository.userId, notificationEntity)
    }
}
