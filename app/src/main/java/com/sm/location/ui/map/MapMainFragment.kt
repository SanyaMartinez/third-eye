package com.sm.location.ui.map

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.model.*
import com.sm.core.utils.getTransparentColor
import com.sm.data.models.*
import com.sm.maps.ui.BaseMapsFragment
import com.sm.location.R
import com.sm.location.adapters.NotesInfoWindowAdapter
import com.sm.location.adapters.TargetsAdapter
import com.sm.location.databinding.FragmentMapMainBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.managers.PermissionsManager
import com.sm.location.ui.main.MainActivity
import com.sm.location.ui.noteimage.NoteImageFragment
import com.sm.location.ui.users.detail.UserDetailFragment
import com.sm.location.utils.setCircleImageWithStroke
import kotlinx.android.synthetic.main.bottomsheet_targets.*
import kotlinx.android.synthetic.main.fragment_map_main.*
import kotlinx.android.synthetic.main.marker.view.*
import javax.inject.Inject

class MapMainFragment : BaseMapsFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val mapViewModel: MapViewModel by viewModels { vmFactory }

    private val permissionsManager = PermissionsManager()

    private val targetsOnClickListener = object : TargetsAdapter.OnClickListener {
        override fun onClick(userLocation: UserLocation) {
            map?.animateCamera(CameraUpdateFactory.newLatLng(userLocation.location.toLatLng()))
        }
        override fun onClickView(userLocation: UserLocation) = toTarget(userLocation.user)
    }

    private val targetsAdapter = TargetsAdapter(targetsOnClickListener)

    private val currentTargetsMarkers: HashMap<Marker, UserLocation> = hashMapOf()
    private val currentNotesMarkers: HashMap<Marker, UserNote> = hashMapOf()
    private val currentCircles: HashMap<Circle, GeofenceEntity> = hashMapOf()

    private lateinit var notesInfoWindowAdapter: NotesInfoWindowAdapter
    private val notesClickListener = object : NotesInfoWindowAdapter.OnNoteInfoWindowClickListener {
        override fun onClickImage(data: UserNote) = toNoteImage(data.note)
        override fun onClickDelete(data: UserNote) = showDeleteNoteAlert(data.note)
    }

    private val targetsObserver = Observer<List<UserLocation>?> {
        it?.let { list -> drawTargetsMarkers(list) }
    }

    private val notesObserver = Observer<List<UserNote>?> {
        it?.let { list -> drawNotesMarkers(list) }
    }

    private val geofencesObserver = Observer<List<GeofenceEntity>?> {
        it?.let { list -> drawGeofences(list) }
    }

    private val updatedLocationObserver = Observer<LocationEntity?> {
        it?.let { location -> mapViewModel.insertUpdatedLocation(location) }
    }

    private val locationPermissionsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == MainActivity.ACTION_LOCATION_PERMISSIONS)
                checkLocationPermissions()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notesInfoWindowAdapter = NotesInfoWindowAdapter(requireContext(), notesClickListener).apply {
            setUserId(mapViewModel.userId)
        }
    }

    override fun onMapReady(huaweiMap: HuaweiMap) {
        super.onMapReady(huaweiMap)

        map?.setInfoWindowAdapter(notesInfoWindowAdapter)

        mapViewModel.targets.value?.let { list -> drawTargetsMarkers(list) }
        mapViewModel.notes.value?.let { list -> drawNotesMarkers(list) }
        mapViewModel.geofences.value?.let { list -> drawGeofences(list) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMapMainBinding.inflate(inflater, container, false).apply {
            vm = mapViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(locationPermissionsReceiver, IntentFilter(MainActivity.ACTION_LOCATION_PERMISSIONS))

        mapViewModel.apply {
            targets.observe(viewLifecycleOwner, targetsObserver)
            notes.observe(viewLifecycleOwner, notesObserver)
            geofences.observe(viewLifecycleOwner, geofencesObserver)
            updatedLocation.observe(viewLifecycleOwner, updatedLocationObserver)
        }

        BottomSheetBehavior.from(targetsBottomSheet).state = BottomSheetBehavior.STATE_COLLAPSED

        targetsRecyclerView.apply {
            setHasFixedSize(true)
            adapter = targetsAdapter
        }

        fabToPermissions.setOnClickListener { toPermissions() }
        fabAddNotes.setOnClickListener { toNoteAdding() }
        fabEditGeofences.setOnClickListener { toGeofences() }
    }

    override fun onResume() {
        super.onResume()
        updatePermissionsButtonVisibility()
    }

    private fun updatePermissionsButtonVisibility() {
        val isVisible = permissionsManager.run {
            !isLocationPermissionsGranted(requireActivity()) ||
                !isBackgroundLocationPermissionsGranted(requireActivity()) ||
                !isActivityRecognitionPermissionsGranted(requireActivity())
        }

        mapViewModel.isPermissionsButtonVisible.set(isVisible)
    }

    private fun drawTargetsMarkers(newData: List<UserLocation>) {
        currentTargetsMarkers.forEach { it.key.remove() }
        currentTargetsMarkers.clear()

        newData.forEach {
            val icon = createMarkerIcon(it)

            val marker = map?.addMarker(
                MarkerOptions()
                    .title(it.user.nameToShow())
                    .position(it.location.toLatLng())
                    .icon(BitmapDescriptorFactory.fromBitmap(icon))
            )
            marker?.let { m -> currentTargetsMarkers[m] = it }
        }
    }

    private fun drawNotesMarkers(data: List<UserNote>) {
        currentNotesMarkers.forEach { it.key.remove() }
        currentNotesMarkers.clear()

        data.forEach {
            val marker = map?.addMarker(
                MarkerOptions()
                    .title(it.note.title)
                    .snippet(it.note.description)
                    .position(it.note.getPosition())
            )
            marker?.let { m -> currentNotesMarkers[m] = it }
        }

        notesInfoWindowAdapter.setData(currentNotesMarkers)
    }

    private fun drawGeofences(geofences: List<GeofenceEntity>) {
        currentCircles.forEach { it.key.remove() }
        currentCircles.clear()

        geofences.forEach {
            val circle = map?.addCircle(
                CircleOptions()
                    .center(it.getCenter())
                    .radius(it.radius)
                    .strokeColor(it.color)
                    .strokeWidth(5f)
                    .fillColor(getTransparentColor(it.color, 0.5))
            )
            circle?.let { c -> currentCircles[c] = it }
        }
    }

    private fun createMarkerIcon(data: UserLocation): Bitmap {
        val bitmap: Bitmap

        LayoutInflater
            .from(requireContext())
            .inflate(R.layout.marker, null, false)
            .apply {
                setCircleImageWithStroke(
                    view = shapeableImageView,
                    imageUri = data.user.imageUri,
                    color = data.user.color
                        ?: ContextCompat.getColor(requireContext(), R.color.colorPrimary)
                )
            }
            .run {
                val size = (resources.getDimension(R.dimen.marker_icon_size)).toInt()
                layout(0, 0, size, size)

                bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                background?.draw(canvas)
                draw(canvas)
            }

        return bitmap
    }

    private fun toPermissions() = findNavController().navigate(R.id.toPermissions)

    private fun toNoteAdding() = findNavController().navigate(R.id.toNoteAdding)

    private fun toNoteImage(note: NoteEntity) = findNavController().navigate(
        R.id.toNoteImage,
        bundleOf(NoteImageFragment.KEY_IMAGE_URI to note.noteImageUri)
    )

    private fun toGeofences() = findNavController().navigate(R.id.toGeofences)

    private fun toTarget(user: UserEntity) = findNavController().navigate(
        R.id.toUserDetail,
        bundleOf(
            UserDetailFragment.KEY_USER_ID to user.id,
            UserDetailFragment.KEY_IS_ACCEPTED_TARGET to true
        )
    )

    private fun showDeleteNoteAlert(note: NoteEntity) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.map_note_delete_alert_message)
            .setPositiveButton(R.string.map_note_delete_alert_button_delete) { _, _ ->
                mapViewModel.deleteNote(note)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .show()
    }

    override fun onDestroyView() {
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(locationPermissionsReceiver)
        super.onDestroyView()
    }
}
