package com.sm.location.ui.noteadding

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.content.FileProvider
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.model.*
import com.sm.data.models.NoteEntity
import com.sm.data.models.UserEntity
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.BuildConfig
import com.sm.location.R
import com.sm.location.databinding.FragmentNoteAddingBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.managers.PermissionsManager
import com.sm.location.models.State
import com.sm.location.utils.showSettingsAlert
import com.sm.maps.ui.BaseMapsFragment
import kotlinx.android.synthetic.main.bottomsheet_note_adding.*
import kotlinx.android.synthetic.main.fragment_note_image.*
import java.io.File
import javax.inject.Inject

class NoteAddingFragment : BaseMapsFragment(), HuaweiMap.OnMapClickListener {

    @Inject
    lateinit var userInfoRepository: UserInfoRepository

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val noteAddingViewModel: NoteAddingViewModel by viewModels { vmFactory }

    private val permissionsManager = PermissionsManager()

    private val acceptedUsersObserver = Observer<List<UserEntity>?> {
        it?.let { list -> acceptedUsers = list }
    }

    private val noteSaveStateObserver = Observer<State?> {
        when (it) {
            State.SUCCESS -> onSuccessResult()
            State.FAILURE -> onFailureResult()
            else -> return@Observer
        }
    }

    private var acceptedUsers: List<UserEntity> = emptyList()
    private val usersIds: MutableList<String> = mutableListOf()

    private var snackbar: Snackbar? = null

    private var marker: Marker? = null

    private var imageUri: Uri? = null

    override fun onMapReady(huaweiMap: HuaweiMap) {
        super.onMapReady(huaweiMap)

        map?.setOnMapClickListener(this)
    }

    override fun onMapClick(position: LatLng) {
        snackbar?.dismiss()
        setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED)

        marker?.remove()
        marker = map?.addMarker(
            MarkerOptions()
                .position(position)
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentNoteAddingBinding.inflate(inflater, container, false).apply {
            vm = noteAddingViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpBottomSheet()

        snackbar = Snackbar.make(
            requireView(),
            getString(R.string.note_adding_snackbar_text),
            Snackbar.LENGTH_INDEFINITE
        )
        snackbar?.show()

        noteAddingViewModel.acceptedUsers.observe(viewLifecycleOwner, acceptedUsersObserver)
        noteAddingViewModel.noteSaveState.observe(viewLifecycleOwner, noteSaveStateObserver)
    }

    private fun setUpBottomSheet() {
        setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN)

        noteTitleEditText.doOnTextChanged { text, _, _, _ ->
            noteTitleInputLayout.error = if (!text.isNullOrEmpty()) ""
            else getString(R.string.note_adding_title_error_text)
        }

        noteDescriptionEditText.doOnTextChanged { text, _, _, _ ->
            noteDescriptionInputLayout.error = if (!text.isNullOrEmpty()) ""
            else getString(R.string.note_adding_description_error_text)
        }

        rowUsersSelect.setOnClickListener { showUsersSelectionDialog() }
        rowAttachImage.setOnClickListener { showAttachImageDialog() }

        attachmentCloseIcon.setOnClickListener { clearAttachment() }

        buttonSaveNote.setOnClickListener {
            saveNote()
        }
    }

    private fun setBottomSheetState(state: Int) {
        BottomSheetBehavior.from(noteAddingBottomSheet).state = state
    }

    private fun showUsersSelectionDialog() {
        val newUsersIdsForNote: MutableList<String> = usersIds.toMutableList()

        val users: List<UserEntity> = acceptedUsers.toList()

        if (users.isEmpty()) {
            MaterialAlertDialogBuilder(requireContext())
                .setMessage(R.string.note_adding_users_alert_message_empty)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .show()
        } else {
            val usersNames = users.map { it.nameToShow() }.toTypedArray()
            val checkedItems =
                users.map { newUsersIdsForNote.contains(it.id) }.toBooleanArray()

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.note_adding_users_alert_title)
                .setMultiChoiceItems(usersNames, checkedItems) { _, which, isChecked ->
                    val userId = users[which].id

                    if (isChecked) newUsersIdsForNote.add(userId)
                    else newUsersIdsForNote.remove(userId)
                }
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    usersIds.apply {
                        clear()
                        addAll(newUsersIdsForNote)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { _, _ -> }
                .show()
        }
    }

    private fun showAttachImageDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setItems(R.array.note_adding_edit_dialog) { _, which ->
                when (which) {
                    0 -> checkStoragePermissions()
                    1 -> toCamera()
                }
            }
            .show()
    }

    private fun checkStoragePermissions() {
        if (!permissionsManager.isStoragePermissionsGranted(requireActivity()))
            permissionsManager.requestStoragePermissions(this)
        else
            toPickPhoto()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsManager.REQUEST_CODE_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                toPickPhoto()
            else
                showSettingsAlert(R.string.note_adding_alert_settings_message_storage)
        }
    }

    private fun toPickPhoto() {
        val pickPhotoIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhotoIntent, REQUEST_CODE_PICK_PHOTO)
    }

    private fun toCamera() {
        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
            setPhotoImageUri()
            putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        }
        startActivityForResult(takePhotoIntent, REQUEST_CODE_TAKE_PHOTO)
    }

    private fun setPhotoImageUri() {
        val folderPath = requireContext().getExternalFilesDir(Environment.DIRECTORY_DCIM)
        val folder = File("$folderPath")
        folder.mkdirs()

        val timestamp = System.currentTimeMillis()
        val file = File(folder, "img_$timestamp.jpg")
        if (file.exists())
            file.delete()
        file.createNewFile()

        imageUri = FileProvider.getUriForFile(
            requireContext(),
            BuildConfig.APPLICATION_ID + ".fileProvider",
            file
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_PICK_PHOTO -> if (resultCode == Activity.RESULT_OK && data != null) {
                data.data?.let {
                    imageUri = it
                    showAttachment()
                }
            }
            REQUEST_CODE_TAKE_PHOTO -> if (resultCode == Activity.RESULT_OK) {
                showAttachment()
            }
        }
    }

    private fun showAttachment() {
        Glide.with(this)
            .load(imageUri)
            .fitCenter()
            .into(attachmentImage)

        attachmentCard.visibility = View.VISIBLE
    }

    private fun clearAttachment() {
        imageUri = null
        attachmentCard.visibility = View.GONE
    }

    private fun saveNote() {
        buttonSaveNote.isEnabled = false

        if (noteTitleEditText.text.isNullOrEmpty() || noteDescriptionEditText.text.isNullOrEmpty()) {
            showAlert(R.string.note_adding_alert_message_empty_text, navigateBack = false)
            buttonSaveNote.isEnabled = true
            return
        }

        marker?.let {
            val note = NoteEntity(
                title = noteTitleEditText.text.toString(),
                description = noteDescriptionEditText.text.toString(),
                latitude = it.position.latitude,
                longitude = it.position.longitude,
                creatorId = userInfoRepository.userId,
                usersIds = usersIds,
            )

            noteAddingViewModel.saveNote(requireContext(), note, imageUri)
        }
    }

    private fun onSuccessResult() {
        showAlert(R.string.note_adding_alert_message_success, navigateBack = true)
    }

    private fun onFailureResult() {
        showAlert(R.string.note_adding_alert_message_error, navigateBack = false)
        buttonSaveNote.isEnabled = true
    }

    private fun showAlert(@StringRes messageId: Int, navigateBack: Boolean) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(messageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> if (navigateBack) backToMap() }
            .setOnCancelListener { if (navigateBack) backToMap() }
            .show()
    }

    private fun backToMap() = findNavController().popBackStack()

    override fun onDestroy() {
        super.onDestroy()
        snackbar?.dismiss()
    }

    companion object {
        private const val REQUEST_CODE_PICK_PHOTO = 3000
        private const val REQUEST_CODE_TAKE_PHOTO = 3001
    }
}
