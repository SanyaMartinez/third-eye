package com.sm.location.ui.map

import androidx.databinding.ObservableField
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sm.data.models.LocationEntity
import com.sm.data.models.NoteEntity
import com.sm.data.repository.geofence.GeofenceRepository
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.note.NoteRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MapViewModel @Inject constructor(
    private val locationRepository: LocationRepository,
    private val noteRepository: NoteRepository,
    geofencesRepository: GeofenceRepository,
    userInfoRepository: UserInfoRepository,
) : ViewModel() {

    val userId = userInfoRepository.userId

    val isPermissionsButtonVisible = ObservableField(false)

    val updatedLocation = locationRepository.updatedLocation

    val targets = locationRepository.getTargetsLastLocations()

    val notes = Transformations.distinctUntilChanged(noteRepository.getAllUsersNotes())

    val geofences = geofencesRepository.getAllToDisplay()

    fun insertUpdatedLocation(location: LocationEntity) = viewModelScope.launch {
        locationRepository.insertLocal(location)
    }

    fun deleteNote(note: NoteEntity) = viewModelScope.launch(Dispatchers.IO) {
        if (note.creatorId == userId)
            noteRepository.deleteOwn(note)
        else
            noteRepository.deleteNotOwn(note, userId)
    }
}
