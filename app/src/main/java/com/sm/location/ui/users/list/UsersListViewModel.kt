package com.sm.location.ui.users.list

import androidx.lifecycle.*
import com.sm.data.repository.user.UserRepository
import javax.inject.Inject

class UsersListViewModel @Inject constructor(
    userRepository: UserRepository
) : ViewModel() {

    private val users = userRepository.getAll()

    val acceptedTargets = Transformations.map(users) { users ->
        users.filter { it.isTarget && it.isAcceptedTarget }
    }

    val requestedTargets = Transformations.map(users) { users ->
        users.filter { it.isTarget && !it.isAcceptedTarget }
    }

    val acceptedWatchers = Transformations.map(users) { users ->
        users.filter { it.isWatcher && it.isAcceptedWatcher }
    }

    val requestedWatchers = Transformations.map(users) { users ->
        users.filter { it.isWatcher && !it.isAcceptedWatcher }
    }

    val isEmptyList = Transformations.map(users) { users ->
        users.none { it.isTarget || it.isAcceptedTarget || it.isWatcher || it.isAcceptedWatcher }
    }
}
