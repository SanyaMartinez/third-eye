package com.sm.location.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import com.sm.location.R

class BaseMainFragment: Fragment() {

    private val rootDestinations = setOf(
        R.id.mapMain,
        R.id.usersList,
        R.id.historyList,
        R.id.profile
    )
    private val appBarConfig = AppBarConfiguration(rootDestinations)

    private var layoutRes = DEFAULT_ID
    private var toolbarId = DEFAULT_ID
    private var navHostId = DEFAULT_ID

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            layoutRes = it.getInt(KEY_LAYOUT)
            toolbarId = it.getInt(KEY_TOOLBAR)
            navHostId = it.getInt(KEY_NAV_HOST)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (layoutRes == DEFAULT_ID) null
        else inflater.inflate(layoutRes, container, false)
    }

    override fun onStart() {
        super.onStart()
        if (toolbarId == DEFAULT_ID || navHostId == DEFAULT_ID)
            return

        val navController = requireActivity().findNavController(navHostId)
        val toolbar = requireActivity().findViewById<Toolbar>(toolbarId)
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfig)
    }

    fun onBackPressed() = requireActivity()
        .findNavController(navHostId)
        .navigateUp(appBarConfig)

    companion object {
        private const val KEY_LAYOUT = "key_layout"
        private const val KEY_TOOLBAR = "key_toolbar"
        private const val KEY_NAV_HOST = "key_nav_host"

        private const val DEFAULT_ID = -1

        fun newInstance(layoutRes: Int, toolbarId: Int, navHostId: Int) = BaseMainFragment().apply {
            arguments = bundleOf(
                KEY_LAYOUT to layoutRes,
                KEY_TOOLBAR to toolbarId,
                KEY_NAV_HOST to navHostId
            )
        }
    }
}
