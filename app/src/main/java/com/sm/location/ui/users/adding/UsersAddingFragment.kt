package com.sm.location.ui.users.adding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.analytics.tracker.AnalyticsTracker
import com.sm.analytics.utils.AnalyticsConstants
import com.sm.core.utils.showToast
import com.sm.location.R
import com.sm.location.databinding.FragmentUsersAddingBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.models.RequestState
import com.sm.location.ui.base.BaseContactsFragment
import com.sm.location.utils.setEditedDialogTextProvider
import kotlinx.android.synthetic.main.fragment_users_adding.*
import javax.inject.Inject

class UsersAddingFragment : BaseContactsFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    @Inject
    lateinit var analyticsTracker: AnalyticsTracker

    private val usersAddingViewModel: UsersAddingViewModel by viewModels { vmFactory }

    private val sendRequestStateObserver = Observer<RequestState?> {
        when (it) {
            RequestState.ERROR_USER_NOT_FOUND -> onUserNotFoundError()
            RequestState.ERROR_SELF_REQUEST -> onSelfRequestError()
            RequestState.SUCCESS -> onSuccessSendingRequest()
            RequestState.FAILURE -> onFailureSendingRequest()
            else -> return@Observer
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentUsersAddingBinding.inflate(inflater, container, false).apply {
            vm = usersAddingViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usersAddingViewModel.sendRequestState.observe(viewLifecycleOwner, sendRequestStateObserver)

        addingCountryCodePicker.setEditedDialogTextProvider(
            title = getString(R.string.country_code_picker_dialog_title)
        )
        addingCountryCodePicker.registerCarrierNumberEditText(addingPhoneNumberEditText)

        addingPhoneFromContactsText.setOnClickListener {
            checkReadContactsPermission()
        }

        buttonSendRequest.setOnClickListener {
            validateData()
        }
    }

    override fun setPhoneNumberFromContacts(phoneNumber: String) {
        addingCountryCodePicker.fullNumber = phoneNumber
    }

    private fun validateData() {
        buttonSendRequest.isEnabled = false
        if (addingCountryCodePicker.isValidFullNumber) {
            val countryCode = addingCountryCodePicker.selectedCountryCodeWithPlus
            val phoneNumber = addingCountryCodePicker.fullNumberWithPlus.removePrefix(countryCode)

            usersAddingViewModel.sendRequest(requireContext(), countryCode, phoneNumber)
        } else {
            showToast(getString(R.string.users_adding_error_text))
            buttonSendRequest.isEnabled = true
        }
    }

    private fun onUserNotFoundError() {
        val message = getString(R.string.users_adding_result_message_error_user_not_found)
        showAlert(message, navigateBack = false)
    }

    private fun onSelfRequestError() {
        val message = getString(R.string.users_adding_result_message_error_self_request)
        showAlert(message, navigateBack = false)
    }

    private fun onSuccessSendingRequest() {
        analyticsTracker.trackEvent(AnalyticsConstants.Events.TARGET_REQUEST_SEND_SUCCESS)

        val message = getString(R.string.users_adding_result_message_success)
        showAlert(message, navigateBack = true)
    }

    private fun onFailureSendingRequest() {
        analyticsTracker.trackEvent(AnalyticsConstants.Events.TARGET_REQUEST_SEND_FAILURE)

        val message = getString(R.string.users_adding_result_message_failure)
        showAlert(message, navigateBack = false)
    }

    private fun showAlert(message: String, navigateBack: Boolean) {
        if (!navigateBack)
            buttonSendRequest.isEnabled = true

        MaterialAlertDialogBuilder(requireContext())
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ -> if (navigateBack) backToUsersList() }
            .setOnCancelListener { if (navigateBack) backToUsersList() }
            .show()
    }

    private fun backToUsersList() = findNavController().popBackStack()
}
