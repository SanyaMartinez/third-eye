package com.sm.location.ui.profile

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.sm.location.R

class PreferencesFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}
