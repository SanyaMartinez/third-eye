package com.sm.location.ui.base

import android.content.Intent
import android.content.pm.PackageManager
import android.provider.ContactsContract
import com.sm.location.R
import com.sm.location.managers.PermissionsManager
import com.sm.location.utils.retrievePhoneNumberFromIntent
import com.sm.location.utils.showSettingsAlert
import dagger.android.support.DaggerFragment

abstract class BaseContactsFragment : DaggerFragment() {

    private val permissionsManager = PermissionsManager()

    fun checkReadContactsPermission() {
        if (!permissionsManager.isReadContactsPermissionsGranted(requireActivity()))
            permissionsManager.requestReadContactsPermissions(this)
        else {
            toContacts()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsManager.REQUEST_CODE_READ_CONTACTS) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                toContacts()
            else
                showSettingsAlert(R.string.phone_from_contacts_settings_alert_message)
        }
    }

    private fun toContacts() {
        val contactPickerIntent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        startActivityForResult(contactPickerIntent, CONTACTS_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CONTACTS_REQUEST_CODE) {
            retrievePhoneNumberFromIntent(requireContext(), data)?.let { phoneNumber ->
                setPhoneNumberFromContacts(phoneNumber)
            }
        }
    }

    abstract fun setPhoneNumberFromContacts(phoneNumber: String)

    companion object {
        private const val CONTACTS_REQUEST_CODE = 1003
    }
}
