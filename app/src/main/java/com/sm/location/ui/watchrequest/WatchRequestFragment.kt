package com.sm.location.ui.watchrequest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.ObservableField
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.location.R
import com.sm.data.models.UserEntity
import com.sm.location.databinding.FragmentWatchRequestBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.models.RequestState
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_watch_request.*
import javax.inject.Inject

class WatchRequestFragment : DaggerFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val watchRequestViewModel: WatchRequestViewModel by viewModels { vmFactory }

    private val acceptRequestStateObserver = Observer<RequestState?> {
        when (it) {
            RequestState.LOADING -> onLoading()
            RequestState.SUCCESS -> onSuccessResult(R.string.watch_request_alert_message_accept)
            RequestState.FAILURE -> onFailureResult()
            else -> return@Observer
        }
    }

    private val denyRequestStateObserver = Observer<RequestState?> {
        when (it) {
            RequestState.LOADING -> onLoading()
            RequestState.SUCCESS -> onSuccessResult(R.string.watch_request_alert_message_deny)
            RequestState.FAILURE -> onFailureResult()
            else -> return@Observer
        }
    }

    val isLoading = ObservableField(false)

    private val watcher: UserEntity?
        get() = arguments?.getSerializable(KEY_USER) as? UserEntity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentWatchRequestBinding.inflate(inflater, container, false).apply {
            fr = this@WatchRequestFragment
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        watchRequestViewModel.apply {
            acceptRequestState.observe(viewLifecycleOwner, acceptRequestStateObserver)
            denyRequestState.observe(viewLifecycleOwner, denyRequestStateObserver)
        }

        buttonAcceptRequest.setOnClickListener {
            watchRequestViewModel.acceptRequest(requireContext(), watcher)
        }

        buttonDenyRequest.setOnClickListener {
            watchRequestViewModel.denyRequest(requireContext(), watcher)
        }
    }

    private fun onLoading() = isLoading.set(true)

    private fun onSuccessResult(@StringRes messageId: Int) {
        isLoading.set(false)
        showAlert(messageId, navigateBack = true)
    }

    private fun onFailureResult() {
        isLoading.set(false)
        showAlert(R.string.watch_request_alert_message_error, navigateBack = false)
    }

    private fun showAlert(@StringRes messageId: Int, navigateBack: Boolean) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(messageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> if (navigateBack) backToUsersList() }
            .setOnCancelListener { if (navigateBack) backToUsersList() }
            .show()
    }

    private fun backToUsersList() = findNavController().popBackStack()

    companion object {
        const val KEY_USER = "user"
    }
}
