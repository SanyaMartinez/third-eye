package com.sm.location.ui.onboarding.slides

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.bumptech.glide.Glide
import com.sm.location.R
import com.sm.location.ui.onboarding.OnboardingActivity
import kotlinx.android.synthetic.main.fragment_slide_background_location.*

class SlideBackgroundLocationFragment : Fragment(R.layout.fragment_slide_background_location) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load(R.drawable.slide_background_location)
            .centerCrop()
            .into(slideBackgroundLocationImage)

        slideBackgroundLocationButton.setOnClickListener {
            (activity as? OnboardingActivity)?.checkBackgroundLocationPermissions()
        }
    }
}
