package com.sm.location.ui.auth.registration

import android.util.Log
import androidx.lifecycle.*
import com.huawei.agconnect.auth.*
import com.sm.core.utils.TAG
import com.sm.core.utils.toMillis
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.models.RegistrationState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class AuthRegistrationViewModel @Inject constructor(
    private val userInfoRepository: UserInfoRepository
) : ViewModel() {

    private var userName: String = ""
    private var phoneCode: String = ""
    private var phoneNumber: String = ""

    private var lastSmsSentTime: Long = 0L

    val registrationState = MutableLiveData<RegistrationState>()

    fun requestSmsVerify(name: String, code: String, number: String) = viewModelScope.launch(Dispatchers.IO) {
        if (userInfoRepository.isExistByPhone(code, number)) {
            registrationState.postValue(RegistrationState.ALREADY)
            return@launch
        }

        val wasSmsAlreadySent = System.currentTimeMillis() - lastSmsSentTime < toMillis(SEND_SMS_INTERVAL_IN_SEC)
        if (wasSmsAlreadySent && phoneCode == code && phoneNumber == number) {
            registrationState.postValue(RegistrationState.CODE_SEND_ALREADY)
            return@launch
        }

        userName = name
        phoneCode = code
        phoneNumber = number

        val settings = VerifyCodeSettings.newBuilder()
            .action(VerifyCodeSettings.ACTION_REGISTER_LOGIN)
            .sendInterval(SEND_SMS_INTERVAL_IN_SEC)
            .locale(Locale.getDefault())
            .build()

        registrationState.postValue(RegistrationState.LOADING)
        PhoneAuthProvider.requestVerifyCode(phoneCode, phoneNumber, settings)
            .addOnSuccessListener {
                registrationState.postValue(RegistrationState.CODE_SEND_SUCCESS)
                lastSmsSentTime = System.currentTimeMillis()
            }
            .addOnFailureListener {
                registrationState.postValue(RegistrationState.CODE_SEND_FAILURE)
                Log.e(TAG, "Error with sending verification code", it)
            }
    }

    fun register(verifyCode: String) {
        registrationState.value = RegistrationState.LOADING

        val phoneUser = PhoneUser.Builder()
            .setCountryCode(phoneCode)
            .setPhoneNumber(phoneNumber)
            .setVerifyCode(verifyCode)
            .build()

        AGConnectAuth.getInstance().createUser(phoneUser)
            .addOnSuccessListener {
                viewModelScope.launch(Dispatchers.IO) {
                    userInfoRepository.userId = it.user.uid
                    userInfoRepository.name = userName
                    userInfoRepository.phoneCode = phoneCode
                    userInfoRepository.phoneNumber = phoneNumber
                    userInfoRepository.sendUserInfo()

                    registrationState.postValue(RegistrationState.SUCCESS)
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "Error with create user by phone", it)
                if (it is AGCAuthException) {
                    if (it.code == AGCAuthException.USER_HAVE_BEEN_REGISTERED)
                        registrationState.value = RegistrationState.ALREADY
                    else
                        registrationState.value = RegistrationState.FAILURE
                } else {
                    registrationState.value = RegistrationState.FAILURE
                }
            }
    }

    companion object {
        private const val SEND_SMS_INTERVAL_IN_SEC = 30
    }
}
