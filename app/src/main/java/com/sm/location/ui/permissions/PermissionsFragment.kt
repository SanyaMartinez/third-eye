package com.sm.location.ui.permissions

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.sm.location.R
import com.sm.location.databinding.FragmentPermissionsBinding
import com.sm.location.managers.PermissionsManager
import com.sm.location.ui.main.MainActivity
import com.sm.location.utils.showSettingsAlert
import kotlinx.android.synthetic.main.fragment_permissions.*

class PermissionsFragment : Fragment() {

    private val permissionsManager = PermissionsManager()

    val isLocationGranted = ObservableField(false)
    val isBgLocationGranted = ObservableField(false)
    val isActivityGranted = ObservableField(false)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentPermissionsBinding.inflate(inflater, container, false).apply {
            fr = this@PermissionsFragment
        }
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    private fun checkPermissions() {
        permissionsManager.run {
            isLocationPermissionsGranted(requireActivity()).let { isGranted ->
                isLocationGranted.set(isGranted)
                if (!isGranted)
                    rowLocation.setOnClickListener { requestLocationPermissions(this@PermissionsFragment) }
            }

            isBackgroundLocationPermissionsGranted(requireActivity()).let { isGranted ->
                isBgLocationGranted.set(isGranted)
                if (!isGranted)
                    rowBackgroundLocation.setOnClickListener { requestBackgroundLocationPermissions(
                        this@PermissionsFragment
                    ) }
            }

            isActivityRecognitionPermissionsGranted(requireActivity()).let { isGranted ->
                isActivityGranted.set(isGranted)
                if (!isGranted)
                    rowActivity.setOnClickListener { requestActivityRecognitionPermissions(this@PermissionsFragment) }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionsManager.REQUEST_CODE_LOCATION -> {
                if (
                    grantResults.size > 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED
                ) {
                    isLocationGranted.set(true)
                    sendBroadcast(MainActivity.ACTION_LOCATION_PERMISSIONS)
                } else {
                    showSettingsAlert()
                }
            }
            PermissionsManager.REQUEST_CODE_BACKGROUND_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    isBgLocationGranted.set(true)
                else
                    showSettingsAlert()
            }
            PermissionsManager.REQUEST_CODE_ACTIVITY_RECOGNITION,
            PermissionsManager.REQUEST_CODE_ACTIVITY_RECOGNITION_SDK_29 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isActivityGranted.set(true)
                    sendBroadcast(MainActivity.ACTION_ACTIVITY_PERMISSIONS)
                } else {
                    showSettingsAlert()
                }
            }
        }
    }

    private fun showSettingsAlert() = showSettingsAlert(R.string.permissions_settings_alert_message)

    private fun sendBroadcast(action: String) =
        LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(Intent(action))
}
