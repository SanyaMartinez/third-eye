package com.sm.location.ui.auth.registration

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.core.utils.showToast
import com.sm.location.R
import com.sm.location.databinding.FragmentAuthRegistrationBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.models.RegistrationState
import com.sm.location.ui.base.BaseContactsFragment
import com.sm.location.ui.splash.SplashActivity
import com.sm.location.utils.setEditedDialogTextProvider
import kotlinx.android.synthetic.main.edit_dialog_sms_confirmation.*
import kotlinx.android.synthetic.main.fragment_auth_registration.*
import javax.inject.Inject

class AuthRegistrationFragment : BaseContactsFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val authRegistrationViewModel: AuthRegistrationViewModel by viewModels { vmFactory }

    private val registrationStateObserver = Observer<RegistrationState?> {
        when (it) {
            RegistrationState.ALREADY -> showErrorAlert(R.string.auth_registration_alert_message_already) {
                findNavController().popBackStack()
            }
            RegistrationState.CODE_SEND_ALREADY,
            RegistrationState.CODE_SEND_SUCCESS -> showSmsConfirmationAlert()
            RegistrationState.CODE_SEND_FAILURE -> showErrorAlert(R.string.auth_sms_code_alert_message_error)
            RegistrationState.SUCCESS -> navigateNext()
            RegistrationState.FAILURE -> showErrorAlert(R.string.auth_registration_alert_message_failure)
            else -> return@Observer
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        authRegistrationViewModel.registrationState.observe(this, registrationStateObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentAuthRegistrationBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            vm = authRegistrationViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authRegistrationCountryCodePicker.setEditedDialogTextProvider(
            title = getString(R.string.country_code_picker_dialog_title)
        )
        authRegistrationCountryCodePicker.registerCarrierNumberEditText(authRegistrationPhoneNumberEditText)

        authRegistrationPhoneFromContactsText.setOnClickListener {
            checkReadContactsPermission()
        }

        authRegistrationButton.setOnClickListener {
            validateData()
        }
    }

    override fun setPhoneNumberFromContacts(phoneNumber: String) {
        authRegistrationCountryCodePicker.fullNumber = phoneNumber
    }

    private fun validateData() {
        val name = authRegistrationNameEditText.text.toString().trim()

        authRegistrationNameInputLayout.error = if (name.isNotEmpty()) ""
        else getString(R.string.auth_registration_name_error_text)

        if (name.isNotEmpty() && authRegistrationCountryCodePicker.isValidFullNumber) {
            val countryCode = authRegistrationCountryCodePicker.selectedCountryCodeWithPlus
            val phoneNumber = authRegistrationCountryCodePicker.fullNumberWithPlus.removePrefix(countryCode)

            authRegistrationViewModel.requestSmsVerify(name, countryCode, phoneNumber)
        } else if (name.isEmpty())
            return
        else
            showToast(getString(R.string.auth_registration_phone_error_text))
    }

    private fun showSmsConfirmationAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setView(R.layout.edit_dialog_sms_confirmation)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                val verifyCode = (dialog as? AlertDialog)?.verifyCodeEditText?.text?.toString()
                    ?: return@setPositiveButton
                authRegistrationViewModel.register(verifyCode)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .setCancelable(false)
            .show()
    }

    private fun showErrorAlert(@StringRes errorMessageId: Int, action: (() -> Unit)? = null) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(errorMessageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> action?.invoke() }
            .show()
    }

    private fun navigateNext() {
        val intent = Intent(requireContext(), SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
