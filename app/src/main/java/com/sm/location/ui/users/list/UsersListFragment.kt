package com.sm.location.ui.users.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.sm.location.R
import com.sm.location.adapters.UsersAdapter
import com.sm.data.models.UserEntity
import com.sm.location.databinding.FragmentUsersListBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.ui.users.detail.UserDetailFragment
import com.sm.location.ui.watchrequest.WatchRequestFragment
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_users_list.*
import javax.inject.Inject

class UsersListFragment : DaggerFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val usersListViewModel: UsersListViewModel by viewModels { vmFactory }

    private val acceptedTargetsOnClickListener = object : UsersAdapter.OnClickListener {
        override fun onClick(user: UserEntity) = toUserDetail(user, isAcceptedTarget = true)
    }

    private val requestedTargetsOnClickListener = object : UsersAdapter.OnClickListener {
        override fun onClick(user: UserEntity) = toUserDetail(user, isRequestedTarget = true)
    }

    private val acceptedWatchersOnClickListener = object : UsersAdapter.OnClickListener {
        override fun onClick(user: UserEntity) = toUserDetail(user, isAcceptedWatcher = true)
    }

    private val requestedWatchersOnClickListener = object : UsersAdapter.OnClickListener {
        override fun onClick(user: UserEntity) = toWatchRequest(user)
    }

    private val acceptedTargetsAdapter = UsersAdapter(acceptedTargetsOnClickListener)
    private val requestedTargetsAdapter = UsersAdapter(requestedTargetsOnClickListener)
    private val acceptedWatchersAdapter = UsersAdapter(acceptedWatchersOnClickListener)
    private val requestedWatchersAdapter = UsersAdapter(requestedWatchersOnClickListener)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentUsersListBinding.inflate(inflater, container, false).apply {
            vm = usersListViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        acceptedTargetsRecyclerView.setUsersAdapter(acceptedTargetsAdapter)
        requestedTargetsRecyclerView.setUsersAdapter(requestedTargetsAdapter)
        acceptedWatchersRecyclerView.setUsersAdapter(acceptedWatchersAdapter)
        requestedWatchersRecyclerView.setUsersAdapter(requestedWatchersAdapter)

        fabAddUser.setOnClickListener { toAddUser() }
    }

    private fun toUserDetail(
        user: UserEntity,
        isAcceptedTarget: Boolean = false,
        isRequestedTarget: Boolean = false,
        isAcceptedWatcher: Boolean = false
    ) = findNavController().navigate(
        R.id.toUserDetail,
        bundleOf(
            UserDetailFragment.KEY_USER_ID to user.id,
            UserDetailFragment.KEY_IS_ACCEPTED_TARGET to isAcceptedTarget,
            UserDetailFragment.KEY_IS_REQUESTED_TARGET to isRequestedTarget,
            UserDetailFragment.KEY_IS_ACCEPTED_WATCHER to isAcceptedWatcher
        )
    )

    private fun toWatchRequest(user: UserEntity) = findNavController().navigate(
        R.id.toWatchRequest,
        bundleOf(WatchRequestFragment.KEY_USER to user)
    )

    private fun toAddUser() = findNavController().navigate(R.id.toUsersAdding)

    private fun RecyclerView.setUsersAdapter(adapter: RecyclerView.Adapter<UsersAdapter.ViewHolder>) {
        setHasFixedSize(true)
        this.adapter = adapter
    }
}
