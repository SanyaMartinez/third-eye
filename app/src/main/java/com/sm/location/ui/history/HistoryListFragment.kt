package com.sm.location.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.location.adapters.HistoryAdapter
import com.sm.data.models.NotificationEntity
import com.sm.location.R
import com.sm.location.databinding.FragmentHistoryListBinding
import com.sm.location.di.ViewModelFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class HistoryListFragment : DaggerFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private lateinit var binding: FragmentHistoryListBinding

    private val historyListViewModel: HistoryListViewModel by viewModels { vmFactory }

    private val historyOnClickListener = object : HistoryAdapter.OnClickListener {
        override fun onClick(notification: NotificationEntity) {
            // TODO: to screen
        }

        override fun onLongClick(notification: NotificationEntity): Boolean {
            showDeleteNotificationAlert(notification)
            return true
        }
    }

    private val historyAdapter = HistoryAdapter(historyOnClickListener)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHistoryListBinding.inflate(inflater, container, false).apply {
            vm = historyListViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.historyRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = historyAdapter
            addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
        }
    }

    private fun showDeleteNotificationAlert(notificationEntity: NotificationEntity) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.history_delete_alert_message)
            .setPositiveButton(R.string.history_delete_alert_button_delete) { _, _ ->
                historyListViewModel.deleteNotification(notificationEntity)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .show()
    }
}
