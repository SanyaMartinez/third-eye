package com.sm.location.ui.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.sm.location.R
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity(R.layout.activity_auth) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Glide.with(this)
            .load(R.drawable.auth)
            .centerCrop()
            .into(authImage)
    }
}
