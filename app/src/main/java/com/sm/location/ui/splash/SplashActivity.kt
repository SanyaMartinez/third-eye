package com.sm.location.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.sm.location.R
import com.sm.location.di.ViewModelFactory
import com.sm.location.models.SplashCheckState
import com.sm.location.ui.auth.AuthActivity
import com.sm.location.ui.main.MainActivity
import com.sm.location.ui.onboarding.OnboardingActivity
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class SplashActivity : DaggerAppCompatActivity(R.layout.activity_splash) {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val splashViewModel: SplashViewModel by viewModels { vmFactory }

    private val splashCheckStateObserver = Observer<SplashCheckState?> {
        when (it) {
            SplashCheckState.TO_ONBOARDING -> toOnboarding()
            SplashCheckState.TO_AUTH -> toAuth()
            SplashCheckState.TO_MAIN -> toMainScreen()
            else -> return@Observer
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        splashViewModel.splashCheckState.observe(this, splashCheckStateObserver)
        splashViewModel.checkSignIn(this)
    }

    private fun toOnboarding() = replaceActivity(OnboardingActivity::class.java)

    private fun toAuth() = replaceActivity(AuthActivity::class.java)

    private fun toMainScreen() = replaceActivity(MainActivity::class.java)

    private fun replaceActivity(activityClass: Class<*>) {
        val intent = Intent(this, activityClass).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
