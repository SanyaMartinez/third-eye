package com.sm.location.ui.locationhistory

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.user.UserRepository
import com.sm.location.utils.DoubleSource
import java.util.*
import javax.inject.Inject

class LocationHistoryViewModel @Inject constructor(
    locationRepository: LocationRepository,
    userRepository: UserRepository
) : ViewModel() {

    val startDateTime = MutableLiveData<Calendar>()
    val endDateTime = MutableLiveData<Calendar>()
    private val timeInterval = DoubleSource(startDateTime, endDateTime)

    val userId = MutableLiveData<String>()

    val target = Transformations.switchMap(userId) {
        userRepository.getById(it)
    }

    init {
        val start = Calendar.getInstance().apply {
            set(Calendar.MINUTE, 0)
        }
        val end = Calendar.getInstance().apply {
            set(Calendar.MINUTE, 0)
            add(Calendar.HOUR, 1)
        }

        startDateTime.value = start
        endDateTime.value = end
    }

    private val _locations = Transformations.switchMap(DoubleSource(timeInterval, userId)) {
        val start = it.first?.first?.time?.time ?: DEFAULT_TIMESTAMP
        val end = it.first?.second?.time?.time ?: DEFAULT_TIMESTAMP
        val id = it.second ?: ""
        locationRepository.getAllBetweenTimeInterval(start, end, id)
    }
    // Триггерим onChanged только когда список изменится (иначе триггерится после каждого добавления своей локации)
    val locations = Transformations.distinctUntilChanged(_locations)

    fun updateDate(year: Int, monthOfYear: Int, dayOfMonth: Int) {
        startDateTime.value?.run {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, monthOfYear)
            set(Calendar.DAY_OF_MONTH, dayOfMonth)
            startDateTime.value = this
        }
        endDateTime.value?.run {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, monthOfYear)
            set(Calendar.DAY_OF_MONTH, dayOfMonth)
            endDateTime.value = this
        }
    }

    fun updateTime(hourOfDay: Int, minute: Int, isStart: Boolean) {
        if (isStart)
            startDateTime.value?.run {
                set(Calendar.HOUR_OF_DAY, hourOfDay)
                set(Calendar.MINUTE, minute)
                startDateTime.value = this
            }
        else
            endDateTime.value?.run {
                set(Calendar.HOUR_OF_DAY, hourOfDay)
                set(Calendar.MINUTE, minute)
                endDateTime.value = this
            }
    }

    companion object {
        private val DEFAULT_TIMESTAMP = Date().time
    }
}
