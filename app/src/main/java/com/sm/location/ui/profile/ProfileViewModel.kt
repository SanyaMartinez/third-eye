package com.sm.location.ui.profile

import androidx.lifecycle.*
import com.huawei.agconnect.auth.AGConnectAuth
import com.sm.data.localdb.AppDatabase
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.models.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val appDatabase: AppDatabase,
    private val userInfoRepository: UserInfoRepository
) : ViewModel() {

    val updateInfoState = MutableLiveData<State>()

    fun getUserName() = userInfoRepository.name

    fun getUserId() = userInfoRepository.userId

    fun getUserPhone() = userInfoRepository.phoneCode + userInfoRepository.phoneNumber

    fun updateUsername(name: String) = viewModelScope.launch(Dispatchers.IO) {
        try {
            updateInfoState.postValue(State.LOADING)
            userInfoRepository.updateName(name)
            updateInfoState.postValue(State.SUCCESS)
        } catch (e: Exception) {
            updateInfoState.postValue(State.FAILURE)
        }
    }

    fun logOut(navigateAction: () -> Unit) = viewModelScope.launch(Dispatchers.IO) {
        // clear user push token
        userInfoRepository.updatePushToken("")

        // clear local data
        appDatabase.clearAllTables()
        userInfoRepository.clearData()

        // hms logout
        AGConnectAuth.getInstance().signOut()

        // to login screen
        withContext(Dispatchers.Main) {
            navigateAction()
        }
    }
}
