package com.sm.location.ui.onboarding

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.location.R
import com.sm.location.managers.PermissionsManager
import com.sm.location.ui.splash.SplashActivity
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnboardingActivity : AppCompatActivity(R.layout.activity_onboarding) {

    private val permissionsManager = PermissionsManager()

    private val onboardingFragmentPagerAdapter = OnboardingFragmentPagerAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        onboardingViewPager.apply {
            adapter = onboardingFragmentPagerAdapter
            isUserInputEnabled = false
            offscreenPageLimit = 3
        }
    }

    fun checkLocationPermissions() {
        if (permissionsManager.isLocationPermissionsGranted(this))
            toNextSlide()
        else
            permissionsManager.requestLocationPermissions(this)
    }

    fun checkBackgroundLocationPermissions() {
        if (permissionsManager.isBackgroundLocationPermissionsGranted(this))
            toNextSlide()
        else
            permissionsManager.requestBackgroundLocationPermissions(this)
    }

    fun checkActivityPermissions() {
        if (permissionsManager.isActivityRecognitionPermissionsGranted(this))
            toNextSlide()
        else
            permissionsManager.requestActivityRecognitionPermissions(this)
    }

    fun toNextSlide() {
        if (onboardingViewPager.currentItem < OnboardingFragmentPagerAdapter.NUM_PAGES - 1)
            onboardingViewPager.currentItem += 1
        else
            toSplashActivity()
    }

    private fun toSplashActivity() {
        PreferenceManager.getDefaultSharedPreferences(this).edit {
            putBoolean(PREFS_KEY_ONBOARDING, true)
        }

        val intent = Intent(this, SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionsManager.REQUEST_CODE_LOCATION -> {
                if (
                    grantResults.size > 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED
                )
                    toNextSlide()
                else
                    showPermissionsNotGrantedAlert(R.string.onboarding_slide_location_alert_message)
            }
            PermissionsManager.REQUEST_CODE_BACKGROUND_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    toNextSlide()
                else
                    showPermissionsNotGrantedAlert(R.string.onboarding_slide_background_location_alert_message)
            }
            PermissionsManager.REQUEST_CODE_ACTIVITY_RECOGNITION,
            PermissionsManager.REQUEST_CODE_ACTIVITY_RECOGNITION_SDK_29 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    toNextSlide()
                else
                    showPermissionsNotGrantedAlert(R.string.onboarding_slide_activity_alert_message)
            }
        }
    }

    private fun showPermissionsNotGrantedAlert(@StringRes messageId: Int) {
        MaterialAlertDialogBuilder(this)
            .setMessage(messageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> toNextSlide() }
            .setOnCancelListener { toNextSlide() }
            .show()
    }

    companion object {
        const val PREFS_KEY_ONBOARDING = "onboarding"
    }
}
