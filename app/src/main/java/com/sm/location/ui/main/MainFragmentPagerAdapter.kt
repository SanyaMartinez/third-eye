package com.sm.location.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.sm.location.R

class MainFragmentPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    val fragments = listOf(
        BaseMainFragment.newInstance(
            R.layout.fragment_map,
            R.id.toolbarMap,
            R.id.navHostFragmentMap
        ),
        BaseMainFragment.newInstance(
            R.layout.fragment_users,
            R.id.toolbarUsers,
            R.id.navHostFragmentUsers
        ),
        BaseMainFragment.newInstance(
            R.layout.fragment_history,
            R.id.toolbarHistory,
            R.id.navHostFragmentHistory
        ),
        BaseMainFragment.newInstance(
            R.layout.fragment_profile,
            R.id.toolbarProfile,
            R.id.navHostFragmentProfile
        )
    )

    override fun getItemCount() = NUM_PAGES

    override fun createFragment(position: Int): Fragment = fragments[position]

    companion object {
        private const val NUM_PAGES = 4

        const val PAGE_NUM_MAP = 0
        const val PAGE_NUM_USERS = 1
        const val PAGE_NUM_HISTORY = 2
        const val PAGE_NUM_PROFILE = 3
    }
}
