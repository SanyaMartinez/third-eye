package com.sm.location.ui.geofences

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.model.*
import com.sm.analytics.tracker.AnalyticsTracker
import com.sm.analytics.utils.AnalyticsConstants
import com.sm.core.utils.getTransparentColor
import com.sm.maps.ui.BaseMapsFragment
import com.sm.location.R
import com.sm.location.adapters.GeofencesAdapter
import com.sm.data.models.GeofenceEntity
import com.sm.location.databinding.FragmentGeofencesBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.ui.geofenceediting.GeofenceEditingFragment.Companion.KEY_GEOFENCE_ID
import kotlinx.android.synthetic.main.bottomsheet_geofences.*
import javax.inject.Inject

class GeofencesFragment : BaseMapsFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    @Inject
    lateinit var analyticsTracker: AnalyticsTracker

    private val geofencesViewModel: GeofencesViewModel by viewModels { vmFactory }

    private val geofencesOnClickListener = object : GeofencesAdapter.OnClickListener {
        override fun onClick(geofence: GeofenceEntity) {
            map?.animateCamera(CameraUpdateFactory.newLatLng(geofence.getCenter()))
        }

        override fun onClickEdit(geofence: GeofenceEntity) = toGeofenceEditing(geofence.id)

        override fun onClickDelete(geofence: GeofenceEntity) = showDeleteAlert(geofence)
    }

    private val geofencesAdapter = GeofencesAdapter(geofencesOnClickListener)

    private val currentCircles: HashMap<Circle, GeofenceEntity> = hashMapOf()

    private val geofencesObserver = Observer<List<GeofenceEntity>?> {
        it?.let { list -> drawGeofences(list) }
    }

    private val deleteResultObserver = Observer<Boolean?> {
        it?.let { result -> if (!result) showErrorDeleteAlert() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analyticsTracker.trackEvent(AnalyticsConstants.Events.GEOFENCES_WATCH)
    }

    override fun onMapReady(huaweiMap: HuaweiMap) {
        super.onMapReady(huaweiMap)

        geofencesViewModel.geofences.value?.let { list -> drawGeofences(list) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentGeofencesBinding.inflate(inflater, container, false).apply {
            vm = geofencesViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        geofencesViewModel.geofences.observe(viewLifecycleOwner, geofencesObserver)
        geofencesViewModel.deleteResult.observe(viewLifecycleOwner, deleteResultObserver)

        BottomSheetBehavior.from(geofencesBottomSheet).state = BottomSheetBehavior.STATE_EXPANDED

        geofencesRecyclerView.apply {
            setHasFixedSize(true)
            adapter = geofencesAdapter
        }

        buttonAddGeofence.setOnClickListener { toGeofenceEditing() }
    }

    private fun drawGeofences(geofences: List<GeofenceEntity>) {
        currentCircles.forEach { it.key.remove() }
        currentCircles.clear()

        geofences.forEach {
            val circle = map?.addCircle(
                CircleOptions()
                    .center(it.getCenter())
                    .radius(it.radius)
                    .strokeColor(it.color)
                    .strokeWidth(5f)
                    .fillColor(getTransparentColor(it.color, 0.5))
            )
            circle?.let { c -> currentCircles[c] = it }
        }
    }

    private fun toGeofenceEditing(geofenceId: Long? = null) {
        analyticsTracker.trackEvent(AnalyticsConstants.Events.GEOFENCES_EDIT)

        val bundle = bundleOf(KEY_GEOFENCE_ID to geofenceId)
        findNavController().navigate(R.id.toGeofenceEditing, bundle)
    }

    private fun showDeleteAlert(geofence: GeofenceEntity) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.geofences_alert_delete_message)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                analyticsTracker.trackEvent(AnalyticsConstants.Events.GEOFENCES_DELETE)
                geofencesViewModel.deleteGeofence(geofence)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .show()
    }

    private fun showErrorDeleteAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.geofences_alert_delete_error_message)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .show()
    }
}
