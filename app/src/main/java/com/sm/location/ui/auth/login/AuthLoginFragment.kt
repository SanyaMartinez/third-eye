package com.sm.location.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.core.utils.showToast
import com.sm.location.R
import com.sm.location.databinding.FragmentAuthLoginBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.models.LoginState
import com.sm.location.ui.base.BaseContactsFragment
import com.sm.location.ui.splash.SplashActivity
import com.sm.location.utils.setEditedDialogTextProvider
import kotlinx.android.synthetic.main.edit_dialog_sms_confirmation.*
import kotlinx.android.synthetic.main.fragment_auth_login.*
import javax.inject.Inject

class AuthLoginFragment : BaseContactsFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val authLoginViewModel: AuthLoginViewModel by viewModels { vmFactory }

    private val loginStateObserver = Observer<LoginState?> {
        when (it) {
            LoginState.CODE_SEND_ALREADY,
            LoginState.CODE_SEND_SUCCESS -> showSmsConfirmationAlert()
            LoginState.CODE_SEND_FAILURE -> showErrorAlert(R.string.auth_sms_code_alert_message_error)
            LoginState.SUCCESS -> navigateNext()
            LoginState.FAILURE -> showErrorAlert(R.string.auth_login_alert_message_failure)
            LoginState.NO_USER -> showErrorAlert(R.string.auth_login_alert_message_no_user) {
                toRegistration()
            }
            else -> return@Observer
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        authLoginViewModel.loginState.observe(this, loginStateObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentAuthLoginBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            vm = authLoginViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authLoginCountryCodePicker.setEditedDialogTextProvider(
            title = getString(R.string.country_code_picker_dialog_title)
        )
        authLoginCountryCodePicker.registerCarrierNumberEditText(authLoginPhoneNumberEditText)

        authLoginButtonContinue.setOnClickListener {
            validateData()
        }

        authLoginPhoneFromContactsText.setOnClickListener {
            checkReadContactsPermission()
        }

        authLoginButtonRegistration.setOnClickListener {
            toRegistration()
        }
    }

    override fun setPhoneNumberFromContacts(phoneNumber: String) {
        authLoginCountryCodePicker.fullNumber = phoneNumber
    }

    private fun validateData() {
        if (authLoginCountryCodePicker.isValidFullNumber) {
            val countryCode = authLoginCountryCodePicker.selectedCountryCodeWithPlus
            val phoneNumber = authLoginCountryCodePicker.fullNumberWithPlus.removePrefix(countryCode)

            authLoginViewModel.requestSmsVerify(countryCode, phoneNumber)
        } else
            showToast(getString(R.string.auth_login_phone_error_text))
    }

    private fun showSmsConfirmationAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setView(R.layout.edit_dialog_sms_confirmation)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                val verifyCode = (dialog as? AlertDialog)?.verifyCodeEditText?.text?.toString()
                    ?: return@setPositiveButton
                authLoginViewModel.signIn(verifyCode)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .setCancelable(false)
            .show()
    }

    private fun showErrorAlert(@StringRes errorMessageId: Int, action: (() -> Unit)? = null) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(errorMessageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> action?.invoke() }
            .show()
    }

    private fun toRegistration() = findNavController().navigate(R.id.toAuthRegistration)

    private fun navigateNext() {
        val intent = Intent(requireContext(), SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
