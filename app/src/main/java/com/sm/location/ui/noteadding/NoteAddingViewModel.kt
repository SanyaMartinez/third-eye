package com.sm.location.ui.noteadding

import android.content.Context
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sm.location.models.State
import com.sm.pushapi.utils.PushConstants
import com.sm.location.R
import com.sm.data.models.NoteEntity
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.note.NoteRepository
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class NoteAddingViewModel @Inject constructor(
    private val noteRepository: NoteRepository,
    private val notificationRepository: NotificationRepository,
    private val userInfoRepository: UserInfoRepository,
    userRepository: UserRepository
) : ViewModel() {

    val acceptedUsers = userRepository.getAllAccepted()

    val noteSaveState = MutableLiveData<State>()

    /**
     * Сохраняем Заметку и уведомляем пользователей о добавлении, чтобы тот загрузил и обновил список
     */
    fun saveNote(context: Context, note: NoteEntity, imageUri: Uri? = null) = viewModelScope.launch(Dispatchers.IO) {
        noteSaveState.postValue(State.LOADING)

        try {
            noteRepository.insert(note, imageUri)
        } catch (e: Exception) {
            noteSaveState.postValue(State.FAILURE)
            return@launch
        }

        val title = context.getString(R.string.note_adding_notification_title, userInfoRepository.name)
        val body = context.getString(R.string.note_adding_notification_body, note.title)
        val senderUserId = userInfoRepository.userId

        val notification = NotificationEntity(
            timestamp = System.currentTimeMillis(),
            topic = PushConstants.Topics.NOTE_ADD,
            title = title,
            body = body,
            senderUserId = senderUserId,
        )

        acceptedUsers.value?.forEach { user ->
            if (note.usersIds.contains(user.id)) {
                val result = notificationRepository.sendNotification(user.id, user.pushToken, notification)

                if (!result) {
                    noteSaveState.postValue(State.FAILURE)
                    return@launch
                }
            }
        }

        noteSaveState.postValue(State.SUCCESS)
    }
}
