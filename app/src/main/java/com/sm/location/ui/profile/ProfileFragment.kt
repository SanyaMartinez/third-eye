package com.sm.location.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.analytics.tracker.AnalyticsTracker
import com.sm.analytics.utils.AnalyticsConstants
import com.sm.core.utils.showToast
import com.sm.location.BuildConfig
import com.sm.location.R
import com.sm.location.di.ViewModelFactory
import com.sm.location.models.State
import com.sm.location.ui.splash.SplashActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.edit_dialog_username.*
import kotlinx.android.synthetic.main.fragment_profile_main.*
import kotlinx.android.synthetic.main.profile_settings.*
import kotlinx.android.synthetic.main.profile_user_info.*
import javax.inject.Inject

class ProfileFragment : DaggerFragment(R.layout.fragment_profile_main) {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    @Inject
    lateinit var analyticsTracker: AnalyticsTracker

    private val profileViewModel: ProfileViewModel by viewModels { vmFactory }

    private val updateInfoStateObserver = Observer<State?> {
        when (it) {
            State.LOADING -> onLoading()
            State.SUCCESS -> onSuccess()
            State.FAILURE -> onFailure()
            else -> return@Observer
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileViewModel.updateInfoState.observe(viewLifecycleOwner, updateInfoStateObserver)

        infoNameText.text = profileViewModel.getUserName()
        infoPhoneText.text = profileViewModel.getUserPhone()
        profileUserId.text = getString(R.string.profile_bottom_text, profileViewModel.getUserId(), BuildConfig.VERSION_NAME)

        profileInfoNameRow.setOnClickListener {
            showEditNameDialog()
        }

        profileInfoLogoutRow.setOnClickListener {
            showLogoutDialog()
        }
    }

    private fun showEditNameDialog() {
        val dialog = MaterialAlertDialogBuilder(requireContext())
            .setView(R.layout.edit_dialog_username)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                val name = (dialog as? AlertDialog)?.editNameEditText?.text?.toString()?.trim()
                    ?: return@setPositiveButton
                profileViewModel.updateUsername(name)
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .setCancelable(false)
            .create()

        dialog.run {
            show()

            val username = profileViewModel.getUserName()
            if (username.isEmpty())
                getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
            else
                editNameEditText.setText(username)

            editNameEditText.doOnTextChanged { text, _, _, _ ->
                if (text?.trim().isNullOrEmpty()) {
                    editNameTextInputLayout.error = getString(R.string.profile_edit_dialog_name_error)
                    getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
                } else {
                    editNameTextInputLayout.error = ""
                    getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = true
                }
            }
        }
    }

    private fun showLogoutDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.profile_logout_alert_message)
            .setPositiveButton(R.string.profile_logout_alert_button_logout) { _, _ ->
                profileViewModel.logOut() {
                    analyticsTracker.trackEvent(AnalyticsConstants.Events.PROFILE_LOGOUT)
                    toSplashScreen()
                }
            }
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .show()
    }

    private fun toSplashScreen() {
        val intent = Intent(requireContext(), SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun onLoading() {}

    private fun onSuccess() {
        showToast(R.string.profile_update_info_toast_success)
        infoNameText.text = profileViewModel.getUserName()
        analyticsTracker.trackEvent(AnalyticsConstants.Events.PROFILE_CHANGE_NAME)
    }

    private fun onFailure() {
        showToast(R.string.profile_update_info_toast_failure)
    }
}
