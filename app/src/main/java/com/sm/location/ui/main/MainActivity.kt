package com.sm.location.ui.main

import android.content.*
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.preference.PreferenceManager
import androidx.viewpager2.widget.ViewPager2
import com.sm.location.R
import com.sm.location.managers.ActivityIdentificationManager
import com.sm.location.managers.GeofenceManager
import com.sm.location.managers.LocationManager
import com.sm.location.managers.PermissionsManager
import com.sm.data.repository.geofence.GeofenceRepository
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(R.layout.activity_main) {

    @Inject
    lateinit var geofenceRepository: GeofenceRepository

    private val permissionsManager = PermissionsManager()

    private lateinit var activityIdentificationManager: ActivityIdentificationManager

    private lateinit var geofenceManager: GeofenceManager

    private lateinit var locationManager: LocationManager

    private val sharedPrefsListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPrefs, key ->
        when (key) {
            PREFS_KEY_ACTIVITY -> {
                val isEnabled = sharedPrefs.getBoolean(key, true)
                toggleActivityIdentification(isEnabled)
            }
            PREFS_KEY_GEOFENCE -> {
                val isEnabled = sharedPrefs.getBoolean(key, true)
                toggleGeofence(isEnabled)
            }
            PREFS_KEY_LOCATION -> {
                val isEnabled = sharedPrefs.getBoolean(key, true)
                toggleLocation(isEnabled)
            }
            else -> return@OnSharedPreferenceChangeListener
        }
    }

    private val mainFragmentPagerAdapter = MainFragmentPagerAdapter(this@MainActivity)

    // Храним список навигации для корректой работы BackPress во ViewPager
    private val fragmentsStack = mutableListOf<Int>()

    // Связываем индекс ViewPager с элементом BottomNavView
    private val pageToMenuItemId = mapOf(
        MainFragmentPagerAdapter.PAGE_NUM_MAP to R.id.navigationMap,
        MainFragmentPagerAdapter.PAGE_NUM_USERS to R.id.navigationUsers,
        MainFragmentPagerAdapter.PAGE_NUM_HISTORY to R.id.navigationHistory,
        MainFragmentPagerAdapter.PAGE_NUM_PROFILE to R.id.navigationProfile
    )

    private val permissionsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                ACTION_LOCATION_PERMISSIONS -> {
                    toggleGeofence(true)
                    toggleLocation(true)
                }
                ACTION_ACTIVITY_PERMISSIONS -> {
                    toggleActivityIdentification(true)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setPermissionsBroadcastReceiver()
        setUiAndNavigation()

        activityIdentificationManager = ActivityIdentificationManager(this)
        geofenceManager = GeofenceManager(this, geofenceRepository)
        locationManager = LocationManager(this)

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this).apply {
            registerOnSharedPreferenceChangeListener(sharedPrefsListener)
        }

        val isActivityIdentificationEnabled = sharedPreferences.getBoolean(PREFS_KEY_ACTIVITY, true)
        val isGeofenceEnabled = sharedPreferences.getBoolean(PREFS_KEY_GEOFENCE, true)
        val isLocationEnabled = sharedPreferences.getBoolean(PREFS_KEY_LOCATION, true)

        toggleActivityIdentification(isActivityIdentificationEnabled)
        toggleGeofence(isGeofenceEnabled)
        toggleLocation(isLocationEnabled)
    }

    private fun setPermissionsBroadcastReceiver() {
        val intentFilter = IntentFilter().apply {
            addAction(ACTION_LOCATION_PERMISSIONS)
            addAction(ACTION_ACTIVITY_PERMISSIONS)
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(permissionsReceiver, intentFilter)
    }

    private fun setUiAndNavigation() {
        if (fragmentsStack.isEmpty())
            fragmentsStack.add(MainFragmentPagerAdapter.PAGE_NUM_MAP)

        viewPager.apply {
            adapter = mainFragmentPagerAdapter
            offscreenPageLimit = 3
            isUserInputEnabled = false
        }

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                val itemId = pageToMenuItemId[position] ?: R.id.navigationMap
                if (bottomNavigationView.selectedItemId != itemId)
                    bottomNavigationView.selectedItemId = itemId
            }
        })

        bottomNavigationView.setOnNavigationItemSelectedListener {
            val position = pageToMenuItemId.values.indexOf(it.itemId)
            if (viewPager.currentItem != position) {
                viewPager.setCurrentItem(position, false)
                fragmentsStack.remove(position)
                fragmentsStack.add(position)
            }
            title = it.title
            true
        }
    }

    private fun toggleActivityIdentification(isEnabled: Boolean) {
        if (isEnabled && permissionsManager.isActivityRecognitionPermissionsGranted(this)) {
            activityIdentificationManager.requestActivityConversion()
        } else {
            activityIdentificationManager.removeActivityConversion()
        }
    }

    private fun toggleGeofence(isEnabled: Boolean) {
        if (isEnabled && permissionsManager.isLocationPermissionsGranted(this)) {
            lifecycleScope.launch { geofenceManager.requestGeofence() }
        } else {
            geofenceManager.removeGeofences()
        }
    }

    private fun toggleLocation(isEnabled: Boolean) {
        if (isEnabled && permissionsManager.isLocationPermissionsGranted(this)) {
            locationManager.requestLocationUpdates()
        } else {
            locationManager.removeLocationUpdates()
        }
    }

    override fun onBackPressed() {
        val fragment = mainFragmentPagerAdapter.fragments[viewPager.currentItem]
        val navigateUp = fragment.onBackPressed()

        if (!navigateUp)
            if (fragmentsStack.size > 1) {
                fragmentsStack.removeLast()
                val position = fragmentsStack.last()
                viewPager.setCurrentItem(position, false)
            } else
                super.onBackPressed()
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(permissionsReceiver)
        super.onDestroy()
    }

    companion object {
        private const val PREFS_KEY_ACTIVITY = "activity"
        private const val PREFS_KEY_GEOFENCE = "geofence"
        private const val PREFS_KEY_LOCATION = "location"

        const val ACTION_LOCATION_PERMISSIONS = "com.sm.location.ui.main.MainActivity.ACTION_LOCATION_PERMISSIONS"
        const val ACTION_ACTIVITY_PERMISSIONS = "com.sm.location.ui.main.MainActivity.ACTION_ACTIVITY_PERMISSIONS"
    }
}
