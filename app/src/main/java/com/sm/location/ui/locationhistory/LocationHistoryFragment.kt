package com.sm.location.ui.locationhistory

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.SupportMapFragment
import com.huawei.hms.maps.model.LatLng
import com.huawei.hms.maps.model.Polyline
import com.huawei.hms.maps.model.PolylineOptions
import com.sm.maps.ui.BaseMapsFragment
import com.sm.location.R
import com.sm.data.models.LocationEntity
import com.sm.data.models.UserEntity
import com.sm.location.databinding.FragmentLocationHistoryBinding
import com.sm.location.di.ViewModelFactory
import kotlinx.android.synthetic.main.bottomsheet_location_history.*
import java.util.*
import javax.inject.Inject

class LocationHistoryFragment : BaseMapsFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val locationHistoryViewModel: LocationHistoryViewModel by viewModels { vmFactory }

    private var polyline: Polyline? = null

    private var color: Int? = null

    private val locationsObserver = Observer<List<LocationEntity>?> { list ->
        list?.let { locations ->
            val coords = locations.map { location -> location.toLatLng() }
            drawPolyline(coords)

            coords.lastOrNull()?.let {
                map?.moveCamera(CameraUpdateFactory.newLatLng(it))
            }
        }
    }

    private val targetObserver = Observer<UserEntity?> {
        color = it?.color
    }

    override fun onMapReady(huaweiMap: HuaweiMap) {
        super.onMapReady(huaweiMap)

        locationHistoryViewModel.locations.value?.let { list ->
            val coords = list.map { location -> location.toLatLng() }
            drawPolyline(coords)

            coords.lastOrNull()?.let {
                map?.moveCamera(CameraUpdateFactory.newLatLng(it))
            }
        }
    }

    private val userId: String?
        get() = arguments?.getString(KEY_USER_ID)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userId?.let { locationHistoryViewModel.userId.value = it }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentLocationHistoryBinding.inflate(inflater, container, false).apply {
            vm = locationHistoryViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (childFragmentManager.findFragmentById(com.sm.maps.R.id.mapFragment) as? SupportMapFragment)?.getMapAsync(
            this
        )

        BottomSheetBehavior.from(locationHistoryBottomSheet).state = BottomSheetBehavior.STATE_EXPANDED

        locationHistoryViewModel.locations.observe(viewLifecycleOwner, locationsObserver)
        locationHistoryViewModel.target.observe(viewLifecycleOwner, targetObserver)

        setDateTimeListeners()
    }

    private fun drawPolyline(coords: List<LatLng>) {
        polyline?.remove()

        if (coords.isEmpty()) return

        val userColor = color ?: ContextCompat.getColor(requireContext(), R.color.colorPrimary)

        polyline = map?.addPolyline(
            PolylineOptions()
                .addAll(coords)
                .color(userColor)
                .width(2f)
        )
    }

    private fun setDateTimeListeners() {
        historyDate.setOnClickListener {
            val date = locationHistoryViewModel.startDateTime.value ?: Calendar.getInstance()
            openDatePicker(date)
        }

        historyTimeStart.setOnClickListener {
            val date = locationHistoryViewModel.startDateTime.value ?: Calendar.getInstance()
            openTimePicker(date, isStart = true)
        }

        historyTimeEnd.setOnClickListener {
            val date = locationHistoryViewModel.endDateTime.value ?: Calendar.getInstance()
            openTimePicker(date, isStart = false)
        }
    }

    private fun openDatePicker(date: Calendar) {
        val onDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            locationHistoryViewModel.updateDate(year, monthOfYear, dayOfMonth)
        }

        DatePickerDialog(
            requireContext(),
            onDateSetListener,
            date.get(Calendar.YEAR),
            date.get(Calendar.MONTH),
            date.get(Calendar.DAY_OF_MONTH)
        )
            .show()
    }

    private fun openTimePicker(date: Calendar, isStart: Boolean) {
        val onTimeSetListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            locationHistoryViewModel.updateTime(hourOfDay, minute, isStart)
        }

        TimePickerDialog(
            requireContext(),
            onTimeSetListener,
            date.get(Calendar.HOUR_OF_DAY),
            date.get(Calendar.MINUTE),
            true
        )
            .show()
    }

    companion object {
        const val KEY_USER_ID = "userId"
    }
}
