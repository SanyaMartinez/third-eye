package com.sm.location.ui.users.detail

import android.app.Activity.RESULT_OK
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.dhaval2404.colorpicker.ColorPickerDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sm.analytics.tracker.AnalyticsTracker
import com.sm.analytics.utils.AnalyticsConstants
import com.sm.location.R
import com.sm.location.databinding.FragmentUserDetailBinding
import com.sm.location.di.ViewModelFactory
import com.sm.location.managers.PermissionsManager
import com.sm.location.models.State
import com.sm.location.ui.locationhistory.LocationHistoryFragment
import com.sm.location.utils.showSettingsAlert
import com.theartofdev.edmodo.cropper.CropImage
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.edit_dialog_send_message.*
import kotlinx.android.synthetic.main.fragment_user_detail.*
import kotlinx.android.synthetic.main.user_detail_content.*
import javax.inject.Inject

class UserDetailFragment : DaggerFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    @Inject
    lateinit var analyticsTracker: AnalyticsTracker

    private val userDetailViewModel: UserDetailViewModel by viewModels { vmFactory }

    private val permissionsManager = PermissionsManager()

    private val userId: String?
        get() = arguments?.getString(KEY_USER_ID)

    private val isAcceptedTarget: Boolean
        get() = arguments?.getBoolean(KEY_IS_ACCEPTED_TARGET) ?: false

    private val isRequestedTarget: Boolean
        get() = arguments?.getBoolean(KEY_IS_REQUESTED_TARGET) ?: false

    private val isAcceptedWatcher: Boolean
        get() = arguments?.getBoolean(KEY_IS_ACCEPTED_WATCHER) ?: false

    private val sendMessageStateObserver = Observer<State?> {
        when (it) {
            State.SUCCESS -> onStateChanged(R.string.user_detail_alert_send_message_success_message)
            State.FAILURE -> onStateChanged(R.string.user_detail_alert_send_message_failure_message)
            else -> return@Observer
        }
    }

    private val soundAlertStateObserver = Observer<State?> {
        when (it) {
            State.SUCCESS -> onStateChanged(R.string.user_detail_alert_sound_alert_success_message)
            State.FAILURE -> onStateChanged(R.string.user_detail_alert_sound_alert_failure_message)
            else -> return@Observer
        }
    }

    private val deleteStateObserver = Observer<State?> {
        when (it) {
            State.SUCCESS -> onDelete(true)
            State.FAILURE -> onDelete(false)
            else -> return@Observer
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userId?.let { userDetailViewModel.userId.value = it }

        userDetailViewModel.isAcceptedTarget.set(isAcceptedTarget)
        userDetailViewModel.isRequestedTarget.set(isRequestedTarget)
        userDetailViewModel.isAcceptedWatcher.set(isAcceptedWatcher)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentUserDetailBinding.inflate(inflater, container, false).apply {
            vm = userDetailViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userDetailViewModel.sendMessageState.observe(viewLifecycleOwner, sendMessageStateObserver)
        userDetailViewModel.soundAlertState.observe(viewLifecycleOwner, soundAlertStateObserver)
        userDetailViewModel.deleteState.observe(viewLifecycleOwner, deleteStateObserver)

        fabEditInfo.setOnClickListener { showEditDialog() }

        rowPhone.setOnClickListener { callTheUser() }

        notificationsSwitch.setOnCheckedChangeListener { compoundButton, isChecked ->
            trackEvent(AnalyticsConstants.Events.USER_DETAIL_TOGGLE_NOTIFICATIONS)
            if (compoundButton.isPressed)
                userDetailViewModel.changeNotificationsState(isChecked)
        }

        rowSendMessage.setOnClickListener { showSendMessageAlert() }
        rowSoundAlert.setOnClickListener { showSoundAlertConfirmation() }
        rowLocationHistory.setOnClickListener { toLocationHistory(userId) }
        rowGeofencesSettings.setOnClickListener { toGeofences() }
        rowDelete.setOnClickListener { showDeleteAlert() }
    }

    private fun showEditDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setItems(R.array.user_detail_edit_dialog) { _, which ->
                when (which) {
                    0 -> checkStoragePermissions()
                    1 -> showColorPicker()
                }
            }
            .show()
    }

    private fun checkStoragePermissions() {
        if (!permissionsManager.isStoragePermissionsGranted(requireActivity()))
            permissionsManager.requestStoragePermissions(this)
        else {
            toPickPhoto()
        }
    }

    private fun toPickPhoto() {
        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, REQUEST_CODE_PICK_PHOTO)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsManager.REQUEST_CODE_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                toPickPhoto()
            else
                showSettingsAlert(R.string.user_detail_alert_settings_message)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_PICK_PHOTO -> if (resultCode == RESULT_OK && data != null) {
                data.data?.let { imageUri ->
                    CropImage.activity(imageUri)
                        .setAspectRatio(1, 1)
                        .start(requireContext(), this)
                }
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> if (resultCode == RESULT_OK && data != null) {
                val result = CropImage.getActivityResult(data)
                val imageUri = result.uri
                userDetailViewModel.updateImage(imageUri.toString())
                trackEvent(AnalyticsConstants.Events.USER_DETAIL_EDIT_PHOTO)
            }
        }
    }

    private fun showColorPicker() {
        val defaultColor = userDetailViewModel.user.value?.color
            ?: ContextCompat.getColor(requireContext(), R.color.colorPrimary)

        ColorPickerDialog
            .Builder(requireActivity())
            .setDefaultColor(defaultColor)
            .setColorListener { color, _ ->
                userDetailViewModel.updateColor(color)
                trackEvent(AnalyticsConstants.Events.USER_DETAIL_EDIT_COLOR)
            }
            .show()
    }

    private fun callTheUser() {
        val phone = userDetailViewModel.user.value?.getPhone()
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    }

    private fun showSendMessageAlert() {
        val dialog = MaterialAlertDialogBuilder(requireContext())
            .setView(R.layout.edit_dialog_send_message)
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .setPositiveButton(android.R.string.ok) { dialog, _ -> sendMessage(dialog) }
            .create()

        dialog.run {
            show()

            getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false

            sendMessageEditText.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    sendMessageTextInputLayout.error = getString(R.string.user_detail_alert_error_send_message)
                    getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
                } else {
                    sendMessageTextInputLayout.error = ""
                    getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = true
                }
            }
        }
    }

    private fun sendMessage(dialog: DialogInterface) {
        val message = (dialog as? AlertDialog)?.sendMessageEditText?.text.toString()
        userDetailViewModel.sendMessage(requireContext(), message)
        trackEvent(AnalyticsConstants.Events.USER_DETAIL_SEND_MESSAGE)
    }

    private fun showSoundAlertConfirmation() {
        if (userDetailViewModel.isSoundAlertAvailable())
            MaterialAlertDialogBuilder(requireContext())
                .setMessage(R.string.user_detail_sound_alert_alert_message)
                .setPositiveButton(R.string.user_detail_sound_alert_alert_button_send) { _, _ ->
                    userDetailViewModel.sendSoundAlert(requireContext())
                    trackEvent(AnalyticsConstants.Events.USER_DETAIL_SOUND_ALERT)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ -> }
                .show()
        else
            MaterialAlertDialogBuilder(requireContext())
                .setMessage(R.string.user_detail_sound_alert_time_interval_message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .show()
    }

    private fun toLocationHistory(userId: String?) {
        findNavController().navigate(
            R.id.toLocationHistory,
            bundleOf(LocationHistoryFragment.KEY_USER_ID to userId)
        )
        trackEvent(AnalyticsConstants.Events.USER_DETAIL_LOCATION_HISTORY)
    }

    private fun toGeofences() = findNavController().navigate(R.id.toGeofences)

    private fun showDeleteAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.user_detail_alert_delete_message)
            .setNegativeButton(android.R.string.cancel) { _, _ -> }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                when {
                    isAcceptedTarget -> userDetailViewModel.deleteTarget(requireContext())
                    isRequestedTarget -> userDetailViewModel.cancelRequest(requireContext())
                    isAcceptedWatcher -> userDetailViewModel.deleteWatcher(requireContext())
                }
            }
            .show()
    }

    private fun onStateChanged(messageResId: Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(messageResId)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .show()

        userDetailViewModel.clearStates()
    }

    private fun onDelete(result: Boolean) {
        trackEvent(AnalyticsConstants.Events.USER_DETAIL_DELETE_USER)
        val messageResId = if (result) R.string.user_detail_alert_delete_success_message
        else R.string.user_detail_alert_delete_failure_message

        MaterialAlertDialogBuilder(requireContext())
            .setMessage(messageResId)
            .setPositiveButton(android.R.string.ok) { _, _ -> if (result) backToUsersList() }
            .setOnCancelListener { if (result) backToUsersList() }
            .show()

        userDetailViewModel.clearStates()
    }

    private fun backToUsersList() = findNavController().popBackStack()

    private fun trackEvent(eventName: String) {
        val params = bundleOf(AnalyticsConstants.Params.USER_ID to userId)
        analyticsTracker.trackEvent(eventName, params)
    }

    companion object {
        const val KEY_USER_ID = "userId"
        const val KEY_IS_ACCEPTED_TARGET = "isAcceptedTarget"
        const val KEY_IS_REQUESTED_TARGET = "isRequestedTarget"
        const val KEY_IS_ACCEPTED_WATCHER = "isAcceptedWatcher"

        private const val REQUEST_CODE_PICK_PHOTO = 2000
    }
}
