package com.sm.location.ui.watchrequest

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sm.core.utils.TAG
import com.sm.data.models.NotificationEntity
import com.sm.pushapi.utils.PushConstants
import com.sm.location.R
import com.sm.data.models.UserEntity
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.models.RequestState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class WatchRequestViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository,
    private val userInfoRepository: UserInfoRepository,
    private val relationsRepository: RelationsRepository
) : ViewModel() {

    val acceptRequestState = MutableLiveData<RequestState>()

    val denyRequestState = MutableLiveData<RequestState>()

    fun acceptRequest(context: Context, watcher: UserEntity?) = viewModelScope.launch(Dispatchers.IO) {
        if (watcher == null)
            return@launch

        acceptRequestState.postValue(RequestState.LOADING)

        try {
            relationsRepository.acceptRequest(watcher.id)

            acceptRequestState.postValue(RequestState.SUCCESS)

            val title = context.getString(R.string.watch_request_notification_title_accept, userInfoRepository.name)
            val body = context.getString(R.string.watch_request_notification_body_accept, userInfoRepository.getPhone())
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.WATCHER_ACCEPT_REQUEST,
                title = title,
                body = body,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(watcher.id, watcher.pushToken, notification)
        } catch (e: Exception) {
            Log.e(TAG, "Error with accepting request", e)
            acceptRequestState.postValue(RequestState.FAILURE)
        }
    }

    fun denyRequest(context: Context, watcher: UserEntity?) = viewModelScope.launch(Dispatchers.IO) {
        if (watcher == null)
            return@launch

        denyRequestState.postValue(RequestState.LOADING)

        try {
            relationsRepository.denyRequest(watcher.id)

            denyRequestState.postValue(RequestState.SUCCESS)

            val title = context.getString(R.string.watch_request_notification_title_deny, userInfoRepository.name)
            val body = context.getString(R.string.watch_request_notification_body_deny, userInfoRepository.getPhone())
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.WATCHER_DENY_REQUEST,
                title = title,
                body = body,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(watcher.id, watcher.pushToken, notification)
        } catch (e: Exception) {
            Log.e(TAG, "Error with denying request", e)
            denyRequestState.postValue(RequestState.FAILURE)
        }
    }
}
