package com.sm.location.ui.onboarding

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.sm.location.ui.onboarding.slides.*

class OnboardingFragmentPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun getItemCount() = NUM_PAGES

    override fun createFragment(position: Int) = when (position) {
        PAGE_NUM_INFO -> SlideInfoFragment()
        PAGE_NUM_LOCATION -> SlideLocationFragment()
        PAGE_NUM_BACKGROUND_LOCATION -> SlideBackgroundLocationFragment()
        PAGE_NUM_ACTIVITY -> SlideActivityFragment()
        else -> Fragment()
    }

    companion object {
        const val NUM_PAGES = 4

        const val PAGE_NUM_INFO = 0
        const val PAGE_NUM_LOCATION = 1
        const val PAGE_NUM_BACKGROUND_LOCATION = 2
        const val PAGE_NUM_ACTIVITY = 3
    }
}
