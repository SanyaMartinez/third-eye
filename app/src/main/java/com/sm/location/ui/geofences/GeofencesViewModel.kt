package com.sm.location.ui.geofences

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sm.data.models.GeofenceEntity
import com.sm.data.repository.geofence.GeofenceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class GeofencesViewModel @Inject constructor(
    private val geofencesRepository: GeofenceRepository
) : ViewModel() {

    val geofences = geofencesRepository.getAllToDisplay()

    val deleteResult = MutableLiveData<Boolean>()

    fun deleteGeofence(geofence: GeofenceEntity) = viewModelScope.launch(Dispatchers.IO) {
        try {
            geofencesRepository.delete(geofence)
            deleteResult.postValue(true)
        } catch (e: Exception) {
            deleteResult.postValue(false)
        }
    }
}
