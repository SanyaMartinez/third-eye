package com.sm.location.ui.splash

import android.content.Context
import android.os.Build
import android.util.Log
import androidx.lifecycle.*
import androidx.preference.PreferenceManager
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.huawei.agconnect.auth.AGConnectAuth
import com.huawei.agconnect.auth.AGConnectUser
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.aaid.HmsInstanceId
import com.sm.analytics.tracker.AnalyticsTracker
import com.sm.core.utils.TAG
import com.sm.data.repository.geofence.GeofenceRepository
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.note.NoteRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.BuildConfig
import com.sm.location.models.SplashCheckState
import com.sm.location.ui.onboarding.OnboardingActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val userInfoRepository: UserInfoRepository,
    private val locationRepository: LocationRepository,
    private val relationsRepository: RelationsRepository,
    private val geofenceRepository: GeofenceRepository,
    private val noteRepository: NoteRepository,
    private val analyticsTracker: AnalyticsTracker,
) : ViewModel() {

    val splashCheckState = MutableLiveData<SplashCheckState>()

    fun checkSignIn(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val hasOnboardingBeenViewed = sharedPreferences.getBoolean(OnboardingActivity.PREFS_KEY_ONBOARDING, false)

        val authedUser = AGConnectAuth.getInstance().currentUser
        val isAuthed = authedUser != null && !authedUser.isAnonymous

        val state = when {
            !hasOnboardingBeenViewed -> SplashCheckState.TO_ONBOARDING
            !isAuthed -> SplashCheckState.TO_AUTH
            else -> {
                syncData(context, authedUser)
                SplashCheckState.TO_MAIN
            }
        }

        splashCheckState.value = state
    }

    private fun syncData(context: Context, authedUser: AGConnectUser) {
        CoroutineScope(Dispatchers.IO).launch {
            // Firebase авторизация для доступа к Realtime Database и Storage
            Firebase.auth.run {
                if (currentUser == null)
                    Tasks.await(signInAnonymously())
            }

            // Загружаем отсутствующие данные
            if (userInfoRepository.userId.isEmpty()) {
                userInfoRepository.userId = authedUser.uid
                userInfoRepository.loadAndUpdateUserInfo()
            }

            // Получаем свежий push-token
            try {
                val appId = AGConnectServicesConfig.fromContext(context).getString("client/app_id")
                val token = HmsInstanceId.getInstance(context).getToken(appId, "HCM")
                userInfoRepository.updatePushToken(token)
            } catch (e: Exception) {
                Log.e(TAG, "Error with update push token", e)
            }

            // Обновляем запросы
            relationsRepository.syncRequests()

            // Обновляем список пользователей
            relationsRepository.syncUsers()

            // Обновляем данные по истории передвижений отслеживаемых пользователей
            userInfoRepository.targetsIds.forEach { targetId ->
                locationRepository.loadLocations(targetId)
            }

            // Обновляем список Геозон
            userInfoRepository.watchersIds.forEach { watcherId ->
                geofenceRepository.loadAndUpdateGeofences(watcherId)
            }

            // Обновляем заметки: подтвержденных пользователей и свои
            userInfoRepository.run {
                (targetsIds + watchersIds + userId).distinct().forEach { creatorId ->
                    noteRepository.loadAndUpdateNotes(creatorId, userId)
                }
            }

            // Подписываемся на обновление геолокаций
            locationRepository.listenToLocationUpdates(userInfoRepository.targetsIds)

            // Сохраняем данные для аналитики
            analyticsTracker.setUserData(
                userId = userInfoRepository.userId,
                phoneCode = userInfoRepository.phoneCode,
                phoneNumber = userInfoRepository.phoneNumber,
                deviceName = "${Build.BRAND} ${Build.MODEL} ${Build.DISPLAY}",
                appVersionCode = BuildConfig.VERSION_CODE.toString(),
                appVersionNumber = BuildConfig.VERSION_NAME,
            )
        }
    }
}
