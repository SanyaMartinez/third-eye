package com.sm.location.ui.auth.login

import android.util.Log
import androidx.lifecycle.*
import com.huawei.agconnect.auth.AGCAuthException
import com.huawei.agconnect.auth.AGConnectAuth
import com.huawei.agconnect.auth.PhoneAuthProvider
import com.huawei.agconnect.auth.VerifyCodeSettings
import com.sm.core.utils.TAG
import com.sm.core.utils.toMillis
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.models.LoginState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class AuthLoginViewModel @Inject constructor(
    private val userInfoRepository: UserInfoRepository
) : ViewModel() {

    private var phoneCode: String = ""
    private var phoneNumber: String = ""

    private var lastSmsSentTime: Long = 0L

    val loginState = MutableLiveData<LoginState>()

    fun requestSmsVerify(code: String, number: String) {
        val wasSmsAlreadySent = System.currentTimeMillis() - lastSmsSentTime < toMillis(SEND_SMS_INTERVAL_IN_SEC)

        if (wasSmsAlreadySent && phoneCode == code && phoneNumber == number) {
            loginState.value = LoginState.CODE_SEND_ALREADY
            return
        }

        phoneCode = code
        phoneNumber = number

        val settings = VerifyCodeSettings.newBuilder()
            .action(VerifyCodeSettings.ACTION_REGISTER_LOGIN)
            .sendInterval(SEND_SMS_INTERVAL_IN_SEC)
            .locale(Locale.getDefault())
            .build()

        loginState.value = LoginState.LOADING
        PhoneAuthProvider.requestVerifyCode(phoneCode, phoneNumber, settings)
            .addOnSuccessListener {
                loginState.value = LoginState.CODE_SEND_SUCCESS
                lastSmsSentTime = System.currentTimeMillis()
            }
            .addOnFailureListener {
                loginState.value = LoginState.CODE_SEND_FAILURE
                Log.e(TAG, "Error with sending verification code", it)
            }
    }

    fun signIn(verifyCode: String) {
        loginState.value = LoginState.LOADING

        val credential = PhoneAuthProvider.credentialWithVerifyCode(phoneCode, phoneNumber, null, verifyCode)
        AGConnectAuth.getInstance().signIn(credential)
            .addOnSuccessListener {
                viewModelScope.launch(Dispatchers.IO) {
                    if (userInfoRepository.isExistByPhone(phoneCode, phoneNumber)) {
                        userInfoRepository.userId = it.user.uid
                        userInfoRepository.loadAndUpdateUserInfo()
                    } else {
                        // Пользователь зареган через AGConnectAuth, но в БД инфа отсутствует
                        userInfoRepository.userId = it.user.uid
                        userInfoRepository.phoneCode = phoneCode
                        userInfoRepository.phoneNumber = phoneNumber
                        userInfoRepository.sendUserInfo()
                    }

                    loginState.postValue(LoginState.SUCCESS)
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "Error with confirming by verification code", it)
                if (it is AGCAuthException) {
                    if (it.code == AGCAuthException.USER_NOT_REGISTERED)
                        loginState.value = LoginState.NO_USER
                    else
                        loginState.value = LoginState.FAILURE
                } else {
                    loginState.value = LoginState.FAILURE
                }
            }
    }

    companion object {
        private const val SEND_SMS_INTERVAL_IN_SEC = 30
    }
}
