package com.sm.location.ui.noteimage

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.bumptech.glide.Glide
import com.sm.location.R
import kotlinx.android.synthetic.main.fragment_note_image.*

class NoteImageFragment : Fragment(R.layout.fragment_note_image) {

    private val imageUri: String?
        get() = arguments?.getString(KEY_IMAGE_URI)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageUri?.let {
            Glide.with(this)
                .load(Uri.parse(it))
                .fitCenter()
                .into(noteFullscreenImage)
        }
    }

    companion object {
        const val KEY_IMAGE_URI = "IMAGE_URI"
    }
}
