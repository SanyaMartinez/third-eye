package com.sm.location.ui.users.detail

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.sm.core.utils.TAG
import com.sm.core.utils.toMillis
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.notification.NotificationRepository
import com.sm.data.repository.relations.RelationsRepository
import com.sm.location.R
import com.sm.location.models.State
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.pushapi.utils.PushConstants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class UserDetailViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val userInfoRepository: UserInfoRepository,
    private val notificationRepository: NotificationRepository,
    private val relationsRepository: RelationsRepository
) : ViewModel() {

    val userId = MutableLiveData<String>()

    val user = Transformations.switchMap(userId) {
        userRepository.getById(it)
    }

    val isAcceptedTarget = ObservableField(false)
    val isRequestedTarget = ObservableField(false)
    val isAcceptedWatcher = ObservableField(false)

    val sendMessageState = MutableLiveData<State?>()
    val soundAlertState = MutableLiveData<State?>()
    val deleteState = MutableLiveData<State?>()

    fun changeNotificationsState(isEnabled: Boolean) = viewModelScope.launch {
        user.value?.copy(notificationsEnabled = isEnabled)?.let { updatedUser ->
            userRepository.update(updatedUser)
        }
    }

    fun updateImage(imageUri: String) = viewModelScope.launch {
        user.value?.copy(imageUri = imageUri)?.let { updatedUser ->
            userRepository.update(updatedUser)
        }
    }

    fun updateColor(color: Int) = viewModelScope.launch {
        user.value?.copy(color = color)?.let { updatedUser ->
            userRepository.update(updatedUser)
        }
    }

    fun sendMessage(context: Context, message: String) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val user = user.value ?: return@launch

            sendMessageState.postValue(State.LOADING)

            val title = context.getString(R.string.user_detail_send_message_title, userInfoRepository.name)
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.MESSAGE,
                title = title,
                body = message,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(user.id, user.pushToken, notification)

            sendMessageState.postValue(State.SUCCESS)
        } catch (e: Exception) {
            Log.e(TAG, "Error while sending message", e)
            sendMessageState.postValue(State.FAILURE)
        }
    }

    fun sendSoundAlert(context: Context) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val user = user.value ?: return@launch

            soundAlertState.postValue(State.LOADING)

            val title = context.getString(R.string.user_detail_sound_alert_title, userInfoRepository.name)
            val body = context.getString(R.string.user_detail_sound_alert_body)
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.SOUND_ALERT,
                title = title,
                body = body,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(user.id, user.pushToken, notification)

            lastSoundAlertTimestamp = System.currentTimeMillis()

            soundAlertState.postValue(State.SUCCESS)
        } catch (e: Exception) {
            Log.e(TAG, "Error while sending sound alert", e)
            soundAlertState.postValue(State.FAILURE)
        }
    }

    fun cancelRequest(context: Context) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val target = user.value ?: return@launch

            deleteState.postValue(State.LOADING)
            relationsRepository.cancelRequest(target.id)

            val title = context.getString(R.string.user_detail_notification_title_cancel, userInfoRepository.name)
            val body = context.getString(R.string.user_detail_notification_body_cancel, userInfoRepository.getPhone())
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.WATCHER_CANCEL_REQUEST,
                title = title,
                body = body,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(target.id, target.pushToken, notification)

            deleteState.postValue(State.SUCCESS)
        } catch (e: Exception) {
            Log.e(TAG, "Error while canceling request", e)
            deleteState.postValue(State.FAILURE)
        }
    }

    fun deleteTarget(context: Context) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val target = user.value ?: return@launch

            deleteState.postValue(State.LOADING)
            relationsRepository.deleteTarget(target.id)

            val title = context.getString(R.string.user_detail_notification_title_delete_target, userInfoRepository.name)
            val body = context.getString(R.string.user_detail_notification_body_delete_target, userInfoRepository.getPhone())
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.DELETE_TARGET,
                title = title,
                body = body,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(target.id, target.pushToken, notification)

            deleteState.postValue(State.SUCCESS)
        } catch (e: Exception) {
            Log.e(TAG, "Error while deleting target", e)
            deleteState.postValue(State.FAILURE)
        }
    }

    fun deleteWatcher(context: Context) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val watcher = user.value ?: return@launch

            deleteState.postValue(State.LOADING)
            relationsRepository.deleteWatcher(watcher.id)

            val title = context.getString(R.string.user_detail_notification_title_delete_watcher, userInfoRepository.name)
            val body = context.getString(R.string.user_detail_notification_body_delete_watcher, userInfoRepository.getPhone())
            val senderUserId = userInfoRepository.userId

            val notification = NotificationEntity(
                timestamp = System.currentTimeMillis(),
                topic = PushConstants.Topics.DELETE_WATCHER,
                title = title,
                body = body,
                senderUserId = senderUserId,
            )
            notificationRepository.sendNotification(watcher.id, watcher.pushToken, notification)

            deleteState.postValue(State.SUCCESS)
        } catch (e: Exception) {
            Log.e(TAG, "Error while deleting watcher", e)
            deleteState.postValue(State.FAILURE)
        }
    }

    private var lastSoundAlertTimestamp = 0L

    fun isSoundAlertAvailable() = System.currentTimeMillis() - lastSoundAlertTimestamp > SOUND_ALERT_INTERVAL

    fun clearStates() {
        sendMessageState.postValue(null)
        soundAlertState.postValue(null)
        deleteState.postValue(null)
    }

    companion object {
        private val SOUND_ALERT_INTERVAL = toMillis(60)
    }
}
