package com.sm.location.ui.geofenceediting

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.dhaval2404.colorpicker.ColorPickerDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.model.Circle
import com.huawei.hms.maps.model.CircleOptions
import com.huawei.hms.maps.model.LatLng
import com.sm.core.utils.getTransparentColor
import com.sm.maps.ui.BaseMapsFragment
import com.sm.location.models.State
import com.sm.location.R
import com.sm.data.models.GeofenceEntity
import com.sm.data.models.UserEntity
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.databinding.FragmentGeofenceEditingBinding
import com.sm.location.di.ViewModelFactory
import kotlinx.android.synthetic.main.bottomsheet_geofence_editing.*
import javax.inject.Inject

class GeofenceEditingFragment : BaseMapsFragment(), HuaweiMap.OnMapClickListener {

    @Inject
    lateinit var userInfoRepository: UserInfoRepository

    @Inject
    lateinit var vmFactory: ViewModelFactory

    private val geofenceEditingViewModel: GeofenceEditingViewModel by viewModels { vmFactory }

    private val geofenceId: Long
        get() = arguments?.getLong(KEY_GEOFENCE_ID, NO_GEOFENCE_ID) ?: NO_GEOFENCE_ID

    private val geofenceObserver = Observer<GeofenceEntity?> {
        it?.let { geofence -> setGeofenceData(geofence) }
    }

    private val acceptedTargetsObserver = Observer<List<UserEntity>?> {
        it?.let { list -> acceptedTargets = list }
    }

    private val geofenceSaveStateObserver = Observer<State?> {
        when (it) {
            State.SUCCESS -> onSuccessResult()
            State.FAILURE -> onFailureResult()
            else -> return@Observer
        }
    }

    private var acceptedTargets: List<UserEntity> = emptyList()
    private val targetsIds: MutableList<String> = mutableListOf()
    private var color: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        color = ContextCompat.getColor(context, R.color.colorPrimary)
    }

    private var snackbar: Snackbar? = null

    private var circle: Circle? = null

    override fun onMapReady(huaweiMap: HuaweiMap) {
        super.onMapReady(huaweiMap)

        map?.setOnMapClickListener(this)

        geofenceEditingViewModel.geofence.value?.let { setGeofenceData(it) }
    }

    /**
     * Выстанавливаем значения переданной Геозоны для редактирования
     */
    private fun setGeofenceData(geofence: GeofenceEntity) {
        circle?.remove()
        circle = map?.addCircle(
            CircleOptions()
                .center(geofence.getCenter())
                .radius(geofence.radius)
                .strokeColor(geofence.color)
                .fillColor(getTransparentColor(geofence.color, 0.5))
        )

        radiusSeekBar.progress = geofence.radius.toInt()
        geofenceEditText.setText(geofence.title)

        targetsIds.apply {
            clear()
            addAll(geofence.targetsIds)
        }

        color = geofence.color

        map?.animateCamera(CameraUpdateFactory.newLatLng(geofence.getCenter()))
        setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED)
    }

    override fun onMapClick(position: LatLng) {
        snackbar?.dismiss()
        setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED)

        circle?.remove()
        circle = map?.addCircle(
            CircleOptions()
                .center(position)
                .radius(radiusSeekBar.progress.toDouble())
                .strokeColor(color)
                .fillColor(getTransparentColor(color, 0.5))
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentGeofenceEditingBinding.inflate(inflater, container, false).apply {
            vm = geofenceEditingViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpBottomSheet()

        if (geofenceId == NO_GEOFENCE_ID) {
            snackbar = Snackbar.make(
                requireView(),
                getString(R.string.geofence_editing_snackbar_text),
                Snackbar.LENGTH_INDEFINITE
            )
            snackbar?.show()
        } else {
            geofenceEditingViewModel.geofenceId.value = geofenceId
        }

        geofenceEditingViewModel.acceptedTargets.observe(viewLifecycleOwner, acceptedTargetsObserver)
        geofenceEditingViewModel.geofence.observe(viewLifecycleOwner, geofenceObserver)
        geofenceEditingViewModel.geofenceSaveState.observe(viewLifecycleOwner, geofenceSaveStateObserver)
    }

    private fun setUpBottomSheet() {
        setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN)

        radiusSeekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (progress < MIN_CIRCLE_RADIUS)
                    seekBar.progress = MIN_CIRCLE_RADIUS
                else
                    circle?.radius = progress.toDouble()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        geofenceEditText.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrEmpty()) {
                geofenceInputLayout.error = getString(R.string.geofence_editing_title_error_text)
                buttonSaveGeofence.isEnabled = false
            } else {
                geofenceInputLayout.error = ""
                buttonSaveGeofence.isEnabled = true
            }
        }

        rowTargetsSelect.setOnClickListener { showTargetsSelectionDialog() }
        rowColorPicker.setOnClickListener { showColorPicker() }

        buttonSaveGeofence.isEnabled = !geofenceEditText.text.isNullOrEmpty()
        buttonSaveGeofence.setOnClickListener {
            saveGeofence()
        }
    }

    private fun setBottomSheetState(state: Int) {
        BottomSheetBehavior.from(geofenceEditingBottomSheet).state = state
    }

    private fun showTargetsSelectionDialog() {
        val newTargetsIdsForGeofence: MutableList<String> = targetsIds.toMutableList()

        val targets: List<UserEntity> = acceptedTargets.toList()

        if (targets.isEmpty()) {
            MaterialAlertDialogBuilder(requireContext())
                .setMessage(R.string.geofence_editing_targets_alert_message_empty)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .show()
        } else {
            val targetsNicknames = targets.map { it.nameToShow() }.toTypedArray()
            val checkedItems =
                targets.map { newTargetsIdsForGeofence.contains(it.id) }.toBooleanArray()

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.geofence_editing_targets_alert_title)
                .setMultiChoiceItems(targetsNicknames, checkedItems) { _, which, isChecked ->
                    val targetId = targets[which].id

                    if (isChecked) newTargetsIdsForGeofence.add(targetId)
                    else newTargetsIdsForGeofence.remove(targetId)
                }
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    targetsIds.apply {
                        clear()
                        addAll(newTargetsIdsForGeofence)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { _, _ -> }
                .show()
        }
    }

    private fun showColorPicker() {
        ColorPickerDialog
            .Builder(requireActivity())
            .setDefaultColor(color)
            .setColorListener { color, _ ->
                this.color = color
                circle?.strokeColor = color
                circle?.fillColor = getTransparentColor(color, 0.5)
            }
            .show()
    }

    private fun saveGeofence() {
        if (targetsIds.isEmpty()) {
            showAlert(R.string.geofence_editing_alert_message_empty_targets, navigateBack = false)
            return
        }

        buttonSaveGeofence.isEnabled = false

        circle?.let {
            val geofence = GeofenceEntity(
                title = geofenceEditText.text.toString(),
                latitude = it.center.latitude,
                longitude = it.center.longitude,
                radius = it.radius,
                watcherId = userInfoRepository.userId,
                targetsIds = targetsIds,
                color = color
            )

            if (geofenceId != NO_GEOFENCE_ID)
                geofence.id = geofenceId

            geofenceEditingViewModel.saveGeofence(requireContext(), geofence)
        }
    }

    private fun onSuccessResult() {
        showAlert(R.string.geofence_editing_alert_message_success, navigateBack = true)
    }

    private fun onFailureResult() {
        showAlert(R.string.geofence_editing_alert_message_error, navigateBack = false)
        buttonSaveGeofence.isEnabled = true
    }

    private fun showAlert(@StringRes messageId: Int, navigateBack: Boolean) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(messageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> if (navigateBack) backToGeofences() }
            .setOnCancelListener { if (navigateBack) backToGeofences() }
            .show()
    }

    private fun backToGeofences() = findNavController().popBackStack()

    override fun onDestroy() {
        super.onDestroy()
        snackbar?.dismiss()
    }

    companion object {
        const val KEY_GEOFENCE_ID = "geofenceId"
        private const val NO_GEOFENCE_ID = -1L

        private const val MIN_CIRCLE_RADIUS = 100
    }
}
