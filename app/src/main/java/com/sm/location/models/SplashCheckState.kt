package com.sm.location.models

enum class SplashCheckState {
    TO_ONBOARDING,
    TO_AUTH,
    TO_MAIN
}
