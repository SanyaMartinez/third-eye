package com.sm.location.models

enum class LoginState {
    LOADING,
    CODE_SEND_ALREADY,
    CODE_SEND_SUCCESS,
    CODE_SEND_FAILURE,
    SUCCESS,
    FAILURE,
    NO_USER,
}
