package com.sm.location.models

enum class RegistrationState {
    LOADING,
    ALREADY,
    CODE_SEND_ALREADY,
    CODE_SEND_SUCCESS,
    CODE_SEND_FAILURE,
    SUCCESS,
    FAILURE,
}
