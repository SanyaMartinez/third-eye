package com.sm.location.models

enum class RequestState {
    LOADING,
    ERROR_USER_NOT_FOUND,
    ERROR_SELF_REQUEST,
    SUCCESS,
    FAILURE
}
