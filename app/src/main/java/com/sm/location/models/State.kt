package com.sm.location.models

enum class State {
    LOADING,
    SUCCESS,
    FAILURE
}
