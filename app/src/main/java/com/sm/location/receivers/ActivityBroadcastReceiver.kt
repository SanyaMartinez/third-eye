package com.sm.location.receivers

import android.content.Context
import android.content.Intent
import com.huawei.hms.location.ActivityConversionResponse
import com.huawei.hms.location.ActivityIdentificationData
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.notification.NotificationRepository
import com.sm.pushapi.utils.PushConstants
import com.sm.location.R
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import dagger.android.DaggerBroadcastReceiver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ActivityBroadcastReceiver : DaggerBroadcastReceiver() {

    @Inject
    lateinit var notificationRepository: NotificationRepository

    @Inject
    lateinit var userInfoRepository: UserInfoRepository

    @Inject
    lateinit var userRepository: UserRepository

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        if (intent.action == ACTION_PROCESS_ACTIVITY) {

            // Смена активности
            ActivityConversionResponse.getDataFromIntent(intent)?.let { activityTransitionResult ->
                val list = activityTransitionResult.activityConversionDatas
                if (list.isEmpty()) return

                CoroutineScope(Dispatchers.IO).launch {
                    list.forEach { data ->
                        val activityTypesNamesArray = context.resources.getStringArray(R.array.activity_types_names)
                        val arrayIndex = when (data.activityType) {
                            ActivityIdentificationData.VEHICLE -> 0
                            ActivityIdentificationData.BIKE -> 1
                            ActivityIdentificationData.FOOT -> 2
                            ActivityIdentificationData.STILL -> 3
                            ActivityIdentificationData.WALKING -> 4
                            ActivityIdentificationData.RUNNING -> 5
                            else -> return@launch
                        }

                        val activityTypeName = activityTypesNamesArray[arrayIndex]

                        val title = context.getString(R.string.activity_conversion_notification_title, userInfoRepository.name)
                        val body = context.getString(R.string.activity_conversion_notification_body, activityTypeName)
                        val senderUserId = userInfoRepository.userId
                        val pushData = "{'${PushConstants.DataKeys.ACTIVITY_TYPE}': '${data.activityType}'}"

                        val notification = NotificationEntity(
                            timestamp = System.currentTimeMillis(),
                            topic = PushConstants.Topics.ACTIVITY_CONVERSION,
                            title = title,
                            body = body,
                            senderUserId = senderUserId,
                            data = pushData,
                        )

                        userRepository.getAcceptedWatchersAsync().forEach { watcher ->
                            notificationRepository.sendNotification(watcher.id, watcher.pushToken, notification)
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val ACTION_PROCESS_ACTIVITY = "com.sm.location.receivers.ActivityBroadcastReceiver.ACTION_PROCESS_ACTIVITY"
    }
}
