package com.sm.location.receivers

import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import com.huawei.hms.location.LocationResult
import com.sm.core.utils.TAG
import com.sm.data.models.LocationEntity
import com.sm.data.repository.location.LocationRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import dagger.android.DaggerBroadcastReceiver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class LocationBroadcastReceiver : DaggerBroadcastReceiver() {

    @Inject
    lateinit var locationRepository: LocationRepository

    @Inject
    lateinit var userInfoRepository: UserInfoRepository

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        if (intent.action == ACTION_PROCESS_LOCATION) {
            if (LocationResult.hasResult(intent)) {
                val result = LocationResult.extractResult(intent)
                result?.let {
                    val locations = result.locations
                    if (locations.isNotEmpty()) {
                        Log.d(TAG, "requestLocationUpdatesWithIntent:\n")
                        saveLocations(locations, userInfoRepository.userId)
                    }
                }
            }

            // Было в примере документации, не используется, на всякий
            /*
            if (LocationAvailability.hasLocationAvailability(intent)) {
                val locationAvailability = LocationAvailability.extractLocationAvailability(intent)
                locationAvailability?.let {
                    Log.d(TAG, "locationAvailability: ${it.isLocationAvailable}")
                }
            }
             */
        }
    }

    private fun saveLocations(locations: List<Location>, userId: String) = CoroutineScope(Dispatchers.IO).launch {
        locations.forEach {
            val locationEntity = LocationEntity(
                userId = userId,
                timestamp = it.time,
                latitude = it.latitude,
                longitude = it.longitude
            )
            locationRepository.insert(locationEntity)

            Log.d(TAG, "Location saved: $locationEntity")
        }
    }

    companion object {
        const val ACTION_PROCESS_LOCATION = "com.sm.location.receivers.LocationBroadcastReceiver.ACTION_PROCESS_LOCATION"
    }
}
