package com.sm.location.receivers

import android.content.Context
import android.content.Intent
import android.util.Log
import com.huawei.hms.location.Geofence
import com.huawei.hms.location.GeofenceData
import com.sm.core.utils.TAG
import com.sm.data.models.NotificationEntity
import com.sm.data.repository.notification.NotificationRepository
import com.sm.pushapi.utils.PushConstants
import com.sm.location.R
import com.sm.data.repository.user.UserRepository
import com.sm.data.repository.userinfo.UserInfoRepository
import com.sm.location.managers.GeofenceManager.Companion.GEOFENCE_ID_DELIMITER
import dagger.android.DaggerBroadcastReceiver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class GeofenceBroadcastReceiver : DaggerBroadcastReceiver() {

    @Inject
    lateinit var notificationRepository: NotificationRepository

    @Inject
    lateinit var userInfoRepository: UserInfoRepository

    @Inject
    lateinit var userRepository: UserRepository

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        if (intent.action == ACTION_PROCESS_GEOFENCE) {
            GeofenceData.getDataFromIntent(intent)?.let {
                if (it.isFailure) {
                    Log.e(TAG, "onReceive error, code: ${it.errorCode}")
                    return
                }

                val myLocation = it.convertingLocation

                val time = myLocation.time
                val latitude = myLocation.latitude
                val longitude = myLocation.longitude
                val conversionType = it.conversion

                CoroutineScope(Dispatchers.IO).launch {
                    it.convertingGeofenceList?.forEach { data ->
                        val (_, geofenceName, watcherId) = data.uniqueId.split(GEOFENCE_ID_DELIMITER)

                        val title = context.getString(R.string.geofence_conversion_notification_title, userInfoRepository.name)
                        val bodyRes =
                            if (conversionType == Geofence.ENTER_GEOFENCE_CONVERSION) R.string.geofence_conversion_notification_body_enter
                            else R.string.geofence_conversion_notification_body_exit
                        val body = context.getString(bodyRes, geofenceName)
                        val senderUserId = userInfoRepository.userId
                        val pushData = "{'${PushConstants.DataKeys.LATITUDE}': '$latitude', '${PushConstants.DataKeys.LONGITUDE}': '$longitude'}"

                        val notification = NotificationEntity(
                            timestamp = time,
                            topic = PushConstants.Topics.GEOFENCE_CONVERSION,
                            title = title,
                            body = body,
                            senderUserId = senderUserId,
                            data = pushData,
                        )

                        userRepository.getByIdAsync(watcherId)?.let { watcher ->
                            if (watcher.isAcceptedWatcher)
                                notificationRepository.sendNotification(watcher.id, watcher.pushToken, notification)
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val ACTION_PROCESS_GEOFENCE = "com.sm.location.receivers.GeofenceBroadcastReceiver.ACTION_PROCESS_GEOFENCE"
    }
}
