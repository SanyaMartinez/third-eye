package com.sm.location.adapters

/**
 * Интерфейс для RecyclerView Адаптеров.
 * Используется для передачи списка данных через Data Binding в xml.
 */
interface BindableAdapter<T> {
    var data: List<T>
}
