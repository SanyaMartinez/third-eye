package com.sm.location.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.sm.data.models.NotificationEntity
import com.sm.location.R
import com.sm.location.databinding.HistoryListItemBinding
import com.sm.pushapi.utils.PushConstants

class HistoryAdapter(private val onClickListener: OnClickListener):
    RecyclerView.Adapter<HistoryAdapter.ViewHolder>(), BindableAdapter<NotificationEntity> {

    override var data = listOf<NotificationEntity>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = HistoryListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(data[position])

    override fun getItemCount() = data.size

    inner class ViewHolder(private val binding: HistoryListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NotificationEntity) {
            binding.notification = item
            binding.executePendingBindings()
            binding.root.setOnClickListener {
                onClickListener.onClick(item)
            }
            binding.root.setOnLongClickListener {
                onClickListener.onLongClick(item)
            }

            val iconId = when (item.topic) {
                PushConstants.Topics.MESSAGE -> R.drawable.ic_message
                PushConstants.Topics.SOUND_ALERT -> R.drawable.ic_notifications
                PushConstants.Topics.GEOFENCE_UPDATE,
                PushConstants.Topics.GEOFENCE_CONVERSION -> R.drawable.ic_location_on
                PushConstants.Topics.NOTE_ADD -> R.drawable.ic_rate_review
                PushConstants.Topics.ACTIVITY_CONVERSION -> R.drawable.ic_run
                PushConstants.Topics.WATCHER_REQUEST,
                PushConstants.Topics.WATCHER_DENY_REQUEST,
                PushConstants.Topics.WATCHER_ACCEPT_REQUEST,
                PushConstants.Topics.WATCHER_CANCEL_REQUEST,
                PushConstants.Topics.DELETE_TARGET,
                PushConstants.Topics.DELETE_WATCHER -> R.drawable.ic_person
                else -> R.drawable.ic_location_on
            }
            val icon = ContextCompat.getDrawable(binding.root.context, iconId)
            binding.historyIcon.setImageDrawable(icon)
        }
    }

    interface OnClickListener {
        fun onClick(notification: NotificationEntity)
        fun onLongClick(notification: NotificationEntity): Boolean
    }
}
