package com.sm.location.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sm.data.models.GeofenceEntity
import com.sm.location.databinding.GeofencesListItemBinding

class GeofencesAdapter(private val onClickListener: OnClickListener):
    RecyclerView.Adapter<GeofencesAdapter.ViewHolder>(), BindableAdapter<GeofenceEntity> {

    override var data = listOf<GeofenceEntity>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GeofencesListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(data[position])

    override fun getItemCount() = data.size

    inner class ViewHolder(private val binding: GeofencesListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: GeofenceEntity) {
            binding.apply {
                geofence = item
                executePendingBindings()
                geofenceTitle.setOnClickListener { onClickListener.onClick(item) }
                iconEdit.setOnClickListener { onClickListener.onClickEdit(item) }
                iconDelete.setOnClickListener { onClickListener.onClickDelete(item) }
            }
        }
    }

    interface OnClickListener {
        fun onClick(geofence: GeofenceEntity)
        fun onClickEdit(geofence: GeofenceEntity)
        fun onClickDelete(geofence: GeofenceEntity)
    }
}
