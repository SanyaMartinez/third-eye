package com.sm.location.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sm.data.models.UserLocation
import com.sm.location.databinding.TargetsListItemBinding

class TargetsAdapter(private val onClickListener: OnClickListener):
    RecyclerView.Adapter<TargetsAdapter.ViewHolder>(), BindableAdapter<UserLocation> {

    override var data = listOf<UserLocation>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = TargetsListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(data[position])

    override fun getItemCount() = data.size

    inner class ViewHolder(private val binding: TargetsListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserLocation) {
            binding.apply {
                userLocation = item
                executePendingBindings()
                targetName.setOnClickListener { onClickListener.onClick(item) }
                iconView.setOnClickListener { onClickListener.onClickView(item) }
            }
        }
    }

    interface OnClickListener {
        fun onClick(userLocation: UserLocation)
        fun onClickView(userLocation: UserLocation)
    }
}
