package com.sm.location.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sm.data.models.UserEntity
import com.sm.location.databinding.UsersListItemBinding

class UsersAdapter(private val onClickListener: OnClickListener) :
    RecyclerView.Adapter<UsersAdapter.ViewHolder>(), BindableAdapter<UserEntity> {

    override var data = listOf<UserEntity>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = UsersListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UsersAdapter.ViewHolder, position: Int) =
        holder.bind(data[position])

    override fun getItemCount() = data.size

    inner class ViewHolder(private val binding: UsersListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserEntity) {
            binding.user = item
            binding.executePendingBindings()
            binding.root.setOnClickListener {
                onClickListener.onClick(item)
            }
        }
    }

    interface OnClickListener {
        fun onClick(user: UserEntity)
    }
}
