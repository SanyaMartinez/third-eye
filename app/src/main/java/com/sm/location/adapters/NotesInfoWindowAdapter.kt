package com.sm.location.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.model.Marker
import com.sm.data.models.UserNote
import com.sm.location.databinding.NoteInfoWindowBinding

class NotesInfoWindowAdapter(
    private val context: Context,
    private val listener: OnNoteInfoWindowClickListener,
): HuaweiMap.InfoWindowAdapter {

    private var notesMarkersMap: HashMap<Marker, UserNote> = hashMapOf()
    private var userId: String? = null

    fun setData(data: HashMap<Marker, UserNote>) { notesMarkersMap = data }
    fun setUserId(data: String) { userId = data }

    override fun getInfoWindow(marker: Marker): View? {
        // Для маркеров Юзеров возвращаем null и используем дефолтный InfoWindow
        val data = notesMarkersMap[marker]
            ?: return null

        val layoutInflater = LayoutInflater.from(context)
        val binding = NoteInfoWindowBinding.inflate(layoutInflater)

        binding.noteNameText.text = data.user.nameToShow()
        binding.noteTitleText.text = data.note.title
        binding.noteDescriptionText.text = data.note.description

        data.note.noteImageUri?.let { uriString ->
            Glide.with(context)
                .load(Uri.parse(uriString))
                .into(binding.noteImage)

            binding.noteImage.apply {
                visibility = View.VISIBLE
                setOnClickListener { listener.onClickImage(data) }
            }
        }

        binding.noteIconDelete.setOnClickListener { listener.onClickDelete(data) }

        return binding.root
    }

    override fun getInfoContents(marker: Marker): View? = null

    interface OnNoteInfoWindowClickListener {
        fun onClickImage(data: UserNote)
        fun onClickDelete(data: UserNote)
    }
}
