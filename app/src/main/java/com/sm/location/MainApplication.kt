package com.sm.location

import android.app.Application
import com.huawei.hms.maps.MapsInitializer
import com.sm.core.utils.Constants
import com.sm.location.di.DaggerApplicationComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = androidInjector

    override fun onCreate() {
        super.onCreate()

        DaggerApplicationComponent
            .factory()
            .create(this)
            .inject(this)

        MapsInitializer.setApiKey(Constants.HmsValues.API_KEY)
    }
}
