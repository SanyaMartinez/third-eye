package com.sm.maps.ui.infowindow

import android.view.View
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.model.Marker
// TODO: заюзать при адаптации под google services
interface NoteInfoWindowAdapter {
    fun getInfoWindowView(marker: Marker): View?
    fun getInfoContentsView(marker: Marker): View?
}

abstract class HuaweiNoteInfoWindowAdapter : HuaweiMap.InfoWindowAdapter, NoteInfoWindowAdapter {
    override fun getInfoWindow(marker: Marker) = getInfoWindowView(marker)
    override fun getInfoContents(marker: Marker) = getInfoContentsView(marker)
}