# Companions - GPS-Tracking APP

## Huawei Cup 2020 Winner

![image](https://img.shields.io/badge/version-1.0.0-success)

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## About

**Companions** - это приложение для GPS-трекинга и взаимодействия между пользователями.

Реализовано в рамках **[Huawei Honor Cup 2020](https://huaweicup.ru/)**

#HMS #HuaweiMobileServices

## Основной функционал

- Добавление зарегистрированных пользователей по номеру телефона для трекинга и обмена информацией. Связи и их количество не ограничены
- Просмотр текущего местоположения отслеживаемых пользователей, а также их истории перемещений за любой указанный период времени
- Создание областей на карте для проверки нахождения в них пользователей и уведомления о входе\выходе
- Уведомления об изменении физической активности пользователей
- Возможность отправки собственных пуш-сообщений и звукового сигнала - уведомления подтвержденным пользователям
- Добавление Маркеров с описанием и вложением на карту и возможность поделиться ими с подтвержденными пользователями
- Сохранение истории всех входящих уведомлений

## Технические особенности

- Авторизация, регистрация, а также дальнейший поиск и добавление пользователей происходит по номеру телефона
- Синхронизация информации с удаленной БД и ее сохранение после выхода из аккаунта, очистки данных или переустановки приложения
- Поддержка светлой и тёмной темы (в соответствии с выставленными настройками на устройстве)
- Применение **Material Design**
- Применение **MVVM**, **Repository** паттернов и модульной архитектуры
- **Unit/UI** тесты

## Технологии и их применение

- **Language**: Kotlin
- **HMS SDKs**:  
  1. **Auth Service** - Авторизация и регистрация пользователей по номеру телефона
  2. **Map Kit** - Отображение на карте иконок пользователей, геозон, истории перемещений и Маркеров
  3. **Location Kit** - Отслеживание текущей геолокации, нахождения в заданных областях и смены физической активности
  4. **Push Kit** - Уведомление пользователей обо всех событиях
  5. **Analytics Kit** - Хранение аналитических данных о пользователе и совершенных событиях
  6. **Crash Service** - Мониторинг и исправление возникающих в приложении ошибок
- **Firebase SDKs**:
  1. **Realtime Database** - Синхронизация всей информации с помощью удаленной БД
  2. **Storage** - Хранение изображений из пользовательских Маркеров на карте
- **Other**:
  - Dagger, Room, Databinding, LiveData, Coroutines, Navigation Component, etc.

## Примеры

[Скриншоты работы приложения](https://gitlab.com/SanyaMartinez/third-eye/-/tree/master/examples)

## Download

<a href='https://appgallery.cloud.huawei.com/ag/n/app/C103030401?channelId=Git&id=6f37ff42274d4471bc09ea8e86e8ad0c&s=F6BFA86AB8F0E16B8DD5C0FCB1DE58D983FFC8B1A6B0CF0CC5B9D80C12E4C3D8&detailType=0&v='><img alt='Get it on App Gallery' width="300" height="120" src='https://huaweimobileservices.com/wp-content/uploads/2020/05/Explore-it-on-AppGallery.png'/></a>

## Authors

- **Aleksandr Litvinov**

## License

Apache 2.0 license
